# Overview
This repository contains the source code of the ecosystem simulator Ecotwin. Ecotwin grew out of the research project **From Special to General AI**, financed by the *Torsten Söderberg Foundation* and led by Claes Strannegård. The original code of this repository was developed by several researchers and students at *Chalmers University of Technology* and the *University of Gothenburg*.

# Contributions
- General model used in Ecotwin
    - Strannegård C. et al., **The Ecosystem Path to General AI**, [arXiv](https://arxiv.org/abs/2108.07578), 2021.
    - Strannegård C. et al., **Evolution and learning in artificial ecosystems**, In: *Proceedings of the IJCAI-18 Workshop on Architectures for Generality & Autonomy, 27th International Joint Conference on Artificial Intelligence*, Stockholm, 2018.
- Homeostasis of animats
    - Ferrari P. and Kleve B., **A Generic Model of Motivation in Artificial Animals Based on Reinforcement Learning**, Master's thesis, Chalmers, 2021.
- Evolution and reflexes of animats
    - Glimmerfors H. and Skoglund V., **Combining Reflexes and Reinforcement Learning in Evolving Ecosystems for Artificial Animals**, Master's thesis, Chalmers, 2021.
- Lotka-Volterra analysis
    - Karlsson T., **Multi-Agent Deep Reinforcement Learning in a Three-Species Predator-Prey Ecosystem**,. Master's thesis, Chalmers, 2021. 

# Get Started
Made for Windows, but the general approach should be the same on other systems. 
1. Download [Unity Hub](https://unity3d.com/get-unity/download). From the Hub, install a Unity version (the project was originally developed in 2020.1.2f1).
2. Make sure to have [Anaconda](https://www.anaconda.com/products/individual) installed.
3. Clone this git repo. The version tags can help you to navigate to a commit that is tested and running. 
4. Open the Unity project (the `Ecotwin` folder) in Unity.
5. Make sure that mlagents is installed in Unity: Open `Window > Packet Manager`, there, search for `ml agents` and make sure that it is installed. Tip: If `ml agents` does not show up in the Packet Manager, make sure that it is set to `Unity Registry`.
6. Open a desired scene from `Ecotwin/Assets/Scenes/`.
7. Install the conda environment: Open the `anaconda prompt` and navigate to the repository `ecotwin` (the anaconda prompt uses commands used in Windows' Command Prompt such as `cd` and `dir`). When in the repository enter the `Training` folder. Here install the conda environment by typing `conda env create --file condaenv-ecotwin.yaml` (Make sure that the filename has not changed in later versions). Conda should then create the environment that can be activated with `conda activate ecotwin` (check this name as well). Note: There have been some problems with the dependencies that should be installed with `pip`. If the pip installation fails, those dependencies can be installed manually with pip by the commands specified in the `.yaml` file.
8. The training can now be started:
    - Navigate to the `Training` folder
    - Activate the conda environment
    - Run the command `mlagents-learn --run-id YYYY_MM_DD-HHMM condaenv-mlagents.yaml`. This will start a new simulation with the given run ID (replace the timestamp with your current time or other desired name), this ID is used as the name for the results folder. The settings used in the run are the ones specified in the `.yaml` file and can be altered, for example in case a new brain is trained and should be loaded at start.
    - Click the play button in Unity's editor. If the simulation stops immediately, this may be due to the scene's configuration (a simulation can stop when no agents are alive).
9. To terminate the training, press the play button in Unity again. 

Good to know:
- The training results can be viewed in `Tensorboard` by running the command `tensorboard --logdir results_dev` from the `Training` folder in a new anaconda terminal with the conda environment activated. 
- The naming of different variables is supposed to be as explicit as possible. However, things might not change in the way you expect in some cases. To be sure about what changes you are doing, searching through the code for the specific variable could be a good idea. 
- Data from the training is saved in `Training/results_dev`. However, this data is not human readable. Data from saved simulations can safely be removed, but do not remove the `data.meta` file if you are not removing all data directories (that is all the folders except `pretrained`). 

# Environments
*This section is to be extended*

- Unity: 2020.3.13f1
    - It may be possible to open the project in an older version of Unity, however a recent version of Unity is highly recommended for easy assignment of variables in the inspector.
- Conda environment for training: `Training/condaenv-mlagents.yaml`
    - Currently using mlagents v0.23.0 however there are newer versions
    - Currently using ML-agents v1.7.2
    - If updating mlagents or ML-agents always check which versions are compatible first!
- Conda environment for ml-agents API training: `Training/condaenv-ecotwin.yaml`
- Operating System:
    - Ecotwin's developers have used various operating systems (Windows, Ubuntu, iOS), but recent iterations of the project have been developed using only Windows and Ubuntu. The project may however be compatible with other operating systems. 

# Versions
Version | Explanation
------- | -----------
v0.1 | A restructured codebase of the project used during Ecotwin's early development by master theses students. This version has been restructured for clarity and readability but is not functional due to a small number of errors and missing prefabs. <br /> Features present: <br /> -Animats: the agents taking decisions and making actions <br /> -AreaManager: a component managing free positions and synchronizing actions between animats <br /> -Consumables: edible objects which can either spread dynamically (Crops) or statically <br /> -Actions: a modular structure for giving animats access to a number of actions
v0.2 | All errors in initial commit resolved and work has begun on updating prefabs to be compatible with the new structure. More comments added in all classes. <br /> New in this version: <br /> -ActionModule: reworked to inherit from ScriptableObject to allow for serialization
v0.3 | Inheritable traits (genetic attributes and reflexes) have been moved to the action modules where the corresponding traits are used. Agent prefabs have been updated to adhere to the new structure (prefabs: one predator and one prey).
v0.4 | Addition of ActionModule for movement in Terrain. Using TerrainMovement, an animat can move at different speeds in different terrains (represented by different textures on the terrain) and different speeds in up/down-hills. <br /> Addition of animat using CameraSensor for observations.
v0.5 | Introduction of ChunkManager. AreaManager now delegates responsibility over *chunks* in the world to ChunkManagers. Each ChunkManager performs the same actions that AreaManager previously did in the whole scene. This is used to optimize spawning speed of new objects.
v0.6 | Genotype & Phenotype reworked to use DNA class for cleaner access to values. Values previously required variables for serialization and thereafter access through dictionaries using nameof(...). Now instead, values are in container classes and the container classes are used to group values together and pass on to evolutionary process. (The container classes are not *needed*, but without the container classes, copying genotype into phenotype becomes harder.)
v1.0 | *End of 2021 summer development* Many bugs have been fixed and the project appears to be in a stable state. Note that the only animat prefab tested to be working as intended (other animats do not have updated parameters) is /Agents_dev/Goat_Variations/Graphics/Goat_WaterTest. Minor pushes may still be made by the 2021 summer team to account for e.g no-graphics prefabs and minor code clean-ups.