using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A consumable whose nutrients and color does not change over time (but destruction due to decay is possible)
/// Consumption of static food is instant regardless of bitesize
/// </summary>
public class StaticFood : Consumable
{
    [SerializeField]
    private float _decayRate;

    protected override void UpdateSpecs()
    {
        if (IsDestroyed())
            Delete();
    }

    public override (Dictionary<string, float>, float) Consume(float biteSize, Transform caller)
    {
        _ratioRemaining = 0f;
        return (new Dictionary<string, float>(_nutrients), _ratioRemaining);
    }

    public override bool IsDestroyed()
    {
        return _ratioRemaining < 0.0001f || _decayRate != 0 && _age >= _decayRate + (int)(_decayRate * UnityEngine.Random.Range(0.8f,1f/0.8f));
    }
}
