using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Water : Consumable
{
    protected override void UpdateSpecs()
    {
        return;
    }

    public override (Dictionary<string, float>, float) Consume(float biteSize, Transform caller)
    { // Assumption: Animats can never fully drink a water supply
        Dictionary<string, float> eatenNutrients = Nutrients; //Note: cannot be changed to _nutrients (Nutrients makes a copy)
        foreach (string key in _nutrients.Keys.ToList())
            eatenNutrients[key] *= biteSize;
        
        return (eatenNutrients, 1f);
    }

    public override bool IsDestroyed()
    {
        return false;
    }
}
