﻿using System;
using UnityEngine;

/// <summary>
/// Settings for spawning of a consumable
/// </summary>
[Serializable]
public class FoodType
{
    [SerializeField]
    private int _initialCount;
    public int InitialCount { get { return _initialCount; } private set { _initialCount = value; } }

    [SerializeField]
    private float _spawnProbability;
    public float SpawnProbability { get { return _spawnProbability; } private set { _spawnProbability = value; } }
    
    [SerializeField]
    private Consumable _prefab;
    public Consumable Prefab { get { return _prefab; } private set { _prefab = value; } }

    public string Name { get { return Prefab.name; } }
}
