﻿using UnityEngine;
using System.Linq;

/// <summary>
/// An implementation of consumables where the consumables spread from another
/// instance of a consumable - as opposed to from wherever
/// </summary>
public abstract class Crop : Consumable
{   
    [SerializeField] [Tooltip("Expected number of offspring during lifetime")]
    protected float _fertility;
    
    [SerializeField]
    protected float _spreadRadius;
    
    [SerializeField]
    protected float _graceRadius;
    public float GraceRadius {
        get { return _graceRadius; }
        protected set { _graceRadius = value; }
    }
    
    [SerializeField]
    protected float _hostileRadius;
    public float HostileRadius {
        get { return _hostileRadius; }
        protected set { _hostileRadius = value; }
    }
    
    [SerializeField] [Range(0f, 1f)] [Tooltip("Percentual resistance to seasonal changes to fertility")]
    protected float _winterResistance;

    protected int _stepsToRipen;

    public override void Awake()
    {
        base.Awake();
        foreach (string key in _nutrients.Keys.ToList())
            if (!_genes.ContainsKey(key + "Ripened"))
                _nutrients.Remove(key);
        
        _stepsToRipen = (int)(_genes["timeToRipen"] * _areaManager.MonthLength);
        _areaManager.SetEnvStateDelta(name, +1);
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!IsDestroyed())
            if (Random.Range(0f, 1f) < AdjustedFertility())
                Reproduce();
        else
            HandleDeath();
    }

    protected override void UpdateSpecs()
    {
        if (_age > _stepsToRipen)
        { // Decay
            float f = (float)_age/(float)_stepsToDecay;
            foreach (string key in _nutrients.Keys.ToList())
                _nutrients[key] = Mathf.Lerp(_genes[key + "Ripened"], _genes[key + "Decayed"], f) * _ratioRemaining;
            
            _meshRenderer.material.color = new Color(
                Mathf.Lerp(_genes["colorRipenedR"], _genes["colorDecayedR"], f),
                Mathf.Lerp(_genes["colorRipenedG"], _genes["colorDecayedG"], f),
                Mathf.Lerp(_genes["colorRipenedB"], _genes["colorDecayedB"], f),
                _ratioRemaining
            );
        }
        else
        { // Ripen
            float f = (float)_age/(float)_stepsToRipen;
            foreach (string key in _nutrients.Keys.ToList())
                _nutrients[key] = Mathf.Lerp(_genes[key + "Initial"], _genes[key + "Ripened"], f) * _ratioRemaining;
            
            _meshRenderer.material.color = new Color(
                Mathf.Lerp(_genes["colorInitialR"], _genes["colorRipenedR"], f),
                Mathf.Lerp(_genes["colorInitialG"], _genes["colorRipenedG"], f),
                Mathf.Lerp(_genes["colorInitialB"], _genes["colorRipenedB"], f),
                _ratioRemaining
            );
        }
    }

    public override bool IsDestroyed()
    {
        return _age >= _stepsToDecay || _ratioRemaining < 0.01f;
    }

    /// <summary>
    /// The spread mechanic which is based upon the instance of this crop
    /// </summary>
    protected abstract void Reproduce();
    
    /// <summary>
    /// How affected is the fertility (and thus the spread) of crops by winters?
    /// </summary>
    /// <returns>The fertility when season is accounted for</returns>
    protected float AdjustedFertility()
    {
        return 
            (1
                - ((Mathf.Sin(
                    2 * Mathf.PI * _areaManager.GetEpisodeStep()
                    / (_areaManager.MonthLength * _areaManager.YearLength) - Mathf.PI / 2
                ) + 1
            ) / 2) 
            * _areaManager.SeasonalFoodSpread * (1 - _winterResistance)) * _fertility / (float)_stepsToDecay;
    }

    /// <summary>
    /// All crops share a common tag, so to separate between types, the areamanager tracks the crops' names
    /// </summary>
    protected virtual void HandleDeath()
    {
        _areaManager.SetEnvStateDelta(name, -1);
        Delete();
    }

    public void Age()
    {
        Age(Random.Range(0, _stepsToRipen));
    }
}
