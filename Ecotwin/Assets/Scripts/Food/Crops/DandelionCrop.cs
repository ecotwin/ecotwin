﻿using UnityEngine;

/// <summary>
/// An implementation of a crop which is intended to spread with wind (thus far from the parent)
/// </summary>
public class DandelionCrop : Crop
{
    public override void Awake()
    {
        name = "DandelionCrop";
        base.Awake();
    }
    protected override void Reproduce()
    {
        float rand1 = Random.Range(Mathf.Pow((_graceRadius/_spreadRadius), 2), 1f);
        float rand2 = Random.Range(Mathf.Pow((_graceRadius/_spreadRadius), 2), rand1);
        float rand3 = Random.Range(0f, 1f);
        float r = _spreadRadius * Mathf.Sqrt(rand2);
        float theta = rand3 * 2f * Mathf.PI;
        float xOffset = r * Mathf.Cos(theta);
        float zOffset = r * Mathf.Sin(theta);
        float x = transform.position.x + xOffset;
        float z = transform.position.z + zOffset;
        float y = _areaManager.Terrain.SampleHeight(new Vector3(x, 0, z));
        if (x < _areaManager.EnvBoundaries.Item1.x
            || x > _areaManager.EnvBoundaries.Item2.x
            || y < _areaManager.EnvBoundaries.Item1.y
            || y > _areaManager.EnvBoundaries.Item2.y
            || z < _areaManager.EnvBoundaries.Item1.z
            || z > _areaManager.EnvBoundaries.Item2.z)
            return;

        Vector3 pos = new Vector3(x, y, z);
        if (_areaManager.CanSpawnCrop(pos, this))
            Instantiate(this, pos, transform.rotation, _areaManager.transform);
    }
}
