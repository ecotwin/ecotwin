﻿using UnityEngine;

/// <summary>
/// An implementation of a crop which is intended to spread close to its parent
/// </summary>
public class GrassCrop : Crop
{
    private int _stepsToRecover;
    private int _stepsRecovering;
    private int _originalLayer;

    private bool recovering = false;

    public override void Awake()
    {
        name = "GrassCrop";
        base.Awake();
        _stepsRecovering = 0;
        _originalLayer = gameObject.layer;
        _stepsToRecover = (int)(_genes["timeToRecover"] * _areaManager.MonthLength);
    }

    protected override void Reproduce()
    {
        float rand1 = Random.Range(Mathf.Pow((_graceRadius/_spreadRadius), 2), 1f);
        float rand2 = Random.Range(0f, 1f);
        float r = _spreadRadius * Mathf.Sqrt(rand1);
        float theta = rand2 * 2f * Mathf.PI;
        float xOffset = r * Mathf.Cos(theta);
        float zOffset = r * Mathf.Sin(theta);
        float x = transform.position.x + xOffset;
        float z = transform.position.z + zOffset;
        float y = _areaManager.Terrain.SampleHeight(new Vector3(x, 0, z));
        if (x < _areaManager.EnvBoundaries.Item1.x
            || x > _areaManager.EnvBoundaries.Item2.x
            || y < _areaManager.EnvBoundaries.Item1.y
            || y > _areaManager.EnvBoundaries.Item2.y
            || z < _areaManager.EnvBoundaries.Item1.z
            || z > _areaManager.EnvBoundaries.Item2.z)
            return;

        Vector3 pos = new Vector3(x, y, z);
        if (_areaManager.CanSpawnCrop(pos, this))
            Instantiate(this, pos, transform.rotation, _areaManager.transform);
    }

    protected override void HandleDeath()
    {
        if (_age + _stepsRecovering >= _stepsToDecay)
        {
            if (recovering)
                _areaManager.SetEnvStateDelta(name, +1);
            base.HandleDeath();
        }
        else if (_ratioRemaining < 0.01f)
        {
            if (!recovering)
            {
                recovering = true;
                _areaManager.UntrackConsumable(this, tag);
                _areaManager.SetEnvStateDelta(name, -1);
                gameObject.layer = 2;
                GetComponent<Collider>().enabled = false;
                UpdateSpecs();
            }
            _stepsRecovering++;
            if (_stepsRecovering % _stepsToRecover == 0)
            {
                _areaManager.SetEnvStateDelta(name, +1);
                if (_areaManager.CanSpawnCrop(transform.position, this))
                {
                    recovering = false;
                    _areaManager.TrackConsumable(this, tag);
                    gameObject.layer = _originalLayer;
                    GetComponent<Collider>().enabled = true;
                    _age = 0;
                    _ratioRemaining = 1f;
                }
                else
                    base.HandleDeath();
            }
        }
    }
}