﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Base class for all consumable objects and which nutrients they contain.
/// </summary>
public abstract class Consumable : MonoBehaviour
{
    [SerializeField]
    protected SerializableDictionary<string, float> _nutrients = new SerializableDictionary<string, float>() {
        { "toxin",      0.0f },
        { "sugar",      0.0f },
        { "nitrogen",   0.0f },
        { "potassium",  0.0f },
        { "phosphorus", 0.0f },
        { "protein",    0.0f },
        { "vitamins",   0.0f }
    };
    public Dictionary<string, float> Nutrients {
        get { return new Dictionary<string, float>(_nutrients); }
        protected set { _nutrients = new SerializableDictionary<string, float>(value); } }

    [SerializeField]
    protected SerializableDictionary<string, float> _genes = new SerializableDictionary<string, float>() {
        { "toxinInitial",      0.0f },
        { "toxinDecayed",      0.0f },
        { "sugarInitial",      0.0f },
        { "sugarDecayed",      0.0f },
        { "nitrogenInitial",   0.0f },
        { "nitrogenDecayed",   0.0f },
        { "potassiumInitial",  0.0f },
        { "potassiumDecayed",  0.0f },
        { "phosphorusInitial", 0.0f },
        { "phosphorusDecayed", 0.0f },
        { "proteinInitial",    0.0f },
        { "proteinDecayed",    0.0f },
        { "vitaminsInitial",   0.0f },
        { "vitaminsDecayed",   0.0f },
        { "colorInitialR",     0.0f },
        { "colorInitialG",     0.0f },
        { "colorInitialB",     0.0f },
        { "colorDecayedR",     0.0f },
        { "colorDecayedG",     0.0f },
        { "colorDecayedB",     0.0f },
        { "timeToDecay",       0.0f }
    };
    public Dictionary<string, float> Genes {
        get { return new Dictionary<string, float>(_genes); }
        protected set { _genes = new SerializableDictionary<string, float>(value); } }

    protected int _age = 0;

    /// <summary>
    /// The ratio of this consumable which is still remaining in the world
    /// </summary>
    protected float _ratioRemaining;

    protected AreaManager _areaManager;

    protected MeshRenderer _meshRenderer;

    protected int _stepsToDecay;

    public virtual void Awake()
    {
        _age = 0;
        _ratioRemaining = 1f;

        _meshRenderer = GetComponent<MeshRenderer>();
        _areaManager  = GetComponentInParent<AreaManager>();
        _areaManager.TrackConsumable(this, tag);
        //_areaManager.SetEnvStateDelta(tag, +1);

        foreach (string key in _nutrients.Keys.ToList())
            _nutrients[key] = _genes[key + "Initial"];

        _stepsToDecay = (int)(_genes["timeToDecay"] * _areaManager.MonthLength);
    }

    public virtual void FixedUpdate()
    {
        _age++;
        UpdateSpecs();
    }

    protected abstract void UpdateSpecs();

    /// <summary>
    /// Reduces the ratio remaining of this consumable from an animat consuming it
    /// </summary>
    /// <returns>All the nutrients given from the bite taken out of this consumable</returns>
    public virtual (Dictionary<string, float>, float) Consume(float biteSize, Transform caller)
    {   
        /*float r = Mathf.Min(ratioRemaining, biteSize);
        ratioRemaining -= r;
        Dictionary<string, float> eatenNutrients = new Dictionary<string, float>(Nutrients);
        foreach (string key in Nutrients.Keys.ToList())
        {
            Nutrients[key] = Mathf.Lerp(0f, Nutrients[key], ratioRemaining/(ratioRemaining + r));
            eatenNutrients[key] -= Nutrients[key];
        }
        return (eatenNutrients, ratioRemaining);*/
        float bite = Mathf.Min(_ratioRemaining, biteSize);
        foreach (string key in _nutrients.Keys)
            if (_nutrients[key] * bite > biteSize)
                bite = _nutrients[key] / biteSize;

        _ratioRemaining -= bite;
        Dictionary<string, float> eatenNutrients = Nutrients; //Note: cannot be changed to _nutrients (Nutrients makes a copy)
        foreach (string key in _nutrients.Keys.ToList())
        {
            _nutrients[key] = Mathf.Lerp(0f, _nutrients[key], _ratioRemaining/(_ratioRemaining + bite));
            eatenNutrients[key] -= _nutrients[key];
        }
        return (eatenNutrients, _ratioRemaining);
    }

    /// <summary>
    /// Ages the consumable by <paramref name="steps" /> number of steps immediately
    /// </summary>
    public void Age(int steps)
    {
        _age += steps;
    }

    /// <returns>Does the consumable fulfil the criteria for "dying"?</returns>
    public virtual bool IsDestroyed()
    {
        return _age >= _stepsToDecay || _ratioRemaining < 0.01f;
    }

    /// <summary>
    /// Destroy this consumable and remove links to areaManager
    /// </summary>
    protected void Delete()
    {
        //_areaManager.SetEnvStateDelta(tag, -1);
        _areaManager.UntrackConsumable(this, tag);
        Destroy(gameObject);
    }
}