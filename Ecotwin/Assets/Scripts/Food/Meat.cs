﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//TODO: rework to fit into consumable
public class Meat : Consumable
{
    protected override void UpdateSpecs()
    {
        if (IsDestroyed())
            Delete();

        float f = (float)_age/(float)_stepsToDecay;
        foreach (string key in _nutrients.Keys.ToList())
            _nutrients[key] = Mathf.Lerp(_genes[key + "Initial"], _genes[key + "Decayed"], f) * _ratioRemaining;

        _meshRenderer.material.color = new Color(
            Mathf.Lerp(_genes["colorInitialR"], _genes["colorDecayedR"], f),
            Mathf.Lerp(_genes["colorInitialG"], _genes["colorDecayedG"], f),
            Mathf.Lerp(_genes["colorInitialB"], _genes["colorDecayedB"], f),
            _ratioRemaining
        );
    }

    public void ReduceInitialEnergy(float mass)
    {
        foreach (string key in _nutrients.Keys.ToList())
        {
            _genes[key + "Initial"] *= mass;
            _nutrients[key] *= mass;
        }
    }
}