//Source: http://wiki.unity3d.com/index.php/SerializableDictionary
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SerializableDictionary {}

[Serializable]
/// <summary>
/// A simple implementation of a serializable dictionary. The implementation supports creating simple serializable dictionaries
/// with the default equality comparer and initializing a SerializableDictionary given a Dictionary<TKey, TValue> or
// IEnumerable<KeyValuePair<TKey,TValue>>
/// </summary>
public class SerializableDictionary<TKey, TValue> : SerializableDictionary, ISerializationCallbackReceiver, IDictionary<TKey, TValue> {
    //[SerializeField, FormerlySerializedAs("keys"), FormerlySerializedAs("m_Keys")]
    //private List<TKey>   keys   = new List<TKey>();
    //[SerializeField, FormerlySerializedAs("values"), FormerlySerializedAs("m_Values")]
    //private List<TValue> values = new List<TValue>();

    [SerializeField]
    private List<SerializableKeyValuePair> _list = new List<SerializableKeyValuePair>();

    [Serializable]
    public struct SerializableKeyValuePair {
        public TKey   Key;
        public TValue Value;

        public SerializableKeyValuePair (TKey key, TValue value) {
            Key   = key;
            Value = value;
        }

        public void SetValue (TValue value) {
            Value = value;
        }
    }


    private Dictionary<TKey, uint> KeyPositions => _keyPositions.Value;
    private Lazy<Dictionary<TKey, uint>> _keyPositions;

    public SerializableDictionary() {
        _keyPositions = new Lazy<Dictionary<TKey, uint>>(MakeKeyPositions);
    }
    public SerializableDictionary(IDictionary<TKey, TValue> dict) : this() {
        foreach (var entry in dict)
            Add(entry.Key, entry.Value);
    }
    public SerializableDictionary(IEnumerable<KeyValuePair<TKey, TValue>> dict) : this() {
        foreach (var entry in dict)
            Add(entry.Key, entry.Value);
    }

    private Dictionary<TKey, uint> MakeKeyPositions () {
        var numEntries = _list.Count;
        var result = new Dictionary<TKey, uint>( numEntries );
        for (int i = 0; i < numEntries; i++)
            result[_list[i].Key] = (uint) i;
        return result;
    }

    public void OnBeforeSerialize() {}
    public void OnAfterDeserialize() {
        // After deserialization, the key positions might be changed
        _keyPositions = new Lazy<Dictionary<TKey, uint>>(MakeKeyPositions);

        //if (keys != null && keys.Count != 0) {
        //    for (int i = 0; i < keys.Count; i++)
        //        this[keys[i]] = values[i];
        //    keys  .Clear();
        //    values.Clear();
        //}
    }

#region IDictionary<TKey, TValue>
    public TValue this[TKey key]
    {
        get => _list[(int) KeyPositions[key]].Value;
        set {
            if (KeyPositions.TryGetValue(key, out uint index))
                _list[(int) index].SetValue(value);
            else
            {
                KeyPositions[key] = (uint) _list.Count;
                _list.Add( new SerializableKeyValuePair( key, value ) );
            }
        }
    }

    public ICollection<TKey>   Keys   => _list.Select( tuple => tuple.Key ).ToArray();
    public ICollection<TValue> Values => _list.Select( tuple => tuple.Value ).ToArray();

    public void Add(TKey key, TValue value) {
        if (KeyPositions.ContainsKey(key))
            throw new ArgumentException("An element with the same key already exists in the dictionary.");
        else {
            KeyPositions[key] = (uint) _list.Count;
            _list.Add( new SerializableKeyValuePair( key, value ) );
        }
    }

    public bool ContainsKey(TKey key) => KeyPositions.ContainsKey(key);


    public bool Remove (TKey key)
    {
        if (KeyPositions.TryGetValue(key, out uint index))
        {
            var kp = KeyPositions;
            kp.Remove(key);

            var numEntries = _list.Count;

            _list.RemoveAt( (int) index );
            for (uint i = index; i < numEntries - 1; i++)
                kp[_list[(int) i].Key] = i;

            return true;
        }
        else
            return false;
    }

    public bool TryGetValue (TKey key, out TValue value)
    {
        if (KeyPositions.TryGetValue(key, out uint index))
        {
            value = _list[(int) index].Value;
            return true;
        }
        else
        {
            value = default;
            return false;
        }
    }
#endregion


#region ICollection <KeyValuePair<TKey, TValue>>
    public int  Count      => _list.Count;
    public bool IsReadOnly => false;

    public void Add (KeyValuePair<TKey, TValue> kvp) => Add( kvp.Key, kvp.Value );

    public void Clear    ()                               => _list.Clear();
    public bool Contains (KeyValuePair<TKey, TValue> kvp) => KeyPositions.ContainsKey(kvp.Key);

    public void CopyTo (KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
        var numKeys = _list.Count;
        if (array.Length - arrayIndex < numKeys)
            throw new ArgumentException("arrayIndex");
        for (int i = 0; i < numKeys; i++, arrayIndex++)
        {
            var entry = _list[i];
            array[arrayIndex] = new KeyValuePair<TKey, TValue>( entry.Key, entry.Value );
        }
    }

    public bool Remove(KeyValuePair<TKey, TValue> kvp) => Remove(kvp.Key);
#endregion


#region IEnumerable <KeyValuePair<TKey, TValue>>
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator ()
    {
        return _list.Select(ToKeyValuePair).GetEnumerator();

        KeyValuePair<TKey, TValue> ToKeyValuePair (SerializableKeyValuePair skvp)
        {
            return new KeyValuePair<TKey, TValue>( skvp.Key, skvp.Value );
        }
    }
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
#endregion
}