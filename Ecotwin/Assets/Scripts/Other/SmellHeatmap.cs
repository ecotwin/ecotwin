﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: remove or re-add support for drawing heatmaps
public class SmellHeatmap : MonoBehaviour
{
    [HideInInspector]
    public Vector4[] positions;
    [HideInInspector]
    public float[] radiuses;
    [HideInInspector]
    public float[] intensities;
    Vector4[] properties;

    public Material material;

    [HideInInspector]
    public int sizeOfBoard;
    [HideInInspector]
    public Vector3 centerOfBoard;

    public void InitializeHeatmap()
    {
        positions = new Vector4[sizeOfBoard*sizeOfBoard];
        radiuses = new float[sizeOfBoard*sizeOfBoard];
        intensities = new float[sizeOfBoard*sizeOfBoard];
        properties = new Vector4[sizeOfBoard*sizeOfBoard];

        for (int iy = 0; iy < sizeOfBoard; iy++)
        {
            for (int ix = 0; ix < sizeOfBoard; ix++)
            {
                positions[iy*sizeOfBoard + ix] = new Vector4(centerOfBoard.x - (sizeOfBoard/2) + 0.5f + ix, centerOfBoard.y - (sizeOfBoard / 2) + 0.5f + iy);
                radiuses[iy * sizeOfBoard + ix] = 0.5f;
                intensities[iy * sizeOfBoard + ix] = Random.Range(-0.25f, 1f); // Just to test
            }
        }
    }

    void UpdateHeatmap()
    {
        material.SetInt("_Points_Length", sizeOfBoard*sizeOfBoard);
        for (int iy = 0; iy < sizeOfBoard; iy++)
        {
            for (int ix = 0; ix < sizeOfBoard; ix++)
            {
                //positions[iy * sizeOfBoard + ix] += new Vector4(Random.Range(-0.1f, +0.1f), Random.Range(-0.1f, +0.1f), 0) * Time.deltaTime;

                properties[iy * sizeOfBoard + ix] = new Vector4(radiuses[iy * sizeOfBoard + ix], intensities[iy * sizeOfBoard + ix], 0, 0);
            }
        }

        material.SetVectorArray("_Points", positions);
        material.SetVectorArray("_Properties", properties);
    }
}
