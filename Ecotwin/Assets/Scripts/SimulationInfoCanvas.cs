﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displays information related to the environment's current state
/// </summary>
public class SimulationInfoCanvas : MonoBehaviour
{
    private Text text;

    public void Awake()
    {
        text = GetComponent<Text>();
    }

    public void UpdateState(Dictionary<string, string> newState, IEnumerable<string> fieldNames)
    {
        string output = "";
        foreach (KeyValuePair<string, string> keyValuePair in newState)
        {
            if (fieldNames.Contains(keyValuePair.Key))
                output += $"{keyValuePair.Key}: {keyValuePair.Value} \n";
            else
            {
                string[] substrings = keyValuePair.Key.Split('_');
                foreach (string s in fieldNames)
                {
                    if (substrings[0] == s)
                    {
                        output += $"{String.Join("s_", substrings.Where(x => x != s))}: {keyValuePair.Value}\n";
                        break;
                    }
                }
            }
        }
        text.text = output;
    }
}
