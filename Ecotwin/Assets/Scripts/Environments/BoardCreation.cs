﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A helper class for creating the environment if the simulation is not intended to use the current scene's settings
/// </summary>
public class BoardCreation
{
    /// <summary>
    /// Repositions walls to locations around the world's center
    /// </summary>
    /// <returns>The minimum and maximum coordinates in the world</returns>
    public (Vector3, Vector3) CreateBox(
        GameObject wallN,
        GameObject wallE,
        GameObject wallS,
        GameObject wallW,
        GameObject roof,
        Vector3 size,
        Vector3 centerPosition = new Vector3())
    {
        (Vector3, Vector3) boundaries = 
            (centerPosition - (size / 2f),
            boundaries.Item2 = centerPosition + (size / 2f)
        );

        Vector3 nOffset = wallN.transform.localScale;
        Vector3 eOffset = wallE.transform.localScale;
        Vector3 sOffset = wallS.transform.localScale;
        Vector3 wOffset = wallW.transform.localScale;
        Vector3 roofOffset = roof.transform.localScale;

        wallN.transform.position = new Vector3(centerPosition.x, (boundaries.Item2.y + nOffset.y) / 2f - 0.5f, boundaries.Item2.z + nOffset.z);
        wallE.transform.position = new Vector3(boundaries.Item2.x + eOffset.x, (boundaries.Item2.y + eOffset.y) / 2f - 0.5f, centerPosition.z);
        wallS.transform.position = new Vector3(centerPosition.x, (boundaries.Item2.y + sOffset.y) / 2f - 0.5f, boundaries.Item1.z - sOffset.z);
        wallW.transform.position = new Vector3(boundaries.Item1.x - wOffset.x, (boundaries.Item2.y + wOffset.y) / 2f - 0.5f, centerPosition.z);
        roof.transform.position  = new Vector3(centerPosition.x, boundaries.Item2.y + roofOffset.y / 2f - 0.5f, centerPosition.z);

        return boundaries;
    }

    public (Vector3, Vector3) SetBoundaries(
        GameObject wallN,
        GameObject wallE,
        GameObject wallS,
        GameObject wallW,
        GameObject roof)
    {
        return (
            new Vector3(
                wallW.transform.position.x + wallW.transform.localScale.x / 2f,
                0,
                wallS.transform.position.z + wallS.transform.localScale.z / 2f
            ),
            new Vector3(
                wallE.transform.position.x - wallE.transform.localScale.x / 2f,
                roof.transform.position.y  - roof.transform.localScale.y  / 2f,
                wallN.transform.position.z - wallN.transform.localScale.z / 2f
            )
        );
    }

    public void SetWallSizes(GameObject[] walls, Vector3[] scales)
    {
        if (walls.Length != scales.Length)
        { 
            Debug.LogWarning("SetWallSizes called with inequal lenghts for walls and scales");
            return;
        }
        for (int i = 0; i < walls.Length; i++)
            walls[i].transform.localScale = scales[i];
    }
}
