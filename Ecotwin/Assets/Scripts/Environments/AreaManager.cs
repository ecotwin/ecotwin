﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Utility;

/// <summary>
/// Control for the game area. Provides global information and keeps track of all objects in the area. 
/// </summary>
public class AreaManager : MonoBehaviour
{
    //----------------------------Variables-----------------------------------

#region Training Settings
    [SerializeField] [Tooltip("If we should do lifelong learning")]
    private bool _doLifelongLearning = false;
    public bool DoLifelongLearning { get { return _doLifelongLearning; } private set { _doLifelongLearning = value; } }

    [SerializeField] [Tooltip("Whether to use episode-learning.")]
    private bool _episodeLearning = false;
    public bool EpisodeLearning { get { return _episodeLearning; } private set { _episodeLearning = value; } }

    [SerializeField] [Tooltip("The max amount of steps per episode. "
        + "If all agents die before, the episode will be terminated.")]
    private int _episodesTimeLimits = int.MaxValue;

    [SerializeField]
    private bool _asyncEpisodes = false;
#endregion

#region Seasons Settings
    [SerializeField] [Tooltip("Should the sun oscillate to only give light during daytime?")]
    private bool _dayNightCycle;
    public bool DayNightCycle { get { return _dayNightCycle; } private set { _dayNightCycle = value; } }

    [SerializeField]
    private Light _theSun;

    [SerializeField] [Tooltip("The number of steps in a day")]
    private int _dayLength = 160;

    [SerializeField] [Tooltip("The number of time steps per month")]
    private int _monthLength = 5000;
    public int MonthLength { get { return _monthLength; } private set { _monthLength = value; } }

    [SerializeField] [Tooltip("The number of months in a year")]
    private int _yearLength = 12;
    public int YearLength { get { return _yearLength; } private set { _yearLength = value; } }

    [SerializeField] [Range(0f, 1f)] [Tooltip("0 => normal spread all year round, 1 => no spread in winter")]
    private float _seasonalFoodSpread;
    public float SeasonalFoodSpread { get { return _seasonalFoodSpread; } private set { _seasonalFoodSpread = value; } }
#endregion

#region World Creation
    /// <summary>
    /// Item1 -> the minimum boundaries (i.e. West/South/Down)
    /// Item2 -> the maximum boundaries (i.e. East/North/Up)
    /// </summary>
    public (Vector3, Vector3) EnvBoundaries { get; private set; }

    [SerializeField] [Tooltip("The wall representing the north boundary of the game area")]
    private GameObject _wallN;

    [SerializeField] [Tooltip("The wall representing the east boundary of the game area")]
    private GameObject _wallE;

    [SerializeField] [Tooltip("The wall representing the south boundary of the game area")]
    private GameObject _wallS;

    [SerializeField] [Tooltip("The wall representing the west boundary of the game area")]
    private GameObject _wallW;

    [SerializeField] [Tooltip("The 'roof' representing the maximum height")]
    private GameObject _roof;

    [SerializeField] [Tooltip("Size of world after resize")]
    private ConditionalField<Vector3> _worldSize;

    [SerializeField] [Tooltip("The species to be spawned initially")]
    private Species[] _species;

    private Dictionary<string, Species> _speciesDict = new Dictionary<string, Species>();

    [SerializeField] [Tooltip("How much food should be spawned initially and how likely they "
    + "should be to spread")]
    private FoodType[] _foodTypes;

    [SerializeField]
    private Terrain _terrain;
    public Terrain Terrain { get { return _terrain; } }
#endregion

#region Environment State
    [SerializeField] [Tooltip("Canvas that holds simulation information.")]
    private SimulationInfoCanvas _simInfo;

    [Serializable]
    // NOTE: It is very important to update GetFullEnvState() if a variable
    // is added/removed/renamed!
    private class EnvironmentState
    {
        public int NSteps = 0;
        public int NEpisodes = 1;
        public int NYears = 0;
        public int NMonths = 0;
        // Array to represent all sexes
        public Dictionary<string, int[]> NSpecies = new Dictionary<string, int[]>();
        public Dictionary<string, int> NFood = new Dictionary<string, int>();
        public int NTookAction = 0;

        public Dictionary<string, string> GetFullEnvState()
        {
            Dictionary<string, string> fullEnvState = new Dictionary<string, string>();
            fullEnvState.Add("NSteps", NSteps.ToString());
            fullEnvState.Add("NEpisodes", NEpisodes.ToString());
            fullEnvState.Add("NYears", NYears.ToString());
            fullEnvState.Add("NMonths", NMonths.ToString());
            foreach (string s in NSpecies.Keys)
            {
                fullEnvState.Add($"NSpecies_{s}_Male", NSpecies[s][0].ToString());
                fullEnvState.Add($"NSpecies_{s}_Female", NSpecies[s][1].ToString());
            }
            foreach (string s in NFood.Keys)
                fullEnvState.Add($"NFood_{s}", NFood[s].ToString());
            fullEnvState.Add("NTookAction", NTookAction.ToString());
            return fullEnvState;
        }
    }
    private EnvironmentState _envState = new EnvironmentState();

    /// <summary>
    /// The number of agents created. Used for numbering the agents. 
    /// </summary>
    private int _agentsCreated = 0;

    private int _iterationAtLatestEpisodeBegin = 0;

    private int _longestEpisodeLength = 0;

    private int _episodeMax = int.MaxValue;

#endregion

#region Miscellaneous
    [SerializeField] [Range(0f, 0.5f)] [Tooltip("The objects' mesh ratio that can be penetrable by other objects.")]
    private float _penetrability;

    [SerializeField] [Min(1)] [Tooltip("The number of ChunkManagers == _countChunkManagers^2."
        + "Fewer ChunkManagers => more accurate checks, More ChunkManagers => faster checks")]
    private int _countChunkManagers;

    [SerializeField]
    private ConditionalField<int> _maximumConsumablesInChunk;

    private ChunkManager[,] _chunkManagers;

    [SerializeField] [Tooltip("Objects already placed in the scene which require initialization")]
    private GameObject[] _predefinedObjects;
#endregion

    //----------------------------Methods-------------------------------------

#region Initialization
    public virtual void Awake()
    {
        BoardCreation b = new BoardCreation();
        if (_worldSize.IsUsed) // Obsolete?
        {
            b.SetWallSizes(
                new GameObject[] { _wallN, _wallE, _wallS, _wallW, _roof },
                new Vector3[] {
                    new Vector3(_worldSize.Value.x + 2f, _worldSize.Value.y + 2f, 1f),
                    new Vector3(1f, _worldSize.Value.y + 2f, _worldSize.Value.z + 2f),
                    new Vector3(_worldSize.Value.x + 2f, _worldSize.Value.y + 2f, 1f),
                    new Vector3(1f, _worldSize.Value.y + 2f, _worldSize.Value.z + 2f),
                    new Vector3(_worldSize.Value.x + 2f, 1f, _worldSize.Value.z + 2f)
                }
            );
            EnvBoundaries = b.CreateBox( _wallN, _wallE, _wallS, _wallW, _roof, _worldSize.Value);
        }
        else
            EnvBoundaries = b.SetBoundaries(_wallN, _wallE, _wallS, _wallW, _roof);

        _chunkManagers = new ChunkManager[_countChunkManagers, _countChunkManagers];
        float xMin = EnvBoundaries.Item1.x;
        float zMin = EnvBoundaries.Item1.z;
        float xStep = (EnvBoundaries.Item2.x - EnvBoundaries.Item1.x) / (float)_countChunkManagers;
        float zStep = (EnvBoundaries.Item2.z - EnvBoundaries.Item1.z) / (float)_countChunkManagers;
        int maximumConsumablesInChunk;
        if (_maximumConsumablesInChunk.IsUsed)
            maximumConsumablesInChunk = _maximumConsumablesInChunk.Value;
        else
            maximumConsumablesInChunk = 0;
        for (int i = 0; i < _countChunkManagers; i++)
        {
            for (int j = 0; j < _countChunkManagers; j++)
            {
                _chunkManagers[i, j] = new ChunkManager(
                    (new Vector3(xMin + xStep * i, 0, zMin + zStep * j), new Vector3(xMin + xStep * (i + 1), 0, zMin + zStep * (j + 1))),
                    _penetrability,
                    _terrain,
                    maximumConsumablesInChunk
                );
            }
        }

        InitializePredefined();
        InitializeAnimats();
        InitializeFood();
        TrackObstacles();

        if(Environment.GetEnvironmentVariable("episodes") != null)
            _episodeMax = int.Parse(Environment.GetEnvironmentVariable("episodes"));

        Writer.SetFolderName($"TRAINING");
        // Generate random seed
        UnityEngine.Random.InitState(DateTime.Now.Millisecond);
    }

    private void InitializePredefined()
    {
        foreach (GameObject g in _predefinedObjects)
        {
            g.SetActive(true);
            Consumable c = g.GetComponent<Consumable>();
            if (c != null)
                GetClosestChunkManager(g.transform.position).TrackConsumable(c, c.tag);
        }
    }

    private void InitializeAnimats()
    {
        foreach (Species s in _species)
        {
            // Initialize count at 0 for female/male
            _envState.NSpecies.Add(s.Tag, new int[2] { 0, 0 });
            _speciesDict.Add(s.Tag, s);

            int initialCount = s.InitialCount;
            bool randomizeSex = s.RandomizeSex;
            float ratioMaleFemale = s.RatioMaleFemale;
            for (int i = 0; i < initialCount; i++)
            {
                Animat a = CreateNewAnimat(s, UnityEngine.Random.Range(.6f, 1f));
                if (a == null)
                    break;

                if (!randomizeSex)
                {
                    if ((float)i / (float)initialCount < ratioMaleFemale)
                        a.IsFemale = false;
                    else
                        a.IsFemale = true;
                }
            }
        }      
    }

    private void InitializeFood()
    {
        foreach (FoodType f in _foodTypes)
        {
            _envState.NFood.Add(f.Name, 0);
            
            int initialCount = f.InitialCount;
            for (int i = 0; i < initialCount; i++)
            {
                Consumable c = CreateConsumable(f.Prefab);
                if (c != null && c.GetType().IsSubclassOf(typeof(Crop)))
                    ((Crop)c).Age();
            }
        }
    }

    private void TrackObstacles()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject obstacle in obstacles)
            GetClosestChunkManager(obstacle.transform.position).TrackObstacle(obstacle);
    }
#endregion

#region Step Update

    public virtual void FixedUpdate()
    {
        //_animats.RemoveAll(x => x == null); //TODO: Still needed? If so, call chunkmanagers

        if (_envState.NEpisodes > _episodeMax)
            Exit($"Experiment complete: {_envState.NEpisodes} episodes.");
        else if (_envState.NSpecies.SelectMany(x => x.Value).Sum() < 1)
            Exit($"All agents died. Pausing the editor at iteration {(_envState.NSteps - 1)}.");
        else if (_asyncEpisodes) //TODO: This is untested since a LONG time back. It doesn't seem like this should work...
        {
            ResetSpeciesCount();
            foreach (ChunkManager chunkManager in _chunkManagers)
                chunkManager.ReviveAnimats();
        }
        else
        {
            bool anyActiveAnimats = false;
            foreach (ChunkManager chunkManager in _chunkManagers)
            {
                if (chunkManager.AnyActiveAnimats())
                {
                    anyActiveAnimats = true;
                    break;
                }
            }
            if (!anyActiveAnimats || GetEpisodeStep() == _episodesTimeLimits)
            {
                if (GetEpisodeStep() == _episodesTimeLimits)
                    foreach (ChunkManager chunkManager in _chunkManagers)
                        chunkManager.DisableAnimats();

                if (EpisodeLearning)
                {
                    // Begin new episode
                    Debug.Log($"Episode #{_envState.NEpisodes} ended. Starting new episode at iteration {_envState.NSteps}.");
                    _longestEpisodeLength = GetEpisodeStep() > _longestEpisodeLength ? GetEpisodeStep() : _longestEpisodeLength;
                    _iterationAtLatestEpisodeBegin = _envState.NSteps;
                    _envState.NEpisodes += 1;

                    ResetFoodOnNewEpisode();
                    ResetSpeciesCount();
                    foreach (ChunkManager chunkManager in _chunkManagers)
                        chunkManager.ResetAnimats();
                } 
                else
                    Exit($"All agents died. Pausing the editor at iteration {_envState.NSteps}.");
            }
        }

        _envState.NSteps += 1;
        _envState.NMonths = (_envState.NSteps / MonthLength) % YearLength;
        _envState.NYears  = (_envState.NSteps / (MonthLength * YearLength));

        foreach (FoodType f in _foodTypes)
            if (UnityEngine.Random.value < f.SpawnProbability)
                CreateConsumable(f.Prefab);

        if (_dayLength != 0)
            _theSun.intensity = 0.5f*Mathf.Cos(GetEpisodeStep()  * Mathf.PI / _dayLength) + 0.5001f;

        Writer.WriteInformation(
            "Environment",
            "Environment",
            _envState.GetFullEnvState()
        );
    }

    private int _frames = 0;
    public void Update()
    {
        if (_simInfo != null && _frames++ % 30 == 0)
            _simInfo.UpdateState(_envState.GetFullEnvState(), _envState.GetType().GetFields().Select(x => x.Name));
    }
#endregion

    private void Exit(string messageOnExit)
    {
        /*if (Application.isEditor)
            UnityEditor.EditorApplication.isPlaying = false;
        else
            Application.Quit();*/
        Debug.Log(messageOnExit);
        //Debug.Break();
    }

#region Agent Creation
    /// <summary>
    /// Create a new agent and set its values.
    /// </summary>
    /// <param name="initialEnergy">The energy the animat is born with</param>
    /// <param name="species">Determines the animat's genes and actions space etc. from a prefab</param>
    private Animat CreateNewAnimat(Species species, float initialEnergy)
    {
        Vector3? agentPlacement = GetRandomFreePosition(species.Prefab.gameObject, false, true, true);
        if (!agentPlacement.HasValue)
            return null;

        return CreateNewAnimat(agentPlacement.Value, initialEnergy, species.Prefab, species.ParentNames, species.Tag);
    }

    /// <summary>
    /// Create a new agent and set its values.
    /// </summary>
    /// <param name="pos">The location to spawn the agent</param>
    /// <param name="initialEnergy">The energy the animat is born with</param>
    /// <param name="parentNames">The parent(s) name(s)</param>
    private Animat CreateNewAnimat(Vector3 pos, float initialEnergy, Animat agentPrefab, string[] parentNames = null, string agentTag = null)
    {
        /*if (animats.Count >= maxNbrOfAgents) {
            Debug.LogWarning("Rejected creation of additional agents due to reaching the maximum capacity.")
            return null;
        }*/

        // Decide behavior name
        string[] nameAndType = CreateNewAnimatName(parentNames);

        Animat newAnimat = Instantiate(agentPrefab, transform);
        newAnimat.transform.eulerAngles = new Vector3(0f, newAnimat.transform.eulerAngles.y, 0f);
        newAnimat.AgentID = _agentsCreated;
        
        Dictionary<string, float> energy = new Dictionary<string, float>() { { "energy", initialEnergy } };
        newAnimat.PreInitialize(pos, nameAndType, energy);
        
        ChunkManager chunkManager = GetClosestChunkManager(pos);
        newAnimat.ChunkManager = chunkManager;
        chunkManager.AddAnimat(newAnimat);
        newAnimat.gameObject.SetActive(true);
        
        return newAnimat;
    }

    public float CreateChild(Vector3 pos, Animat[] parents, string[] parentNames = null, bool sexualReproduction = true)
    {
        /*if (animats.Count >= maxNbrOfAgents)
        {
            Debug.LogWarning("Rejected creation of additional agents due to reaching the maximum capacity.")
            return 0f;
        }*/

        // Decide behavior name
        string[] nameAndType = CreateNewAnimatName(parentNames);

        Animat child = Instantiate(_speciesDict[parents[0].tag].Prefab, transform).GetComponent<Animat>();
        child.transform.eulerAngles = new Vector3(0f, child.transform.rotation.y, 0);
        child.AgentID = _agentsCreated;

        // Set inherited parameters
        child.InitialAge = 0;
        child.RandomizeSex = sexualReproduction ? true : false; // randomize sex only if sexual reproduction
        
        Dictionary<string, float> energy = new Dictionary<string, float>() { { "energy", 1f } };
        child.PreInitialize(pos, nameAndType, energy);

        int count = child.HomeostasisFactory.Configs.Count;
        for (int i = 0; i < count; i++)
            child.HomeostasisFactory.Configs[i].Weight =
                UnityEngine.Random.value < 0.5f
                    ? parents[0].HomeostasisFactory.Configs[i].Weight
                    : parents[1].HomeostasisFactory.Configs[i].Weight;
        
        Dictionary<string, (string, bool)>[] reflexes = child.GetReflexes();
        Dictionary<string, (string, bool)>[] parent1Reflexes = parents[0].GetReflexes();
        Dictionary<string, (string, bool)>[] parent2Reflexes = parents[1].GetReflexes();
        count = reflexes.Length;
        for (int i = 0; i < count; i++)
            foreach (string reflexName in reflexes[i].Keys.ToList())
                reflexes[i][reflexName] =
                    UnityEngine.Random.value < 0.5f
                    ? parent1Reflexes[i][reflexName]
                    : parent2Reflexes[i][reflexName];
        child.SetReflexes(reflexes); //might already be done in loop due to passing by reference?
        
        List<float[]> attributes = child.GetGenotype();
        List<float[]> parent1Attributes = parents[0].GetGenotype();
        List<float[]> parent2Attributes = parents[1].GetGenotype();
        count = attributes.Count;
        for (int i = 0; i < count; i++)
            for (int j = 0; j < attributes[i].Length; j++)
                attributes[i][j] =
                    UnityEngine.Random.value < 0.5f
                    ? parent1Attributes[i][j]
                    : parent2Attributes[i][j];
        child.SetGenotype(attributes); //might already be done in loop due to passing by reference?

        ChunkManager chunkManager = GetClosestChunkManager(pos);
        child.ChunkManager = chunkManager;
        chunkManager.AddAnimat(child);
        child.gameObject.SetActive(true);

        // TODO: Verify that phenotype is initialized! Otherwise use genotype value
        return child.Phenotype.Mass * child.Phenotype.OffspringSize;
    }

    /// <summary>
    /// Algorithm for creation of a new agent name. It also updates the number agentsCreated.
    /// </summary>
    /// <param name="parentNames">The name(s) of the parent(s) to the new agent</param>
    /// <returns>The name of the new agent and the agent's type.</returns>
    private string[] CreateNewAnimatName(string[] parentNames)
    {
        if (parentNames == null)
            throw new Exception("Agent name demanded without parent names! Unable to create a new name!");

        // Example name: "Goat_PPO_23-45_67"

        // Extract the parents properties
        List<Dictionary<string, string>> parentProperties = new List<Dictionary<string, string>>(parentNames.Length);
        foreach (string parent in parentNames)
        {
            string[] parentProp = parent.Split('_');
            parentProperties.Add(
                new Dictionary<string, string>(3) {
                    { "type", parentProp[0] },
                    { "model", parentProp[1] },
                    { "ID", parentProp[3] }
                }
            );
        }

        // All types should be the same
        string type = parentProperties[0]["type"];

        // TODO: Take one of the parent's models
        // Take the first parent's model
        string model = parentProperties[0]["model"];

        // Use the parents IDs to build the parents part
        string parents = "";
        foreach (Dictionary<string, string> parent in parentProperties)
        {
            if (parents.Length > 0)
                parents += "-";
            parents += parent["ID"];
        }
        // Add a unique number and return the complete name
        return new string[] { $"{type}_{model}_{parents}_{++_agentsCreated}", type };
    }
#endregion

#region Consumables
    private Consumable CreateConsumable(Consumable prefab)
    {
        Vector3? pos = GetRandomFreePosition(prefab.gameObject, true, false, true);
        if (!pos.HasValue)
            return null;

        if (prefab.GetType().IsSubclassOf(typeof(Crop)))
        {
            Crop c = prefab.GetComponent<Crop>();
            if (!CanSpawnCrop(pos.Value, c))
                return null;
        }
        return Instantiate(prefab, pos.Value, transform.rotation, transform);
    }

    public void TrackConsumable(Consumable consumable, string tag)
    {
        GetClosestChunkManager(consumable.transform.position).TrackConsumable(consumable, tag);
        if (!_envState.NFood.ContainsKey(tag))
            _envState.NFood.Add(tag, 0);
    }

    public void UntrackConsumable(Consumable consumable, string tag)
    {
        GetClosestChunkManager(consumable.transform.position).UntrackConsumable(consumable, tag);
    }

    private void ResetFoodOnNewEpisode()
    {
        if (_envState.NEpisodes == 0)
            return;
            
        // Resetting food for standardization between episodes
        foreach (ChunkManager chunkManager in _chunkManagers)
            chunkManager.DestroyConsumables();
        
        InitializeFood();
    }
#endregion
    public int GetEpisodeStep()
    {
        return _envState.NSteps - _iterationAtLatestEpisodeBegin;
    }

#region Environment State
    public int GetEnvState(string objectType)
    {
        switch (objectType)
        {
            case "NSteps":
                return _envState.NSteps;
            case "NEpisodes":
                return _envState.NEpisodes;
            default:
                throw new Exception($"Tried getting unavailable information {objectType} from GetEnvState!");
        }
    }

    private void ResetSpeciesCount()
    {
        foreach (String species in _envState.NSpecies.Keys)
            for (int i = 0; i < _envState.NSpecies[species].Length; i++)
                _envState.NSpecies[species][i] = 0;
    }

    public void SetEnvStateDelta(string objectType, int delta, bool isFemale = true)
    {
        if (objectType == "NTookAction")
        {
            _envState.NTookAction += delta;
            if (_envState.NTookAction == _envState.NSpecies.Values.SelectMany(x => new int[] { x[0], x[1] }).Sum())
            {

                List<Animat> matingAnimats = new List<Animat>();
                foreach (ChunkManager chunkManager in _chunkManagers)
                    matingAnimats.AddRange(chunkManager.GetMatingAnimats());
                    
                foreach (Animat animat in matingAnimats)
                    animat.Reproduce();

                _envState.NTookAction = 0;
            }
        }
        else if (_envState.NSpecies.ContainsKey(objectType))
        {
            int index = Convert.ToInt32(!isFemale);
            _envState.NSpecies[objectType][index] += delta;
            if (_envState.NSpecies[objectType][index] == 0 && _speciesDict[objectType].RespawnWhenExtinct)
            {
                Animat a = CreateNewAnimat(_speciesDict[objectType], UnityEngine.Random.Range(.6f, 1f));
                if (a == null)
                    return;

                a.IsFemale = isFemale;
            }
        }
        else if (_envState.NFood.ContainsKey(objectType))
            _envState.NFood[objectType] += delta;
        else
            Debug.LogWarning("Unknown objectType " + objectType + " in SetEnvStateDelta");
    }
#endregion

#region Position Checks
    /// <summary>
    /// Searches for a random position that is free from agents and obstacles in the area and returns it. 
    /// </summary>
    /// <returns>A random free position</returns>
    public Vector3? GetRandomFreePosition(GameObject caller, bool fromFood, bool fromAgents, bool fromObstacles)
    {
        int r0 = UnityEngine.Random.Range(0, _countChunkManagers);
        int r1 = UnityEngine.Random.Range(0, _countChunkManagers);
        
        return _chunkManagers[r0, r1].GetRandomFreePosition(caller, fromFood, fromAgents, fromObstacles);
    }

    public ChunkManager GetCurrentChunkManager(ChunkManager chunkManager, Animat animat)
    {
        ChunkManager currentChunkManager = GetClosestChunkManager(animat.transform.position);
        if (chunkManager != currentChunkManager)
        {
            chunkManager.RemoveAnimat(animat);
            currentChunkManager.AddAnimat(animat);
        }
        return currentChunkManager;
    }

    public bool CanSpawnCrop(Vector3 pos, Crop crop)
    {
        return GetClosestChunkManager(pos).CanSpawnCrop(pos, crop);
    }
#endregion

    private ChunkManager GetClosestChunkManager(Vector3 pos)
    {
        return _chunkManagers[
            Mathf.Clamp((int)((pos.x - EnvBoundaries.Item1.x) / (EnvBoundaries.Item2.x - EnvBoundaries.Item1.x) * _countChunkManagers), 0, _countChunkManagers - 1),
            Mathf.Clamp((int)((pos.z - EnvBoundaries.Item1.z) / (EnvBoundaries.Item2.z - EnvBoundaries.Item1.z) * _countChunkManagers), 0, _countChunkManagers - 1)
        ];
    }

    private (int, int) GetClosestChunkManagerIndices(Vector3 pos)
    {
        return (
            Mathf.Clamp((int)((pos.x - EnvBoundaries.Item1.x) / (EnvBoundaries.Item2.x - EnvBoundaries.Item1.x) * _countChunkManagers), 0, _countChunkManagers - 1),
            Mathf.Clamp((int)((pos.z - EnvBoundaries.Item1.z) / (EnvBoundaries.Item2.z - EnvBoundaries.Item1.z) * _countChunkManagers), 0, _countChunkManagers - 1)
        );
    }

    public List<ChunkManager> GetChunkManagersWithinRadius(Vector3 pos, float radius)
    {
        (int, int) currentChunkManager    = GetClosestChunkManagerIndices(pos);
        (int, int) topMostChunkManager    = GetClosestChunkManagerIndices(pos + new Vector3(0, 0, radius));
        (int, int) rightMostChunkManager  = GetClosestChunkManagerIndices(pos + new Vector3(radius, 0, 0));
        (int, int) bottomMostChunkManager = GetClosestChunkManagerIndices(pos + new Vector3(0, 0, -radius));
        (int, int) leftMostChunkManager   = GetClosestChunkManagerIndices(pos + new Vector3(-radius, 0, 0));

        int height = topMostChunkManager.Item2 - bottomMostChunkManager.Item2;
        List<int[]> rangesInsideRadius = new List<int[]>(); 
        
        // Left half
        int i = currentChunkManager.Item1;
        for (int j = 0; j < height; j++)
        {
            rangesInsideRadius.Add(new int[2]);
            while (IsChunkInsideRadius(_chunkManagers[i, j], pos, radius))
            {
                i--;
                if (i < leftMostChunkManager.Item1)
                {
                    i++;
                    break;
                }
            }
            rangesInsideRadius[j][0] = i + 1;
        }
        // Right half
        i = Mathf.Min(currentChunkManager.Item1 + 1, _countChunkManagers - 1);
        for (int j = 0; j < height; j++)
        {
            while (IsChunkInsideRadius(_chunkManagers[i, j], pos, radius))
            {
                i++;
                if (i > rightMostChunkManager.Item1)
                {
                    i--;
                    break;
                }
            }
            rangesInsideRadius[j][1] = i - 1;
        }

        List<ChunkManager> chunkManagers = new List<ChunkManager>();
        for (int j = 0; j < height; j++)
        {
            for (int k = rangesInsideRadius[j][0]; k <= rangesInsideRadius[j][1]; k++)
            {
                chunkManagers.Add(_chunkManagers[k, j + bottomMostChunkManager.Item2]);
            }
        }


        return chunkManagers;

        static bool IsChunkInsideRadius(ChunkManager chunkManager, Vector3 pos, float radius)
        {
            return Vector3.SqrMagnitude(new Vector3(chunkManager.ChunkBoundaries.Item1.x, pos.y, chunkManager.ChunkBoundaries.Item1.z) - pos) < Mathf.Pow(radius, 2)
                || Vector3.SqrMagnitude(new Vector3(chunkManager.ChunkBoundaries.Item2.x, pos.y, chunkManager.ChunkBoundaries.Item1.z) - pos) < Mathf.Pow(radius, 2)
                || Vector3.SqrMagnitude(new Vector3(chunkManager.ChunkBoundaries.Item1.x, pos.y, chunkManager.ChunkBoundaries.Item2.z) - pos) < Mathf.Pow(radius, 2)
                || Vector3.SqrMagnitude(new Vector3(chunkManager.ChunkBoundaries.Item2.x, pos.y, chunkManager.ChunkBoundaries.Item2.z) - pos) < Mathf.Pow(radius, 2)
            ;
        }
    }

    public List<GameObject> GetSmellables(string tag, Vector3 pos, float radius)
    {
        List<GameObject> smellables = new List<GameObject>();
        List<ChunkManager> chunkManagers = GetChunkManagersWithinRadius(pos, radius);
        foreach (ChunkManager chunkManager in chunkManagers)
            smellables.AddRange(chunkManager.GetSmellables(tag));
        return smellables;
    }

    public List<(GameObject, float)> GetSmellVisionables(string tag, Vector3 pos, float radius)
    {
        return GetSmellables(tag, pos, radius).Select(g => (g, GetLightIntensity(g.transform.position))).ToList();
    }
    
    // TODO: Redesign function/verify it works (currently only adjusted to work without errors)
    public float GetLightIntensity(Vector3 pos)
    {
        float lightDepthPenetration = _worldSize.Value.y * 0.05f;
        float lightIntesity = Math.Max(_theSun.intensity * (pos.y - lightDepthPenetration) / (_worldSize.Value.y - lightDepthPenetration), 0.0000001f);
        return lightIntesity;
    }

    // Temporary keeping this function during refactoring
    // TODO: Rework and remove
    public List<Animat> GetNeighbouringAnimatsWithTag(Vector3 pos, string[] tags, float radius)
    {
        List<Animat> nearAnimats = new List<Animat>();
        /*foreach (Animat animat in _animats)
            if (Vector3.SqrMagnitude(animat.transform.position - pos) <= radius*radius + 0.00001f
                && tags.Any(tag => animat.CompareTag(tag))) // Decimals for float precision errors
                    nearAnimats.Add(animat);*/
        
        return nearAnimats;
    }

}
