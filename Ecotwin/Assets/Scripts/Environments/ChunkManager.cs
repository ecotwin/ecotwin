using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChunkManager
{
    private Dictionary<string, List<Animat>> _animats = new Dictionary<string, List<Animat>>();
    private Dictionary<string, List<Consumable>> _consumables = new Dictionary<string, List<Consumable>>();
    public (Vector3, Vector3) ChunkBoundaries { get; private set; }
    private float _collisionTolerance;
    private Terrain _terrain;
    private int _maximumConsumables;
    private List<GameObject> _obstacles = new List<GameObject>();

    public ChunkManager((Vector3, Vector3) chunkBoundaries, float penetrability, Terrain terrain, int maximumConsumables)
    {
        ChunkBoundaries = chunkBoundaries;
        _collisionTolerance = 0.5f - penetrability;
        _terrain = terrain;
        _maximumConsumables = maximumConsumables;
    }

    private const int MaxTrials = 20;

    public Vector3? GetRandomFreePosition(GameObject caller, bool fromFood, bool fromAgents, bool fromObstacles)
    {
        Vector3 pos = new Vector3();
        Vector3 size = caller.transform.localScale;
        int trial = 0;
        do
        {
            trial++;
            pos = new Vector3(
                UnityEngine.Random.Range(ChunkBoundaries.Item1.x + (size.x / 2f), ChunkBoundaries.Item2.x - (size.x / 2f)),
                0,
                UnityEngine.Random.Range(ChunkBoundaries.Item1.z + (size.z / 2f), ChunkBoundaries.Item2.z - (size.z / 2f))
            );
            pos.y = _terrain.SampleHeight(pos) + 0.00001f;
            if (trial > MaxTrials)
            {
                Debug.LogWarning("Couldn't find a random free position within reasonable time. Returning null.");
                return null;
            }
        } while (!IsPositionFree(pos, caller, fromFood, fromAgents, fromObstacles));
        return pos;
    }

    private bool IsPositionFree(Vector3 pos, GameObject caller, bool fromFood, bool fromAgents, bool fromObstacles)
    {
        if (fromFood)
            foreach (Consumable food in _consumables.Values.SelectMany(x => x))
            {
                if (caller == food.gameObject)
                    continue;
                if (IsTooClose(food.transform.position, food.transform.localScale))
                    return false;
            }
        if (fromAgents)
            foreach (Animat animat in _animats.SelectMany(x => x.Value))
            {
                if (caller == animat.gameObject)
                    continue;
                if (IsTooClose(animat.transform.position, animat.transform.localScale))
                    return false;
            }
        if (fromObstacles)
            foreach (GameObject obstacle in _obstacles)
                if (IsTooClose(obstacle.transform.position, obstacle.transform.localScale))
                    return false;
        return true;

        bool IsTooClose(Vector3 otherPos, Vector3 otherSize)
        {
            Vector3 dist = new Vector3(
                Mathf.Abs(otherPos.x - pos.x),
                Mathf.Abs(otherPos.y - pos.y),
                Mathf.Abs(otherPos.z - pos.z)
            );
            // Small vector added to avoid tol.x/y/z == 0 (=> would make any position free)
            Vector3 tol = (caller.transform.localScale + otherSize) * _collisionTolerance + new Vector3(0.0001f, 0.0001f, 0.0001f);
            if (dist.x < tol.x && dist.y < tol.y && dist.z < tol.z)
                return true;
            return false;
        }
    }
    
    public bool CanSpawnCrop(Vector3 pos, Crop crop)
    {
        if (_maximumConsumables > 0 && _consumables.Values.SelectMany(x => x).Count() >= _maximumConsumables)
            return false;
            
        if (!_consumables.ContainsKey("fruit"))
            return true;
        
        if (!IsPositionFree(pos, crop.gameObject, false, false, true))
            return false;

        List<Consumable> crops = _consumables["fruit"];
        bool availablePos = true;
        for (int i = 0; i < crops.Count; i++)
        {
            float sqrDistance = Vector3.SqrMagnitude(pos - crops[i].transform.position);
            if (crops[i].GetType() == crop.GetType())
            {
                if (sqrDistance < Mathf.Pow(((Crop)crops[i]).GraceRadius, 2))
                {
                    availablePos = false;
                    break;
                }
            }
            else if (sqrDistance < Mathf.Pow(((Crop)crops[i]).HostileRadius, 2))
            {
                availablePos = false;
                break;
            }
        }
        return availablePos;
    }

    public void TrackConsumable(Consumable consumable, string tag)
    {
        if (!_consumables.Keys.Contains(tag))
            _consumables.Add(tag, new List<Consumable>());
        _consumables[tag].Add(consumable);
    }

    public void UntrackConsumable(Consumable consumable, string tag)
    {
        if (_consumables.Keys.Contains(tag))
            _consumables[tag].Remove(consumable);
        else
            throw new System.Exception("Attempting to untrack consumable of a tag which has never been tracked!");
    }

    public void TrackObstacle(GameObject obstacle)
    {
        _obstacles.Add(obstacle);
    }

    public void AddAnimat(Animat animat)
    {
        if (!_animats.ContainsKey(animat.tag))
            _animats.Add(animat.tag, new List<Animat>());
        _animats[animat.tag].Add(animat);
    }

    public void RemoveAnimat(Animat animat)
    {
        _animats[animat.tag].Remove(animat);
    }

    /// <summary>
    /// Resets all animats (dead or alive)
    /// </summary>
    public void ResetAnimats()
    {
        foreach (Animat animat in _animats.SelectMany(x => x.Value))
            animat.ResetAgent(animat.gameObject.activeSelf);
    }

    /// <summary>
    /// Resets all dead animats
    /// </summary>
    public void ReviveAnimats()
    {
        foreach (Animat animat in _animats.SelectMany(x => x.Value).Where(x => !x.gameObject.activeSelf))
            animat.ResetAgent(false);
    }

    public void DisableAnimats()
    {
        foreach (Animat animat in _animats.SelectMany(x => x.Value))
            animat.gameObject.SetActive(false);
    }

    public IEnumerable<Animat> GetMatingAnimats()
    {
        return _animats.SelectMany(x => x.Value).Where(y => y != null && (y.IsFemale || y.IsAsexual) && y.IsLookingForMate);
    }

    public bool AnyActiveAnimats()
    {
        return _animats.SelectMany(x => x.Value).Any(a => a != null && a.gameObject.activeSelf);
    }

    public void DestroyConsumables()
    {
        _consumables.Values.SelectMany(x => x).Where(x=> x != null).Select(x=>x.gameObject).ToList().ForEach(UnityEngine.Object.Destroy);
        _consumables = new Dictionary<string, List<Consumable>>();
    }

    public List<GameObject> FindGameObjectsOfTagWithinRadius(string tag, Vector3 point, float radius)
    {
        List<GameObject> gameObjects = new List<GameObject>();
        foreach (Animat animat in _animats.SelectMany(x => x.Value))
            if (animat.tag == tag && Vector3.SqrMagnitude(animat.transform.position - point) <= Mathf.Pow(radius, 2))
                gameObjects.Add(animat.gameObject);

        foreach (Consumable consumable in _consumables.SelectMany(x => x.Value))
            if (consumable.tag == tag && Vector3.SqrMagnitude(consumable.transform.position - point) <= Mathf.Pow(radius, 2))
                gameObjects.Add(consumable.gameObject);
        
        foreach (GameObject obstacle in _obstacles)
            if (obstacle.tag == tag && Vector3.SqrMagnitude(obstacle.transform.position - point) <= Mathf.Pow(radius, 2))
                gameObjects.Add(obstacle);

        return gameObjects;
    }

    public List<GameObject> GetSmellables(string tag)
    {
        List<GameObject> smellables = new List<GameObject>();
        foreach (Animat a in _animats.SelectMany(x => x.Value))
            if (a != null && a.tag == tag)
                smellables.Add(a.gameObject);

        if (!_consumables.Keys.Contains(tag))
            return smellables;

        foreach (Consumable c in _consumables[tag])
            if (c != null)
                smellables.Add(c.gameObject);

        return smellables;
    }
}
