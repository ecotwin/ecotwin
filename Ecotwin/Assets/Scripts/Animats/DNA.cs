/// <summary>
/// The class representing animats' genotype and phenotype
/// The purpose of the class is to facilitate serialization
/// of variables and getting/setting many values at once.
/// All ActionModules and Animat need to define child-classes
/// to make use of this helper class.
/// </summary>
public abstract class DNA
{
    public abstract float[] GetValues();
    public abstract void SetValues(float[] genotype);
}