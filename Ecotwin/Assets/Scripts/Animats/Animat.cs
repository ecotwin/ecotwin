using System;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Assets.Scripts.Agents.Homeostasis;
using Utility;

/// <summary>
/// The agent taking actions in the environment and the variables describing the animal
/// </summary>
public class Animat : Agent
{
    //----------------------------Variables-----------------------------------
#region Agent Specific Data 
    [NonSerialized]
    public int AgentID;

    [Serializable]
    public class AnimatDNA : DNA
    {
        public float Mass;
        public float Size;
        public float Fertility;
        public float OffspringSize;
        public float MeanOffspring;
        public float VarianceOffspring;
        public float SmellRadius;
        public float VisionRadius;
        public float MatingSeason;

        public AnimatDNA()
        { }

        public override float[] GetValues()
        {
            return new float[] { Mass, Size, Fertility, OffspringSize, MeanOffspring, VarianceOffspring, SmellRadius, MatingSeason };
        }

        public override void SetValues(float[] genotype)
        {
            Mass              = genotype[0];
            Size              = genotype[1];
            Fertility         = genotype[2];
            OffspringSize     = genotype[3];
            MeanOffspring     = genotype[4];
            VarianceOffspring = genotype[5];
            SmellRadius       = genotype[6];
            MatingSeason      = genotype[7];
        }

        public void Mutate(float mutationAmount)
        {
            float r = UnityEngine.Random.value;
            if (r < mutationAmount / 2)
                MeanOffspring++;
            else if (r < mutationAmount)
                MeanOffspring = Mathf.Max(1, MeanOffspring - 1);

            r = UnityEngine.Random.value;
            if (r < mutationAmount / 2)
                VarianceOffspring++;
            else if (r < mutationAmount)
                VarianceOffspring = Mathf.Max(0, VarianceOffspring - 1);

            Mass              *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            Size              *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            Fertility         *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            OffspringSize     *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            MeanOffspring     *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            VarianceOffspring *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            SmellRadius       *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
            MatingSeason      *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
        }
    }

    [SerializeField]
    private ConditionalFieldCode<AnimatDNA> _genotype;
    
    [NonSerialized]
    public AnimatDNA Phenotype;
#endregion

#region Death
    [Tooltip("The current age of an animat (in timesteps).")]
    private int _age = 0;
    public int Age { get { return _age; } }

    [Min(0f)] [Tooltip("The starting age of an animat (use if an animat should be spawned as an adult")]
    public int InitialAge;

    [SerializeField] [Min(0f)] [Tooltip("The maximum age of an animat (the animat dies when it reaches this age)")]
    private int _maximumAge;

    [SerializeField] [Tooltip("Should the animat be revived if it dies?")]
    private bool _isImmortal = false;

    [SerializeField] [Min(0f)] [Tooltip("The punishment given to the agent if it dies.")]
    private float _punishmentForDying = 0f;

    [SerializeField] [Tooltip("Whether this animat turns into a meat object "
        + "when dying by unnatural causes (killed).")]
    private bool _createCorpseOnUnnaturalDeath = false;
    
    [SerializeField] [Tooltip("Whether this animat turns into a meat object "
        + "when dying from natural causes (homeostatics).")]
    private bool _createCorpseOnNaturalDeath = false;

    [SerializeField]
    private GameObject _corpsePrefab;

    private bool _unnaturalDeathHappened;
#endregion

#region Reproduction
    private float _fertilityOfMate = 0f;

    [SerializeField]
    private float _speciesLibido = 0.023f;

    [SerializeField]
    private float _speciesFertilityLibidoOffset = 0.75f;

    [SerializeField]
    private float _speciesRecoveryPregnancy = 9f;

    [SerializeField]
    private int _maturityAge;

    private bool _mature = false;

    public bool IsFemale = true;
    
    [NonSerialized]
    public bool IsAsexual;

    public bool RandomizeSex = true;

    [NonSerialized]
    public bool IsLookingForMate = false;

    [SerializeField]
    private int _timeToLookForMate = 10;

    [NonSerialized]
    public int StepLookingForMate = 0;

    private int _recoveryPregnancy;

    private List<Animat> _possibleMates = new List<Animat>() { };

    [SerializeField]
    private const float MutationAmount = 0.1f;
#endregion

#region Eating
    [NonSerialized]
    public List<Consumable> NearbyConsumables = new List<Consumable>();
#endregion

#region Actions

    [SerializeField]
    private ActionModule[] _actionModules;
    
    [SerializeField] [Tooltip("Leave null if unable to touch")]
    private Touch _touch;
    
    [SerializeField] [Tooltip("Leave null if unable to smell")]
    private Smell _smell;

    private PreprocessedVision _preprocessedVision;
    
    [Tooltip("Leave null if unable to sense light")]
    public LightSense LightSense;

    private ActionHandler _actionHandler;
    
    [SerializeField] [Tooltip("The time between two decision when using inference or heuristics.")]
    private float _timeBetweenDecisionsAtInference = .2f;

    /// <summary>
    /// The time that the last action was taken. 
    /// This variable is used to make sure there is enough time between actions when using inference or heuristics. 
    /// </summary>
    private float _timeForLastAction;

#endregion
    
#region Observations
    [SerializeField] [Tooltip("Game objects with tag that are sensed by the animat.")]
    private ConditionalField<ObservableTag>[] _observableTags;
    
    private List<ObservableTag> _observableTagsList = new List<ObservableTag> { };
    
    public List<float> SmellObservation { get; private set; }
    
    private List<float> _touchObservation;
    
    private List<float> _homeostasisObservation;
    
    public List<float> VisionObservation { get; private set; } = new List<float>();

    public Vector3 ClosestColor { get; private set; } = new Vector3(-1, -1, -1);

    private List<Vector3> _closestColors = new List<Vector3>();
    public List<Vector3> ClosestColors { get { return new List<Vector3>(_closestColors); } private set { _closestColors = value; } }

    private RayPerceptionSensorComponent3D _raySensor;
#endregion

#region Healthbars
    [Serializable]
    public struct NamedSlider
    {
        public string name;
        public Slider slider;
    }

    [SerializeField] [Tooltip("Bars showcasing the real-time values of the animat's homeostatic and sensory variables. "
        + "SliderBar's Name has to be equal to the name of an homeostatic variable or a sensory variable.")]
    private NamedSlider[] _sliderBars;

    //The slider that should be attached to this StatusBarCanvas object.
    private Dictionary<string, Slider> _sliders = new Dictionary<string, Slider> { };
#endregion
    
#region Homeostasis
    public HomeostasisModule Homeostasis { get; private set; }
    
    [SerializeField]
    private HomeostasisFactory _homeostasisFactory;
    public HomeostasisFactory HomeostasisFactory { get { return _homeostasisFactory; } private set { _homeostasisFactory = value; } }

    private Dictionary<string, float> _initialHomeostasis;
#endregion
    
#region Energy Consumption

    /// <summary>
    /// The energy an animat loses every time step.
    /// This value is a constant multiplied by the animats' M^(3/4).
    /// </summary>
    public float Bmr = 0.0002f;
#endregion
    
#region Miscellaneous    
    private string _agentType;

    public AreaManager AreaManager { get; private set; }
    
    public ChunkManager ChunkManager;

    public SerializableDictionary<string, SerializableDictionary<string, float>> _nutrientsEffectMap = new SerializableDictionary<string, SerializableDictionary<string, float>>();
    public Dictionary<string, Dictionary<string, float>> NutrientsEffectMap {
        get {
            var nem = _nutrientsEffectMap;
            var ret = new Dictionary<string, Dictionary<string, float>>();
            foreach (KeyValuePair<string, SerializableDictionary<string, float>> em in nem)
                ret.Add(em.Key, new Dictionary<string, float>(em.Value));
            return ret; 
        }
        private set {
            var nem = new SerializableDictionary<string, SerializableDictionary<string, float>>();
            foreach (KeyValuePair<string, Dictionary<string, float>> em in value)
                nem.Add(em.Key, new SerializableDictionary<string, float>(em.Value));
        }
    }

    public bool LightSensitive = false;

    public float MinVisibleLight = 0.2f;

    [SerializeField] [Tooltip("Age when agent's reflexes are disabled")]
    private ConditionalField<int> _disableReflexesOnAge;

    private int _monthLength;

    private int _yearLength;
    
    private bool _doLifelongLearning;

    private bool _episodeLearning;

    private Vector3 _startPosition;
#endregion

    //----------------------------Methods-------------------------------------

#region Initialization  
    /// <summary>
    /// Initializes all the animat's components which must be active PRIOR to the gameobject being activated.
    /// Is called from AreaManager during animat's creation.
    /// </summary>
    public void PreInitialize(Vector3 pos, string[] nameAndType, Dictionary<string, float> homeostasis)
    {
        _startPosition = pos + new Vector3(0, 1, 0);
        _agentType = nameAndType[1];
        name = nameAndType[0];
        _initialHomeostasis = homeostasis;
        if (_initialHomeostasis.Count < _homeostasisFactory.Configs.Count)
            foreach (var config in _homeostasisFactory.Configs)
                if (!_initialHomeostasis.ContainsKey(config.Name))
                    _initialHomeostasis.Add(config.Name, config.InitialValue);
    #region Homeostasis Initialization (TODO: SERIALIZE) (WIP)
        _homeostasisFactory.AddLogarithmicUtilityFunction(homeostaticVariableName: "energy", weight: _homeostasisFactory.Configs[0].Weight, isSensable: true)
                           //.AddLinearUtilityFunction(homeostaticVariableName: "libido", slope: 1, intercept: 1, sign: -1, weight: _homeostasisFactory.Configs[1].Weight, isSensable: true)
                           .AddLogarithmicUtilityFunction(homeostaticVariableName: "water", weight: _homeostasisFactory.Configs[2].Weight, isSensable: true)
                           .AddHappinessAggregation("prod");
        
        Homeostasis = _homeostasisFactory.Build();
    #endregion

        AreaManager  = GetComponentInParent<AreaManager>();
        _monthLength = AreaManager.MonthLength;
        _yearLength  = AreaManager.YearLength;
        _doLifelongLearning = AreaManager.DoLifelongLearning;
        _episodeLearning    = AreaManager.EpisodeLearning;
        Phenotype = new AnimatDNA();
        
        if (_actionHandler == null)
            _actionHandler = new ActionHandler(this, _actionModules);

        _observableTagsList = new List<ObservableTag>();
        for (int i = 0; i < _observableTags.Count(); i++)
            if (_observableTags[i].IsUsed)
                _observableTagsList.Add(_observableTags[i].Value);

        _raySensor  = GetComponent<RayPerceptionSensorComponent3D>();
        _touch = new Touch(_observableTagsList.FindAll(x => x.IsDetectedByTouch));
        LightSense = new LightSense(AreaManager, Homeostasis, this);
        _smell = new Smell(_observableTagsList.FindAll(x => x.IsDetectedBySmell), AreaManager, this);
        _preprocessedVision = new PreprocessedVision(_observableTagsList.FindAll(x => x.IsDetectedByPreprocessedVision), AreaManager, this, 1, Phenotype.VisionRadius);

        // Automatically set the observation size to number of sensor inputs
        BehaviorParameters behaviorParameters = GetComponent<BehaviorParameters>();
        behaviorParameters.BehaviorName = _agentType;
        behaviorParameters.BrainParameters.VectorObservationSize = CollectSenses().Count;
        behaviorParameters.BrainParameters.ActionSpec = ActionSpec.MakeDiscrete(_actionHandler.ActionSpaceSize);
    }

    /// <summary>
    /// Initializes all parts of the animat which only need initializing/resetting exactly once.
    /// </summary>
    public override void Initialize()
    {
        _genotype.Disable();
        IsFemale = RandomizeSex ? (UnityEngine.Random.value < 0.5 ? true : false) : IsFemale;

        foreach (NamedSlider namedSlider in _sliderBars)
            _sliders[namedSlider.name] = namedSlider.slider;

    #region Mutation
        for (int i = 0; i < _homeostasisFactory.Configs.Count; i++)
        {
            _homeostasisFactory.Configs[i].Weight =
                Mathf.Max(0f, Mathf.Min(1f, _homeostasisFactory.Configs[i].Weight * UnityEngine.Random.Range(1 - MutationAmount, 1 / (1 - MutationAmount))));
        }
        _genotype.Value.Mutate(MutationAmount);
        _actionHandler.Mutate(MutationAmount);
        // Initialization Phenotype
        Phenotype.SetValues(_genotype.Value.GetValues());
    #endregion

        InitializeModules();

        Writer.SetFolderName(_agentType);
    }

    /// <summary>
    /// Initializes/resets all parts of the animat which must be initialized/reset more than once.
    /// </summary>
    private void InitializeModules()
    {
        Phenotype.Fertility = 0.0f;

        IsLookingForMate = false;
        _possibleMates.Clear();
        _mature = false;
        _recoveryPregnancy = 0;
        _age = InitialAge;
        _unnaturalDeathHappened = false;

        Homeostasis.Update(_initialHomeostasis);
        AreaManager.SetEnvStateDelta(name.Split('_')[0], +1, IsFemale);
        ChunkManager = AreaManager.GetCurrentChunkManager(ChunkManager, this);

        if (NutrientsEffectMap == null)
            throw new Exception("Invalid nutrition map");

        CharacterController c = GetComponent<CharacterController>();
        if (c != null)
            c.enabled = false;
        transform.rotation = Quaternion.identity;
        transform.position = _startPosition;        
        if (c != null)
            c.enabled = true;

        gameObject.SetActive(true);
    }

    /// <summary>
    /// Resets the agent to its initial state at a random position in the game area.
    /// </summary>
    public void ResetAgent(bool initialize)
    {
        RandomizeSex = false;
        BehaviorParameters agentBehaviorParameters = GetComponent<BehaviorParameters>();
        if (_doLifelongLearning)
            agentBehaviorParameters.BehaviorName = _agentType + AreaManager.GetEnvState("NEpisodes");
        else
            agentBehaviorParameters.BehaviorName = _agentType;

        // Reset variables
        _timeForLastAction = Time.time;

        if (initialize)
        {
            Vector3? pos = AreaManager.GetRandomFreePosition(gameObject, false, true, true);
            if (pos.HasValue)
                _startPosition = pos.Value + new Vector3(0, 1, 0);
            InitializeModules();
        }

        UpdateStatusBars();
    }
#endregion

#region Observations
    /// <summary>
    /// Gathers the animat's observations from its various senses
    /// </summary>
    private List<float> CollectSenses()
    {
        List<float> observations = new List<float>();
        SmellObservation = _smell.GetSense();
        observations.AddRange(SmellObservation);

        observations.AddRange(LightSense.GetSense());

        _touchObservation = _touch.GetSense().Select(x => x ? 1f : 0f).ToList();
        observations.AddRange(_touchObservation);

        _homeostasisObservation = Homeostasis.GetSense().ToList();
        //Debug.Log(homeostatisObservation.Count());
        observations.AddRange(_homeostasisObservation);
        // Vision unsing Ray Sensors are added automatically through ML-Agents

        observations.AddRange(_preprocessedVision.Observe());

        RayPerceptionOutput obs = RayPerceptionSensor.Perceive(_raySensor.GetRayPerceptionInput());

        VisionObservation = new List<float>();
        foreach (RayPerceptionOutput.RayOutput r in obs.RayOutputs)
        {
            if (r.HitTagIndex == 0)
                VisionObservation.Add(r.HitFraction);
            else
                VisionObservation.Add(-1f);
        }

        Color hitColor = new Color(1, 1, 1, 0);
        GameObject hitGameObject = obs.RayOutputs[0].HitGameObject;
        if (hitGameObject != null)
        {
            Renderer renderer = hitGameObject.GetComponent<Renderer>();
            if (renderer != null)
                hitColor = renderer.material.color;
            else
            {
                Renderer childRenderer = hitGameObject.GetComponentInChildren<Renderer>();
                if (childRenderer != null)
                    hitColor = childRenderer.material.color;
            }
        }

        observations.Add(hitColor.r);
        observations.Add(hitColor.g);
        observations.Add(hitColor.b);
        observations.Add(hitColor.a);

        observations.Add(ClosestColor.x);
        observations.Add(ClosestColor.y);
        observations.Add(ClosestColor.z);
        
        observations.Add(_fertilityOfMate);

        return observations;
    }

    /// <summary>
    /// Called via ML-agents to give the agent its observations
    /// </summary>
    public override void CollectObservations(VectorSensor sensor)
    {        
        List<float> senses = CollectSenses();
        foreach(float f in senses)
            sensor.AddObservation(f);
    }

    public void ResetCloseColor(Vector3 c)
    {
        ClosestColor = new Vector3(-1, -1, -1);
        _closestColors.Remove(c);
    }
#endregion

#region Reflexes
    /// <summary>
    /// Prohibits certain actions available to the animat. Actions can be forced by prohibiting all other actions.
    /// </summary>
    public override void WriteDiscreteActionMask(IDiscreteActionMask actionMasker)
    {
        // TODO: make decisions based on input
        if (_disableReflexesOnAge.IsUsed && _age >= _disableReflexesOnAge.Value)
            return;

        List<List<int>> actionMasks = _actionHandler.GetActionMasks();
        for (int i = 0; i < actionMasks.Count; i++)
            for (int j = 0; j < actionMasks[i].Count; j++)
                actionMasker.SetActionEnabled(i, actionMasks[i][j], false);
    }

    public Dictionary<string, (string, bool)>[] GetReflexes()
    {
        return _actionHandler.GetReflexes();
    }

    public void SetReflexes(Dictionary<string, (string, bool)>[] reflexes)
    {
        _actionHandler.SetReflexes(reflexes);
    }
#endregion

#region Handle Action
    /// <summary>
    /// The core of the animat's behaviour.
    /// The animat takes an action from its policy network which should be taken in its environment.
    /// At the same time, the animat's state is updated such as maturing and aging.
    /// </summary>
    public override void OnActionReceived(ActionBuffers vectorAction)
    {
        Homeostasis.DeltaStep();
        Mature();
        if (++_age - StepLookingForMate > _timeToLookForMate)
            IsLookingForMate = false;

        ActionSegment<int> actions = vectorAction.DiscreteActions;
        if (Academy.Instance.IsCommunicatorOn ||
            (Time.time - _timeForLastAction) > _timeBetweenDecisionsAtInference)
            if (_actionHandler.HandleActions(actions))
                _timeForLastAction = Time.time;

        ChunkManager = AreaManager.GetCurrentChunkManager(ChunkManager, this); //Updates which animats are in which chunks after they move
        if (!Homeostasis.IsAlive() || (_maximumAge > 0 && _age > _maximumAge))
        {
            AreaManager.SetEnvStateDelta(name.Split('_')[0], -1, IsFemale);
            
            bool makeMeat = (_unnaturalDeathHappened && _createCorpseOnUnnaturalDeath) || (!_unnaturalDeathHappened && _createCorpseOnNaturalDeath);
            if (!Homeostasis.IsAlive())
                AddReward(-_punishmentForDying);
            
            if (makeMeat)
            {
                GameObject g = Instantiate(_corpsePrefab, transform.position, transform.rotation, AreaManager.transform);
                g.GetComponent<Meat>().ReduceInitialEnergy(_age >= _maturityAge ? 1f : _age / _maturityAge);
            }

            if (_isImmortal)
            {
                ResetAgent(true);
                //if (name.Contains("Food")) // TODO: Remove? Still needed?
                //    transform.position = AreaManager.GetRandomFreePosition(gameObject, false, true, true).Value;
                EndEpisode();
            }
            else
            {
                gameObject.SetActive(false);
                if (!_episodeLearning)
                {
                    ChunkManager.RemoveAnimat(this);
                    Destroy(gameObject);
                }

                return;
            }
        }
        AreaManager.SetEnvStateDelta("NTookAction", +1);
        float r = Homeostasis.Reward();
        AddReward(r);






        // Update the UI
        UpdateStatusBars();

        // Write agent status
        Dictionary<string, string> agentState = Homeostasis.GetState().ToDictionary(x => x.Key, x => x.Value.ToString());
        agentState["episode"] = AreaManager.GetEnvState("NEpisodes").ToString();
        agentState["step"] = AreaManager.GetEnvState("NSteps").ToString();
        agentState["cumulative_reward"] = GetCumulativeReward().ToString();
        agentState["x"] = transform.position.x.ToString();
        agentState["y"] = transform.position.y.ToString();
        agentState["z"] = transform.position.z.ToString();
        agentState["actions"] = String.Join(";", _actionHandler.GetNames(actions));
        agentState = agentState.Concat(GetAgentState()).ToDictionary(k => k.Key, v => v.Value);

        Writer.WriteInformation(_agentType, name, agentState);

        /// Updates the animat's phenotype based on its genotype and age.
        /// Also updates the animat's fertility and libido (if the animat has reached full maturity).
        void Mature()
        {
            if (!_mature)
            {
                if (_age >= _maturityAge)
                {
                    if (!IsFemale)
                        Phenotype.Fertility = _genotype.Value.Fertility;
                    _mature = true;
                }
                float maturityProportion = Mathf.Max(_genotype.Value.OffspringSize, (float)_age / (float)_maturityAge);
                float newMass = _genotype.Value.Mass * maturityProportion;
                if (_age > 0)
                    Homeostasis.SetValue("energy", Homeostasis.GetValue("energy") * Phenotype.Mass / newMass);
                Phenotype.Mass = newMass;
                transform.localScale = new Vector3(1, 1, 1) * Mathf.Max(0.2f, _genotype.Value.Size * maturityProportion);
                Phenotype.SmellRadius = _genotype.Value.SmellRadius * Mathf.Max(maturityProportion, 0.5f);
                _actionHandler.Mature(maturityProportion);
                Homeostasis.ChangeDeltaValue("energy", -Bmr * Mathf.Pow(newMass, 3f / 4f));
            }
            if (_mature)
            {
                if (--_recoveryPregnancy < 0)
                {
                    int nSteps = AreaManager.GetEpisodeStep();
                    int period = _monthLength * _yearLength;
                    if (IsFemale)
                    {
                        Phenotype.Fertility = _genotype.Value.Fertility
                            * ((Mathf.Sqrt(
                                (17 / (1 + 16 * Mathf.Pow(
                                    Mathf.Cos(
                                        2 * Mathf.PI * nSteps
                                        / period
                                        + 2 * Mathf.PI * (_speciesFertilityLibidoOffset + Phenotype.MatingSeason)
                                    ),
                                    2
                                )))
                            ) * Mathf.Cos(
                                2 * Mathf.PI * nSteps
                                / period
                                + 2 * Mathf.PI * (_speciesFertilityLibidoOffset + Phenotype.MatingSeason)
                            ) + 1) / 2);
                    }
                    float uncheckedDelta = _speciesLibido * Mathf.Cos(
                                2 * Mathf.PI * nSteps
                                / period + 2 * Mathf.PI * Phenotype.MatingSeason
                            ) * 2 * Mathf.PI / period;
                    float libido = _speciesLibido
                        * Mathf.Sin(
                            2 * Mathf.PI * nSteps
                            / period + 2 * Mathf.PI * Phenotype.MatingSeason
                        );
                    if (uncheckedDelta >= 0f || libido <= 1f)
                        Homeostasis.ChangeDeltaValue("libido", uncheckedDelta);
                    else
                        Homeostasis.ChangeDeltaValue("libido", 0f);
                }
            }
        }
    }

    /// <summary>
    /// Controls the animat's actions by user input
    /// </summary>
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var discreteActionsOut = actionsOut.DiscreteActions;
        int[] heuristicActions = _actionHandler.GetHeuristics();
        for (int i = 0; i < heuristicActions.Length; i++)
            discreteActionsOut[i] = heuristicActions[i];
    }
#endregion

#region Collision-detection
    /// <summary>
    /// Collision-detection of the animat, used to check which consumables are nearby,
    /// and which animats are available for mating.
    /// </summary>
    public virtual void OnTriggerEnter(Collider collider)
    {
        _touch.OnTouch(collider);
        if (collider.CompareTag("reproductionZone"))
        {
            Animat mate = collider.GetComponentInParent<Animat>();
            if (mate.tag == tag &&
                mate.IsFemale != IsFemale &&
                !_possibleMates.Contains(mate))
            {
                _touch.OnTouch(mate.GetComponent<Collider>());
                _possibleMates.Add(mate);
                _fertilityOfMate = mate.Phenotype.Fertility;
            }
        }
        else
        {
            Consumable c = collider.GetComponent<Consumable>();
            if (c == null)
                return;
            foreach (ObservableTag o in _observableTagsList)
            {
                if (o.Tag == c.tag)
                {
                    NearbyConsumables.Add(c);
                    Renderer renderer = c.GetComponent<Renderer>();
                    if (renderer != null)
                    {
                        float r = renderer.material.color.r;
                        float g = renderer.material.color.g;
                        float b = renderer.material.color.b;

                        ClosestColor = new Vector3(r, g, b);
                        _closestColors.Add(new Vector3(r, g, b));
                    }
                    else if (c.GetType().IsSubclassOf(typeof(Water)))
                    {
                        ClosestColor = new Vector3(0, 0, 1);
                        _closestColors.Add(new Vector3(0, 0, 1));
                    }
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Handles the unregistering of colliding objects upon ending a collision.
    /// </summary>
    public virtual void OnTriggerExit(Collider collider)
    {
        _touch.OnTouchStop(collider);
        if (collider.CompareTag("reproductionZone"))
        {
            Animat animat = collider.GetComponentInParent<Animat>();
            if (animat.tag == tag && _possibleMates.Contains(animat))
            {
                _possibleMates.Remove(collider.GetComponentInParent<Animat>());
                if (_possibleMates.Count == 0)
                    _fertilityOfMate = 0;
                else
                    _fertilityOfMate = _possibleMates[0].Phenotype.Fertility;
            }
        }
        else
        {
            Consumable c = collider.GetComponent<Consumable>();
            NearbyConsumables.Remove(c);
            Renderer renderer = collider.GetComponent<Renderer>();
            if (renderer != null)
            {
                ResetCloseColor(
                    new Vector3(
                        renderer.material.color.r,
                        renderer.material.color.g,
                        renderer.material.color.b
                    )
                );
            }
            else if (c != null && c.GetType().IsSubclassOf(typeof(Water)))
                ResetCloseColor(new Vector3(0, 0, 1));
        }
    }
#endregion

#region Agent State
    /// <summary>
    /// Gets all the genetic attributes of the such that it can be printed or saved.
    /// </summary>
    private Dictionary<string, string> GetAgentState()
    {
        Dictionary<string, string> d = new Dictionary<string, string>();
        /*foreach (string trait in _phenotype.Keys)
            d.Add($"Attribute_{trait}", $"{_phenotype[trait]}");*/

        //TODO: fix with new dictionary structure 
        /*List<float[]> geneticActionAttributes = _actionHandler.GetGeneticAttributes();
        for (int i = 0; i < geneticActionAttributes.Count; i++)
            for (int j = 0; j < geneticActionAttributes[i].Length; j++)
                d.Add($"Action_Attribute_{i}_{j}", $"{geneticActionAttributes[i][j]}");
        */
        //for (int i = 0; i < GeneticReflexes.Length; i++)
        //    d.Add($"Reflex_{i}", $"{GeneticReflexes[i]}");

        return d;
    }

    /// <summary>
    /// Represents the animat's homeostatic values visually using health bars
    /// </summary>
    private void UpdateStatusBars()
    {
        foreach (KeyValuePair<string, float> hValue in Homeostasis.GetState())
            if (_sliders.ContainsKey(hValue.Key))
                _sliders[hValue.Key].value = hValue.Value; 
    }
#endregion    

#region Reproduction
    /// <returns>The male with the highest product of energy and fertility, who is looking to mate.</returns>
    private Animat FindMate()
    {
        if (IsAsexual)
            return this;

        IEnumerable<Animat> mates = _possibleMates
            .Where(x => x != null && !x.IsAsexual && x.IsLookingForMate)
            .OrderBy(y => y.Homeostasis.GetValue("energy") * y.Phenotype.Fertility)
            .Select(x => x);

        if (mates == null || !mates.Any())
            return null;

        return mates.Last();
    }

    /// <summary>
    /// Lets animats reproduce either asexually or sexually.
    /// mate == this => Asexual
    /// mate != this => Sexual
    /// </summary>
    public void Reproduce()
    {
        Animat mate = FindMate();
        if (mate == null)
            return;
        
        bool isFertilized;
        Homeostasis.SetValueDelta("energy", -0.0001f);
        if (this != mate)
        {
            this.IsLookingForMate = false;
            mate.IsLookingForMate = false;
            mate.Homeostasis.SetValue("libido", 0f); //Only male libido does not depend on having a baby
            mate.Homeostasis.SetValueDelta("energy", -0.0001f);
            isFertilized = UnityEngine.Random.value <= Phenotype.Fertility * mate.Phenotype.Fertility;
        }
        else
            isFertilized = UnityEngine.Random.value <= Phenotype.Fertility;
        
        if (isFertilized)
        {
            int numberOfOffspring = 
                Mathf.RoundToInt(Phenotype.MeanOffspring
                + UnityEngine.Random.Range(-Phenotype.VarianceOffspring, Phenotype.VarianceOffspring));
            for (int i = 0; i < numberOfOffspring; i++)
            {
                if (!_isImmortal)
                {
                    float offspringMass = AreaManager.CreateChild(transform.position, new Animat[] { this, mate }, new string[] { name, mate.name }, true);
                    Homeostasis.SetValueDelta("energy", -offspringMass / Phenotype.Mass);
                }
                else
                    Homeostasis.SetValueDelta("energy", -Phenotype.OffspringSize * _genotype.Value.Mass / Phenotype.Mass);
                if (Homeostasis.GetValue("energy") < 0.00001f)
                    break;
            }
            Homeostasis.SetValue("libido", 0f);
            Homeostasis.ChangeDeltaValue("libido", 0f);
            Phenotype.Fertility = 0f;
            _recoveryPregnancy = Mathf.RoundToInt(_speciesRecoveryPregnancy * _monthLength);
        }
    }
#endregion

#region Genotype
    public List<float[]> GetGenotype()
    {
        List<float[]> genotype = new List<float[]>();
        genotype.Add(_genotype.Value.GetValues());
        genotype.AddRange(_actionHandler.GetGenotype());
        return genotype;
    }

    public void SetGenotype(List<float[]> genotype)
    {
        _genotype.Value.SetValues(genotype[0]);
        genotype.RemoveAt(0);
        _actionHandler.SetGenotype(genotype);
    }
#endregion

    /// <summary>
    /// Deals damage to the agent equal to <paramref name="attackDamage" /> * <paramref name="attackerMass" /> / rigidbody.mass
    /// If the animat dies, the death is registered as unnatural which can be used to determine if a corpse should be created.
    /// </summary>
    /// <returns>Is the agent killed</returns>
    public bool Damage(float attackDamage, Animat attacker)
    {
        // Assumption: The damage dealt is random between 0 and the predator's attack force
        float damage = attackDamage * (attacker.Phenotype.Mass / Phenotype.Mass);
        Homeostasis.SetValueDelta("energy", -1 * damage); 
        if (!Homeostasis.IsAlive())
            _unnaturalDeathHappened = true;

        return Homeostasis.IsAlive();
    }
}