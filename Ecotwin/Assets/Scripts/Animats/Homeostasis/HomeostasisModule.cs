using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Agents.Homeostasis;

/// <summary>
/// The class keeping track of an animat's homeostasis (e.g. hunger, thirst, libido etc.)
/// The reward to give the agent is calculated directly based on the homeostasis, and no
/// other rewards should be given at any time. Only AddReward(HomeostasisModule.Reward())
/// should be used (once per step).
/// </summary>
public class HomeostasisModule
{
    //----------------------------PARAMETERS-------------------------------------
    
    //----------------------------DICTIONARIES-----------------------------------
    // is the variable a critical variable? I.e. can the agent die because of it
    public Dictionary<string, bool> IsCritical = new Dictionary<string, bool>();

    // values of each homeostatic variable for a particular state
    public Dictionary<string, float> HomeostaticsDict = new Dictionary<string, float>();

    // Sensory variables used for the Anticipation Term
    private Dictionary<string, float> _sensesDict = new Dictionary<string, float>();

    // values of step change
    private Dictionary<string, float> _hDeltaStep = new Dictionary<string, float> { };

    // the life-death boundaries for critical variables
    private Dictionary<string, float> _deathLB = new Dictionary<string, float>();
    private Dictionary<string, float> _deathUB = new Dictionary<string, float>();

    // the domain boundaries
    private Dictionary<string, float> _domainLB = new Dictionary<string, float>();
    private Dictionary<string, float> _domainUB = new Dictionary<string, float>();

    private IHappiness _happiness;
    private Anticipation _anticipation = null;
    private List<string> _senseVariables;
    private HashSet<string> _ignore;

    private float _positiveWeight = 1f;
    private float _negativeWeight = 1f;

    public HomeostasisModule(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict, HashSet<string> ignore, Dictionary<string, float> hDeltaStep, List<string> senseVariables, IHappiness happiness = null, Anticipation anticipation = null)
    {
        HomeostaticsDict = homeostaticsDict;
        _sensesDict = sensesDict;
        _hDeltaStep = hDeltaStep;
        _happiness = happiness;
        _anticipation = anticipation;
        _senseVariables = senseVariables;
        _ignore = ignore;
    }

    public void SetHappinessAsymmetry(float positiveWeight = 1f, float negativeWeight = 1f)
    {
        _positiveWeight = positiveWeight;
        _negativeWeight = negativeWeight;
    }

    public void SetCritical(Dictionary<string, bool> isCritical)
    {
        IsCritical =  isCritical;
    }

    public void SetDomains(Dictionary<string, float> deathLB, Dictionary<string, float> deathUB, Dictionary<string, float> domainLB, Dictionary<string, float> domainUB)
    {
        _deathLB = deathLB;
        _deathUB = deathUB;
        _domainUB = domainUB;
        _domainLB = domainLB;
    }

    public void SetHappiness(IHappiness happiness)
    {
        _happiness = happiness;
    }

    public void EnableAnticipation(Anticipation anticipation)
    {
        _anticipation = anticipation;
    }

    public float[] GetSense()
    {
        ProjectToDomain();
        List<float> senses = new List<float>();
        foreach(string v in _senseVariables)
        {
            if (HomeostaticsDict.ContainsKey(v))
                senses.Add(HomeostaticsDict[v]);
            else
                senses.Add(_sensesDict[v]);
        }
        senses.AddRange(_happiness.GetSense(HomeostaticsDict, _sensesDict).Select(x => x.Value).ToList());

        if (_anticipation != null)
            senses.AddRange(_anticipation.GetSense());

        return senses.ToArray();
    }

    public Dictionary<string, float> GetSenseDictionary()
    {
        ProjectToDomain();
        return _happiness.GetSense(HomeostaticsDict, _sensesDict);
    }
       
    /// <summary>
    /// Calculates the reward from drive-reduction theory.
    /// </summary>
    /// <returns>Reward</returns>
    public float Reward()
    {
        ProjectToDomain();
        float reward = _happiness.GetReward(HomeostaticsDict, _sensesDict);

        if (_anticipation != null)
            reward += _anticipation.GetAnticipation(HomeostaticsDict,_sensesDict);

        return reward > 0? _positiveWeight*reward : _negativeWeight*reward;
    }

    /// <summary>
    /// Returns a dictionary of names and values for the current homeostatic variables, sensory variables and drive.
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, float> GetState()
    {
        // Send homeostatic variables
        Dictionary<string, float> stateToSend = new Dictionary<string, float>(HomeostaticsDict);
        // Send sensory variables
        foreach (var sense in _sensesDict)
            stateToSend.Add(sense.Key, sense.Value);

        return stateToSend;
    }

    /// <summary>
    /// Updates the homeostatic variables according to the delta value specified in hDeltaStep
    /// </summary>
    public void DeltaStep()
    {
        foreach (KeyValuePair<string, float> hDelta in _hDeltaStep)
            SetValueDelta(hDelta.Key, hDelta.Value);
    }

    /// <summary>
    /// Updates all homeostatic values to a given Dictionary's values
    /// </summary>
    /// <param name="updatedValues"></param>
    public void Update(Dictionary<string, float> updatedValues)
    {
        foreach (KeyValuePair<string, float> entry in updatedValues)
            SetValue(entry.Key, entry.Value);
    }

    /// <summary>
    /// Get the current value for an homeostatic or sensory variable <paramref name="varName"/>.
    /// </summary>
    /// <param name="varName"></param>
    /// <returns></returns>
    public float GetValue(string varName)
    {
        if (HomeostaticsDict.ContainsKey(varName))
            return HomeostaticsDict[varName];
        else if (_sensesDict.ContainsKey(varName))
            return _sensesDict[varName];

        throw new Exception("[HomeostaticNetwork.GetValue] Name of homeostatic or sensory variable not found.");
    }

    /// <summary>
    /// Get a list of the current homeostatic variables' values.
    /// </summary>
    /// <returns></returns>
    public List<float> GetHomeostaticValuesList()
    {
        return HomeostaticsDict.Values.ToList();
    }

    /// <summary>
    /// Get a list of the current homeostatic variables' values.
    /// </summary>
    /// <returns></returns>
    public List<string> GetHomeostaticKeysList()
    {
        return HomeostaticsDict.Keys.ToList();
    }

    /// <summary>
    /// Get a list of the current sensory variables' values.
    /// </summary>
    /// <returns></returns>
    public List<float> GetSensoryValuesList()
    {
        return _sensesDict.Values.ToList();
    }

    /// <summary>
    /// Get a list of the current sensory variables' names.
    /// </summary>
    /// <returns></returns>
    public List<string> GetSensoryKeysList()
    {
        return _sensesDict.Keys.ToList();
    }

    /// <summary>
    /// Set a variable's value.
    /// </summary>
    /// <param name="varName"></param>
    /// <param name="newValue"></param>
    public void SetValue(string varName, float newValue)
    {
        if (HomeostaticsDict.ContainsKey(varName))
            HomeostaticsDict[varName] = newValue;
        else if (_sensesDict.ContainsKey(varName))
            _sensesDict[varName] = newValue;
        else if (!_ignore.Contains(varName))
            throw new Exception($"[HomeostaticNetwork.SetValue] {varName} passed is neither an homeostatic nor a sensory variable.");
    }

    /// <summary>
    /// Increase an homeostatic or sensory variable's variable <paramref name="variableName"/>'s value by a <paramref name="delta"/> amount.
    /// </summary>
    /// <param name="variableName"></param>
    /// <param name="delta"></param>
    public void SetValueDelta(string variableName, float delta)
    {
        if (HomeostaticsDict.ContainsKey(variableName))
            HomeostaticsDict[variableName] += delta;
        else if (_sensesDict.ContainsKey(variableName))
            _sensesDict[variableName] += delta;
        else if(!_ignore.Contains(variableName))
            throw new Exception($"[HomeostaticNetwork.SetValueDelta] {variableName} passed is neither a homeostatic nor a sensory variable.");
    }

    public void ChangeDeltaValue(string variableName, float newDeltaValue)
    {
        if(_hDeltaStep.ContainsKey(variableName))
            _hDeltaStep[variableName] = newDeltaValue;
        else
            throw new Exception($"[HomeostasisModule.ChangeDeltaValue] {variableName} passed does not exist.");
    }

    /// <summary>
    /// Applies domain constraints: projects homeostatic variables within their domain.
    /// </summary>
    private void ProjectToDomain()
    {
        foreach (KeyValuePair<string, float> entry in _domainLB)
        {
            string name = entry.Key;
            if (HomeostaticsDict[name] < _domainLB[name])
                HomeostaticsDict[name] = _domainLB[name];
        }
        foreach (KeyValuePair<string, float> entry in _domainUB)
        { 
            string name = entry.Key;
            if (HomeostaticsDict[name] > _domainUB[name])
                HomeostaticsDict[name] = _domainUB[name];
        }
    }

    /// <summary>
    /// Gets the domain of a given homeostatic variable
    /// </summary>
    /// <returns>{ Lower domain, Upper domain }</returns>
    public float[] GetDomain(string name)
    {
        return new float[2] { _domainLB[name], _domainUB[name] };
    }

    /// <summary>
    /// Checks whether the critical variables are within the boundaries
    /// </summary>
    /// <returns>is alive boolean</returns>
    public bool IsAlive()
    {
        foreach (KeyValuePair<string, bool> entry in IsCritical)
        {
            string name = entry.Key;
            if (entry.Value && ((_deathLB.ContainsKey(name) && HomeostaticsDict[name] <= _deathLB[name])
                || (_deathUB.ContainsKey(name) && HomeostaticsDict[name] >= _deathUB[name])))
                return false;
        }
        
        return true;
    }   
}

