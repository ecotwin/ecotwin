using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;

/********************************************************
Unused class (v0.5). Left-over from master thesis project
********************************************************/
namespace Assets.Scripts.Agents.Homeostasis
{
    public class Anticipation
    {    
        private float _sensoryInertia = 0.9f;
        private float _rememberThreshold = 0.01f; 
        private int _correlationExperience; // With genetic transmission of the wCorrSH, raise this initial value

        //-------------------------VALUES--------------------------------------------
        private int _nHomeo;
        private int _nSenses;

        //-------------------------VECTORS & MATRICES--------------------------------
        // Previous homeostatic values
        private Vector<float> _previousHValues;
        // Previous homeostatic  differences from the desired state
        private Vector<float> _previousHDiff;

        // Vector of homeostatic desired states
        private Vector<float> _desiredHVec;
        // Vector for the homeostatic increase terms (which have inertia)
        private Vector<float> _increaseH;

        // Previous sensory values
        private Vector<float> _previousSValues;
        // Previous sensory differences from the desired state
        private Vector<float> _previousSDiff;
        // Vector of sensory desired states
        private Vector<float> _desiredSVec;
        // Vector for the sensory increase terms (which have inertia)
        private Vector<float> _increaseS;
        // Vector of weights for the homeostatic variables
        private Vector<float> _weightsHVec;

        // Vector with a lack term of each homeostatic variable
        private Vector<float> _hLack;
        // Vector with a lack term of each homeostatic variable
        private Vector<float> _hProg;
        // Vector with a progress term for each sensorial variable
        private Vector<float> _sProg;
        // Matrix with sense-homeostatic correlation terms
        private Matrix<float> _wCorrSH;

        public IEnumerable<float> GetSense()
        {
            //TODO
            return new List<float>();
        }

        public Anticipation(Dictionary<string, float> initHomeo, Dictionary<string, float> initSenses, 
            Dictionary<string, float> desiredH, Dictionary<string, float> desiredS,
            Dictionary<string, float> hWeights = null,float[,] initCorrWeights = null, int initCorrelationExperience = 0)
        {
            _desiredHVec = Vector<float>.Build.DenseOfEnumerable(desiredH.Values);
            _previousHValues = Vector<float>.Build.DenseOfEnumerable(initHomeo.Values);
            _previousHDiff = Vector<float>.Build.Dense(_nHomeo);
            for (int i = 0; i < _nHomeo; i++)
            _previousHDiff[i] = Math.Abs(_previousHValues[i] - _desiredHVec[i]);
            _increaseH = Vector<float>.Build.Dense(_nHomeo);
            _hLack = Vector<float>.Build.Dense(_nHomeo);
            _hProg = Vector<float>.Build.Dense(_nHomeo); // hProg and increaseH are the same thing ATM
            
            _desiredSVec = Vector<float>.Build.DenseOfEnumerable(desiredS.Values);
            _previousSDiff = Vector<float>.Build.Dense(_nSenses);
            _previousSValues = Vector<float>.Build.DenseOfEnumerable(initSenses.Values);
            for (int i = 0; i < _nSenses; i++)
                _previousSDiff[i] = Math.Abs(_previousSValues[i] - _desiredSVec[i]);
            _increaseS = Vector<float>.Build.Dense(_nSenses);
            _sProg = Vector<float>.Build.Dense(_nSenses);
 
            _weightsHVec = Vector<float>.Build.DenseOfEnumerable(hWeights.Values);
           
            _wCorrSH = Matrix<float>.Build.Dense(_nSenses, _nHomeo);
            _wCorrSH = Matrix<float>.Build.DenseOfArray(initCorrWeights);
            _correlationExperience = initCorrelationExperience;            
        }

        public float GetAnticipation(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            // Homeostatics terms
            Vector<float> currentHValues = Vector<float>.Build.DenseOfEnumerable(homeostaticsDict.Values);
            Vector<float> currentHDiff = Vector<float>.Build.Dense(_nHomeo);
            for (int i = 0; i < _nHomeo; i++)
            {
                currentHDiff[i] = Math.Abs(currentHValues[i] - _desiredHVec[i]);
                _hLack[i] = _weightsHVec[i] * _previousHDiff[i];
                _hProg[i] = _previousHDiff[i] - currentHDiff[i];
            }
            // Sensory terms
            Vector<float> currentSValues = Vector<float>.Build.DenseOfEnumerable(sensesDict.Values);
            Vector<float> currentSDiff = Vector<float>.Build.Dense(_nSenses);
            for (int i = 0; i < _nSenses; i++)
            {
                currentSDiff[i] = Math.Abs(currentSValues[i] - _desiredSVec[i]);
                _sProg[i] = _previousSDiff[i] * currentSDiff[i];
            }
            // Correlation weights
            float[,] corrSample = new float[_nSenses, _nHomeo];
            for (int iS = 0; iS < _nSenses; iS++)
            {
                _increaseS[iS] = _sensoryInertia * _increaseS[iS] + (1 - _sensoryInertia) * _sProg[iS];
                for (int iH = 0; iH < _nHomeo; iH++)
                {
                    _increaseH[iH] = _hProg[iH]; // leaving this assignment here for possible future mods
                    corrSample[iS, iH] = _increaseS[iS] * _increaseH[iH];
                    if (corrSample[iS, iH] <= _rememberThreshold)
                        corrSample[iS, iH] = 0;
                    // Update rule for correlation weight
                    _correlationExperience += 1;
                    _wCorrSH[iS, iH] *= (_correlationExperience - 1) / _correlationExperience;
                    _wCorrSH[iS, iH] += corrSample[iS, iH] / _correlationExperience;
                }
            }

            _previousHDiff = currentHDiff;
            _previousHValues = currentHValues;
            _previousSDiff = currentSDiff;
            _previousSValues = currentSValues;
            return _sProg * _wCorrSH * _hLack;
        }
    }
}
