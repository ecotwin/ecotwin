﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility;

namespace Assets.Scripts.Agents.Homeostasis
{
    [Serializable]
    /// <summary>
    /// The class responsible for creating HomeostasisModules with the correct settings
    /// </summary>
    public class HomeostasisFactory
    {
        [Serializable]
        public class HomeostasisConfiguration
        {
            public string Name;
            public float InitialValue = 1;
            public float Desired = 1;   
            public bool IsSense = false;
            public bool IsSensable = false;
            
            public ConditionalField<float> DeathLB;
            public ConditionalField<float> DeathUB;
            public ConditionalField<float> DomainLB;
            public ConditionalField<float> DomainUB;

            public float Weight = 1;
            public float Delta = 0;
        }

        public class UtilityFunction
        {
            public bool IsSensable = false;
            public bool IsSense = false;
            public string HomeostaticVariableName;
            public Func<float, float> Function;
            public bool IsForcedSum;

            public UtilityFunction(string homeostaticVariableName, Func<float, float> function, bool isSensable, bool isSense, bool isForcedSum)
            {
                this.Function = function;
                this.IsSensable = isSensable;
                this.IsSense = isSense;
                this.HomeostaticVariableName = homeostaticVariableName;
                this.IsForcedSum = isForcedSum;
            }
        }

        private class BaselineHappiness : IHappiness
        {
            public float GetReward(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
            {
                return 0.1f;
            }

            public Dictionary<string, float> GetSense(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
            {
                return new Dictionary<string, float>();
            }
        }
        
        [SerializeField]
        private List<HomeostasisConfiguration> _configs = new List<HomeostasisConfiguration>();
        public List<HomeostasisConfiguration> Configs { get { return _configs; } private set { _configs = value; } }

        private List<UtilityFunction> _utilityFunctions = new List<UtilityFunction>();
        private string _aggregation = "sum";
        private bool _useHappiness;

        private bool _useDrive;
        private float _m;
        private float _n;
        private bool _delta;
        private bool _useBaseline;
        private HashSet<string> _ignoreSet = new HashSet<string>();


        private float _positiveWeight = 1f;
        private float _negativeWeight = 1f;

        /*public HomeostasisFactory AddVariable(string name, float initialValue = 0, bool isCritical = false, float? deathLB = null, bool isSensable = false, float? deathUB = null,
            float? domainLB = null, float? domainUB = null, bool isSense = false, float delta = 0, float weight = 1, float desired = 1f)
        {
            _configs.Add(new HomeostasisConfiguration
            {
                Name = name,
                InitialValue = initialValue,
                IsCritical = isCritical,
                DeathLB = deathLB.HasValue ? deathLB : null,
                DeathUB = deathUB.HasValue ? deathUB : null,
                DomainLB = domainLB.HasValue ? domainLB : null,
                DomainUB = domainUB.HasValue ? domainUB : null,
                Desired = desired,
                IsSense = isSense,
                IsSensable = isSensable,
                Weight = weight,
                Delta = delta
            });

            return this;
        }*/

        public HomeostasisFactory AddBaseline()
        {
            _useBaseline = true;

            return this;
        }

        public HomeostasisFactory IgnoreVariable(string v)
        {
            _ignoreSet.Add(v);

            return this;
        }

        public HomeostasisFactory AddDriveReduction(float n, float m, bool delta=true)
        {
            _useDrive = true;
            _n = n;
            _m = m;
            _delta = delta;

            return this;
        }

        public HomeostasisFactory AddLinearUtilityFunction(string homeostaticVariableName, float slope, float intercept, int sign = 1, float weight = 1f, bool isSensable = false, bool isSense = false, bool isForcedSum=false)
        {
            float f(float x) => weight * sign * (slope * x) + intercept;

            _utilityFunctions.Add(new UtilityFunction(homeostaticVariableName, f, isSensable, isSense, isForcedSum));

            _useHappiness = true;
            return this;
        }

        public HomeostasisFactory AddBellUtilityFunction(string homeostaticVariableName, float activation = 0, bool activateGreater = true, float mu = 0.5f, float sigma = 1f/3f, float weight = 1, float intercept = 0, int sign = 1, bool isSensable = false, bool isForcedSum = false, bool isSense = false)
        {
            float f(float x) => (activateGreater == true ? (x > activation ? 1f : 0f) : (x < activation ? 1f : 0f))* weight * sign * ( (float)Math.Exp(-1 * Math.Pow((x - mu) /sigma, 2))) + intercept;

            _utilityFunctions.Add(new UtilityFunction(homeostaticVariableName, f, isSensable, isSense, isForcedSum));

            _useHappiness = true;
            return this;
        }

        public HomeostasisFactory AddRootUtilityFunction(string homeostaticVariableName, float activation = 0, bool activateGreater = true, float shift = 0, float radical = .5f, float weight = 1, float intercept = 0, int sign = 1, bool isSensable = false, bool isForcedSum = false ,bool isSense = false)
        {
            float f(float x) => (activateGreater == true ? (x > activation ? 1f : 0f) : (x < activation ? 1f : 0f)) * weight * sign * ( (float) Math.Pow(x-shift,radical)) + intercept;

            _utilityFunctions.Add(new UtilityFunction(homeostaticVariableName, f, isSensable, isSense, isForcedSum));

            _useHappiness = true;
            return this;
        }

        public HomeostasisFactory AddLogarithmicUtilityFunction(string homeostaticVariableName, float b = 10f, float activation = 0, bool activateGreater = true, float shift = 0, float weight = 1, float intercept = 0, int sign = 1, bool isSensable = false, bool isForcedSum = false, bool isSense = false)
        {
            float f(float x) => (activateGreater == true ? (x > activation ? 1f : 0f) : (x < activation ? 1f : 0f)) * weight * sign * ((float)(Math.Log10(1 + (b - 1) * (x - shift)) / Math.Log10(b))) + intercept;

            _utilityFunctions.Add(new UtilityFunction(homeostaticVariableName, f, isSensable, isSense, isForcedSum));

            _useHappiness = true;
            return this;
        }

        public HomeostasisFactory AddUpperSigmoidUtilityFunction(string homeostaticVariableName, float activation = 0, bool activateGreater = true, float shift = 0, float k = 5f, float weight = 1, float intercept = 0, int sign = 1, bool isSensable = false, bool isSense = false, bool isForcedSum= false)
        {
            float f(float x) => (activateGreater == true ? (x > activation ? 1f : 0f) : (x < activation ? 1f : 0f)) * weight * sign *  ((float)((2 / (1 + Math.Exp(-k * (x - shift)))) - 1) ) + intercept;

            _utilityFunctions.Add(new UtilityFunction(homeostaticVariableName, f, isSensable, isSense, isForcedSum));

            _useHappiness = true;
            return this;
        }

        public HomeostasisFactory AddSquareUtilityFunction(string homeostaticVariableName, float activation = 0, bool activateGreater = true, float shift = 0, float weight = 1, float intercept = 0, int sign = 1, bool isSensable = false, bool isSense = false, bool isForcedSum = false)
        {
            float f(float x) => (activateGreater == true ? (x > activation? 1f:0f) : (x < activation?1f:0f)) * (weight * sign *  (float)Math.Pow(x - shift, 2) ) + intercept;
            _utilityFunctions.Add(new UtilityFunction(homeostaticVariableName, f, isSensable, isSense, isForcedSum));

            _useHappiness = true;
            return this;
        }

        public HomeostasisFactory AddHappinessAggregation(string aggregation, bool delta = true)
        {
            if (aggregation != "sum" && aggregation != "prod")
                throw new Exception("Invalid aggregation choice");

            _useHappiness = true;
            _aggregation = aggregation;
            _delta = delta;

            return this;
        }

        public HomeostasisFactory SetHappinessAsymmetry(float positiveAssymetry, float negativeAssymetry)
        {
            _positiveWeight = positiveAssymetry;
            _negativeWeight = negativeAssymetry;

            return this;
        }

        public HomeostasisModule Build()
        {
            if (_useHappiness && _useDrive)
                throw new Exception("Please use either Drive Reduction or Happiness to calculate rewards");

            Dictionary<string, float> homeostaticsDict = _configs.FindAll(x => !x.IsSense).ToDictionary(x => x.Name, x => x.InitialValue);
            Dictionary<string, float> sensesDict = _configs.FindAll(x => x.IsSense).ToDictionary(x => x.Name, x => x.InitialValue);
            Dictionary<string, float> hDeltaStep = _configs.ToDictionary(x => x.Name, x => x.Delta);
            Dictionary<string, bool> isCritical = _configs.ToDictionary(x => x.Name, x => x.DeathLB.IsUsed || x.DeathUB.IsUsed);
            List<string> isSensable = _configs.Where(x =>  x.IsSensable).Select(x=>x.Name).ToList();

            Dictionary<string, float> deathUB = _configs.Where(x => x.DeathUB.IsUsed).ToDictionary(x => x.Name, x => x.DeathUB.Value);
            Dictionary<string, float> deathLB = _configs.Where(x => x.DeathLB.IsUsed).ToDictionary(x => x.Name, x => x.DeathLB.Value);
            Dictionary<string, float> domainUB = _configs.Where(x => x.DomainUB.IsUsed).ToDictionary(x => x.Name, x => x.DomainUB.Value);
            Dictionary<string, float> domainLB = _configs.Where(x => x.DomainLB.IsUsed).ToDictionary(x => x.Name, x => x.DomainLB.Value);
            IHappiness happiness;

            if (_useDrive)
            {
                Dictionary<string, float> desiredHDict = _configs.FindAll(x => !x.IsSense).ToDictionary(x => x.Name, x => x.Desired);
                Dictionary<string, float> desiredSDict = _configs.FindAll(x => x.IsSense).ToDictionary(x => x.Name, x => x.Desired);

                Dictionary<string, float> weightsHDict = _configs.FindAll(x => !x.IsSense).ToDictionary(x => x.Name, x => x.Weight);
                Dictionary<string, float> weightsSDict = _configs.FindAll(x => x.IsSense).ToDictionary(x => x.Name, x => x.Weight);

                happiness = new DriveHappiness(_n, _m, homeostaticsDict, sensesDict, desiredHDict, desiredSDict, weightsHDict, weightsSDict, _delta);
            }
            else if (_useHappiness)
                happiness = new Happiness(homeostaticsDict, sensesDict, _utilityFunctions, _aggregation, _delta);
            else if (_useBaseline)
                happiness = new BaselineHappiness();
            else
                throw new Exception("Please select method to calculate reward");

            HomeostasisModule homeostasis = new HomeostasisModule(homeostaticsDict, sensesDict, _ignoreSet, hDeltaStep, isSensable, happiness);
            homeostasis.SetCritical(isCritical);
            homeostasis.SetHappinessAsymmetry(_positiveWeight, _negativeWeight);
            homeostasis.SetDomains(deathUB: deathUB, deathLB: deathLB, domainUB: domainUB, domainLB: domainLB);
            return homeostasis;
        }
    }
}
