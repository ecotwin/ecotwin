﻿using System.Collections.Generic;

namespace Assets.Scripts.Agents.Homeostasis
{
    public interface IHappiness
    {
        float GetReward(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict);
        Dictionary<string, float> GetSense(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict);
    }
}
