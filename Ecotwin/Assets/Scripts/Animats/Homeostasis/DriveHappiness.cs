﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Agents.Homeostasis
{
    public class DriveHappiness : IHappiness
    {
        // the desired homeostatic variables' values
        private Dictionary<string, float> _desiredHDict = new Dictionary<string, float>();

        // weights (between 0 and 1?) determine how important a variable is
        private Dictionary<string, float> _weightsHDict = new Dictionary<string, float>();

        // the desired sensory variables' values
        private Dictionary<string, float> _desiredSDict = new Dictionary<string, float>();

        // weights (between 0 and 1?) determine how important a variable is
        private Dictionary<string, float> _weightsSDict = new Dictionary<string, float>();

        private float _previousDrive;
        private float _n;
        private float _m;
        private bool _delta;

        public DriveHappiness(float n, float m, Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict,
            Dictionary<string, float> desiredHDict, Dictionary<string, float> desiredSDict,
            Dictionary<string, float> weightsHDict, Dictionary<string, float> weightsSDict, bool delta)
        {
            _n = n;
            _m = m;
            _delta = delta;

            _desiredHDict = desiredHDict;
            _desiredSDict = desiredSDict;
            _weightsHDict = weightsHDict;
            _weightsSDict = weightsSDict;

            _previousDrive = GetDrive(homeostaticsDict, sensesDict);
        }

        private float GetDrive(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            float drive  = 0;
            // Homeostatic Terms
            foreach (KeyValuePair<string, float> entry in homeostaticsDict)
            {
                drive += _weightsHDict[entry.Key] *
                        (float)Math.Pow(Math.Abs(_desiredHDict[entry.Key] - entry.Value), _n);
            }
            // Sensory terms
            foreach (KeyValuePair<string, float> entry in sensesDict)
            {
                drive += _weightsSDict[entry.Key] *
                        (float)Math.Pow(Math.Abs(_desiredSDict[entry.Key] - entry.Value), _n);
            }
            return (float)Math.Pow(drive, 1 / _m);
        }

        public float GetReward(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            float currentDrive = GetDrive(homeostaticsDict, sensesDict);
            if (!_delta)
                return 1 - currentDrive;

            float reward = _previousDrive - currentDrive;
            _previousDrive = currentDrive;

            return reward;
        }

        public Dictionary<string, float> GetSense(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            return new Dictionary<string, float>();
        }
    }
}
