﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Assets.Scripts.Agents.Homeostasis.HomeostasisFactory;

namespace Assets.Scripts.Agents.Homeostasis
{
    public class Happiness : IHappiness
    {
        private List<UtilityFunction> _utilityFunctions;

        //----------------------------PARAMETERS-------------------------------------

        // Previous homeostatic drive
        private float _previousHappiness;
        private readonly string _aggregation;
        private readonly bool _delta;

        public Happiness(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict,
            List<UtilityFunction> utilityFunctions, string aggregation, bool delta)
        {
            _utilityFunctions = utilityFunctions;
            _previousHappiness = GetHappiness(homeostaticsDict, sensesDict);
            _aggregation = aggregation;
            _delta = delta;
        }

        private float GetHappiness(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            // Group togheter the utility functions of the same homeostatic variables by summing them
            Dictionary<string, float> utilities = new Dictionary<string, float>();
            float forcedSum = 0; // accummulate utility function forced sum
            foreach (UtilityFunction func in _utilityFunctions)
            {
                float utility = CalculateUtility(homeostaticsDict, sensesDict, func);
                if (func.IsForcedSum)
                    forcedSum += utility;
                else if (!utilities.ContainsKey(func.HomeostaticVariableName))
                    utilities[func.HomeostaticVariableName] = utility;
                else
                    utilities[func.HomeostaticVariableName] += utility;
            }            

            // sum all terms if "sum", multiply the various max(term, 0) for each h.variable if "prod"
            return _aggregation == "sum" ? utilities.Values.Sum() + forcedSum: utilities.Values.Aggregate(1f, (acc, val) => acc * Math.Max(val, 0)) + forcedSum;
        }

        public virtual Dictionary<string, float> GetSense(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            return CalculateUtilities(homeostaticsDict, sensesDict, _utilityFunctions.Where(x => x.IsSensable)); 
        }

        public virtual float GetReward(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict)
        {
            float currentHappiness = GetHappiness(homeostaticsDict, sensesDict);
            float reward = currentHappiness  - (_delta ? _previousHappiness : 0);
            _previousHappiness = currentHappiness;
            return reward;
        }

        private Dictionary<string, float> CalculateUtilities(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict, IEnumerable<UtilityFunction> functions)
        {
            Dictionary<string, float> utilities = new Dictionary<string, float>();
            foreach (UtilityFunction func in functions)
            {
                float utility = CalculateUtility(homeostaticsDict, sensesDict, func);
                if (!utilities.ContainsKey(func.HomeostaticVariableName))
                    utilities[func.HomeostaticVariableName] = utility;
                else
                    utilities[func.HomeostaticVariableName] += utility;
            }
            return utilities;
        }

        private float CalculateUtility(Dictionary<string, float> homeostaticsDict, Dictionary<string, float> sensesDict, UtilityFunction func)
        {
            float value = func.IsSense ? sensesDict[func.HomeostaticVariableName] : homeostaticsDict[func.HomeostaticVariableName];
            float result = (float)func.Function.DynamicInvoke(value);
            if (float.IsNegativeInfinity(result))
                result = -99999;
            else if (float.IsPositiveInfinity(result))
                result = 99999;
            else if (float.IsNaN(result))
            {
                Debug.LogError($"Invalid happiness, defaulting to 0. Value: {value}  Func: {func.Function}");
                result = 0;
            }
            return result;
        }
    }
}
