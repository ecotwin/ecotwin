﻿using System;
using UnityEngine;

/// <summary>
/// A helper class for how to initialize a population of animats
/// </summary>
[Serializable]
public class Species
{
    [SerializeField] [Tooltip("The number of animats to spawn of this species at start")]
    private int _initialCount;
    /// <returns> The number of animats to spawn of this species at start </returns>
    public int InitialCount {
        get { return _initialCount; }
        private set { _initialCount = value; }
    }

    [SerializeField] [Tooltip("Should the sex of this species be randomized?")]
    private bool _randomizeSex;
    /// <returns> Should the sex of this species be randomized? </returns>
    public bool RandomizeSex {
        get { return _randomizeSex; }
        private set { _randomizeSex = value; }
    }

    [SerializeField] [Range(0f, 1f)] [Tooltip("The ratio of male/female animats (0 => all female, 1 => all male)")]
    private float _ratioMaleFemale;
    /// <summary> The ratio of male animats in this species. The other animats are female. (0 => all female, 1 => all male) </summary>
    public float RatioMaleFemale {
        get { return _ratioMaleFemale; }
        private set { _ratioMaleFemale = value; }
    }

    [SerializeField]
    private Animat _prefab;
    public Animat Prefab {
        get { return _prefab; }
        private set { _prefab = value; }
    }

    [SerializeField]
    private string _tag;
    /// <summary> The common name of animats of this species which can be used to identify it from other species </summary>
    public string Tag {
        get { return _tag; }
        private set { _tag = value; }
    }
    
    //TODO: create better naming system?
    [SerializeField] [Tooltip("The names used to create the new animat names: Example: new string[] { \"Goat_PPO_0000_0\" }")]
    private string[] _parentNames;
    /// <summary> The names used to create the new animat names (an array of names to use the same method as CreateChild in areamanager) </summary>
    public string[] ParentNames {
        get { return _parentNames; }
        private set { _parentNames = value; }
    } 
    
    [SerializeField] [Tooltip("Should an animat of this species be respawned when the species goes extinct?")]
    private bool _respawnWhenExtinct;
    /// <returns> Should an animat of this species be respawned when the species goes extinct? </returns>
    public bool RespawnWhenExtinct {
        get { return _respawnWhenExtinct; }
        private set {_respawnWhenExtinct = value; }
    }
}
