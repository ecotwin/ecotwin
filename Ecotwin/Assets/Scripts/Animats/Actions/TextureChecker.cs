using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Solution by https://gamedevbeginner.com/terrain-footsteps-in-unity-how-to-detect-different-textures/
public class TextureChecker
{
	public Terrain Terrain;

	private int _posX;
	private int _posZ;
	private float[] _textureValues;
    public float[] TextureValues {
        get { 
            float[] textureValues = new float[_textureValues.Length];
            _textureValues.CopyTo(textureValues, 0);
            return textureValues;
        }
    }
    private float[,,] _aMap;
    private int _nrOfTerrainLayers = 2;

    public TextureChecker(Terrain terrain)
    {
        Terrain = terrain;
        _textureValues = new float[_nrOfTerrainLayers];
    }

    private void ConvertPosition(Vector3 playerPosition)
    {
        Vector3 terrainPosition = playerPosition - Terrain.transform.position;

        Vector3 mapPosition = new Vector3(
            terrainPosition.x / Terrain.terrainData.size.x,
            0,
            terrainPosition.z / Terrain.terrainData.size.z
        );

        float xCoord = mapPosition.x * Terrain.terrainData.alphamapWidth;
        float zCoord = mapPosition.z * Terrain.terrainData.alphamapHeight;

        _posX = (int)xCoord;
        _posZ = (int)zCoord;
    }

    // You need to manually check on the terrain paint settings which index that corresponds to which terrain layer.
    private void CheckTexture()
    {
        _aMap = Terrain.terrainData.GetAlphamaps (_posX, _posZ, 1, 1);
        _textureValues[0] = _aMap[0,0,0];
        _textureValues[1] = _aMap[0,0,1];    
    }

    public void GetTerrainTexture(Vector3 position)
    {
        ConvertPosition(position);
        CheckTexture();
    }
}