using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The implementation of reproduction for animats
/// Due to dependencies, reproduction is also partially handled in Animat and AreaManager
/// </summary>
[CreateAssetMenu(menuName = "Scriptable Objects/Reproduction")]
public class Reproduction : ActionModule
{
    private enum ReproductiveMethod
    {
        Asexual,
        Sexual
    };
    [SerializeField]
    private ReproductiveMethod _reproductiveMethod = ReproductiveMethod.Asexual;

    private class ReproductionDNA : DNA
    {
        public ReproductionDNA()
        { }

        public override float[] GetValues()
        {
            return new float[] { };
        }

        public override void SetValues(float[] genotype)
        { }
    }

    private Animat _animat;
    private string[][] _actions;

    public override void Initialize(Animat animat)
    {
        _animat = animat;
        switch (_reproductiveMethod)
        {
            case ReproductiveMethod.Asexual:
                _actions = new string[][] { new string[]{ "asexual" } };
                _animat.IsAsexual = true;
                break;
            case ReproductiveMethod.Sexual:
                _actions = new string[][] { new string[]{ "sexual" } };
                _animat.IsAsexual = false;
                break;
            default:
                throw new Exception("Undefined reproductive method used in Reproduction.cs");
        }
    }

    public override string[][] GetActions()
    {
        string[][] actions = new string[_actions.GetLength(0)][];
        for (int i = 0; i < _actions.GetLength(0); i++)
        {
            string[] temp = new string[_actions.Length];; 
            _actions[i].CopyTo(temp, 0);
            actions[i] = temp;
        }
            
        
        return actions;
    }

    public override DNA GetGenotype()
    {
        return new ReproductionDNA();
    }

    public override void Mature(float maturityProportion)
    { }

    public override void Mutate(float mutationAmount)
    { }

    public override bool TakeAction(string action)
    {            
        switch (action)
        {
            case "asexual":
                _animat.IsLookingForMate = true;
                return true;
            case "sexual":
                _animat.IsLookingForMate = true;
                _animat.StepLookingForMate = _animat.Age;
                return true;
            default:
                Debug.LogError($"Undefined reproduction action {action} in SexualReproduction");
                return false;
        }
    }

    public override List<(string, bool)> GetReflexes() { return new List<(string, bool)>(); }
}