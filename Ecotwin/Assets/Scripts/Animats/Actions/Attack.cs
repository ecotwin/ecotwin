﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utility;

/// <summary>
/// The implementation of an animat's attack action (for predators)
/// </summary>
[CreateAssetMenu(menuName = "Scriptable Objects/Attack")]
public class Attack : ActionModule
{
    [Serializable]
    private class AttackDNA : DNA
    {
        public float AttackEnergy;
        public float AttackForce;
        public float AttackRange;

        public AttackDNA(float[] genotype)
        { 
            SetValues(genotype);
        }

        public override float[] GetValues()
        {
            return new float[] { AttackEnergy, AttackForce, AttackRange };
        }

        public override void SetValues(float[] genotype)
        {
            AttackEnergy = genotype[0];
            AttackForce  = genotype[1];
            AttackRange  = genotype[2];
        }
    }

    [Header("Genotype")]
    [SerializeField]
    private ConditionalFieldCode<AttackDNA> _genotype;
    private AttackDNA _phenotype;

    [SerializeField]
    private List<string> _preyTags = new List<string>();

    private Animat _animat;
    private AreaManager _areaManager;

    public override void Initialize(Animat animat)
    {
        _genotype.Disable();
        _phenotype = new AttackDNA(_genotype.Value.GetValues());

        _animat = animat;

        // TODO: Remove connection to areamanager (this dependency is not logical)
        _areaManager = animat.GetComponentInParent<AreaManager>();
    }

    public override DNA GetGenotype()
    {
        return _genotype.Value;
    }

    // Attack currently does not scale with age
    public override void Mature(float maturityProportion)
    {
        _phenotype.AttackEnergy = _genotype.Value.AttackEnergy;
        _phenotype.AttackForce  = _genotype.Value.AttackForce;
        _phenotype.AttackRange  = _genotype.Value.AttackRange;
    }

    public override void Mutate(float mutationAmount)
    {
        _genotype.Value.AttackEnergy *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
        _genotype.Value.AttackForce  *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
        _genotype.Value.AttackRange  *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
    }

    public override string[][] GetActions()
    {
        return new string[][] { new string[] { "attack" } };
    }

    public override bool TakeAction(string action)
    {
        switch (action)
        {
            case "attack":    
                List<Animat> nearbyPreys = _areaManager.GetNeighbouringAnimatsWithTag(_animat.transform.position, _preyTags.ToArray(), _phenotype.AttackRange);
                bool closeToPrey = nearbyPreys.Count > 0;
                if (closeToPrey)
                {
                    // Consumes attacking energy
                    _animat.Homeostasis.SetValueDelta("energy", _phenotype.AttackEnergy);

                    // Choose a random near prey and damage it
                    nearbyPreys[UnityEngine.Random.Range(0, nearbyPreys.Count)].Damage(UnityEngine.Random.Range(0f, _phenotype.AttackForce), _animat);
                }
                return true;
            default:
                Debug.LogError($"Invalid action {action} taken in Attack!");
                return false;
        }
    }

    public override List<(string, bool)> GetReflexes() { return new List<(string, bool)>(); }
}
