﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Unity.MLAgents.Actuators;

/// <summary>
/// The controller for all action modules in an animat.
/// Delegates calls to all the animat's action modules.
/// </summary>
public class ActionHandler
{
    private ActionModule[] _actionModules;
    private List<string[][]> _actionNames = new List<string[][]>();
    private readonly Dictionary<string, (int, int)> _actionToInt = new Dictionary<string, (int, int)>();
    public Dictionary<string, (int, int)> ActionToInt { get { return new Dictionary<string, (int, int)>(_actionToInt); } }
    private readonly Dictionary<(int, int), string> _intToAction = new Dictionary<(int, int), string>();
    private int _actionBranches;

    /// <summary>
    /// Gets the size of all the action branches of the animat.
    /// </summary>
    /// <returns>A copy of the array with the sizes of the action branches array</returns>
    public int[] ActionSpaceSize { get; private set; }

    public ActionHandler(Animat animat, ActionModule[] actionModules)
    {
        _actionModules = new ActionModule[actionModules.Length];
        for (int i = 0; i < _actionModules.Length; i++)
            _actionModules[i] = actionModules[i].Copy();
        
        _actionBranches = 1;
        foreach (ActionModule actionModule in _actionModules)
        {
            actionModule.Initialize(animat);
            string[][] actionNames = actionModule.GetActions();
            _actionBranches = Mathf.Max(actionNames.GetLength(0), _actionBranches);
            _actionNames.Add(actionNames);
        }
        
        List<string[]> idles = new List<string[]>();    
        for (int i = 0; i < _actionBranches; i++)
            idles.Add(new string[] { $"idle{i}" });
        _actionNames.Add(idles.ToArray());

        ActionSpaceSize = new int[_actionBranches];
        for (int i = 0; i < _actionNames.Count; i++)
        {
            for (int j = 0; j < _actionNames[i].GetLength(0); j++)
            {
                for (int k = 0; k < _actionNames[i][j].Length; k++)
                {
                    _actionToInt[_actionNames[i][j][k]] = (j, ActionSpaceSize[j]);
                    _intToAction[(j, ActionSpaceSize[j])] = _actionNames[i][j][k];
                    ActionSpaceSize[j]++;
                }
            }
        }
    }

    /// <summary>
    /// Finds the action module of an action id and takes one action per action branch
    /// </summary>
    /// <returns>Was an action (excluding idle) handled successfully?</return>
    public bool HandleActions(ActionSegment<int> actions)
    {
        bool successfulActions = false;
        for (int i = 0; i < actions.Length; i++)
        {
            string action = _intToAction[(i, actions[i])];
            for (int j = 0; j < _actionModules.Length; j++)
            {
                for (int k = 0; k < _actionNames[j].GetLength(0); k++)
                {
                    if (_actionNames[j][k].Contains(action))
                    {
                        successfulActions = _actionModules[j].TakeAction(action) || successfulActions;
                        break;
                    }
                }       
            }
        }

        return successfulActions;
    }

    //TODO: needs rework
    public int[] GetHeuristics()
    {
        string action = "idle";

        if (Input.GetKey(KeyCode.W))
            action = "run";
        else if (Input.GetKey(KeyCode.S))
            action = "walk";
        else if (Input.GetKey(KeyCode.D))
            action = "turnRight";
        else if (Input.GetKey(KeyCode.A))
            action = "turnLeft";
        else if (Input.GetKey(KeyCode.E))
            action = "eat";
        else if (Input.GetKey(KeyCode.R))
            action = "attack";

        return new int[]{ _actionToInt[action].Item2 };
    }

    /// <returns>The string identifiers of all the animat's actions</returns>
    public string[] GetNames(ActionSegment<int> actions)
    {
        string[] names = new string[actions.Length];
        for (int i = 0; i < actions.Length; i++)
            names[i] = _intToAction[(i, actions[i])];
        
        return names;
    }

    /// <summary>
    /// Assembles the genotype split into action modules into a single array
    /// and returns it to the animat such that it can be used for evolutionary processes
    /// </summary>
    public float[][] GetGenotype()
    {
        float[][] genotype = new float[_actionModules.Length][];
        for (int i = 0; i < _actionModules.Length; i++)
            genotype[i] = _actionModules[i].GetGenotype().GetValues();
        
        return genotype;
    }

    /// <summary>
    /// Sets the genotype split into action modules (used during evolutionary process)
    /// </summary>
    public void SetGenotype(List<float[]> genotype)
    {
        for (int i = 0; i < genotype.Count; i++)
            _actionModules[i].GetGenotype().SetValues(genotype[i]);
    }

    /// <summary>
    /// Gets the reflexes bounded to action modules such that they can be assigned
    /// during evolutionary processes
    /// </summary>
    public Dictionary<string, (string, bool)>[] GetReflexes()
    {
        int length = _actionModules.Length;
        Dictionary<string, (string, bool)>[] reflexes = new Dictionary<string, (string, bool)>[length];
        for (int i = 0; i < length; i++)
            reflexes[i] = _actionModules[i].Reflexes;

        return reflexes;
    }

    /// <summary>
    /// Sets the reflexes of all action in action modules (used in evolutionary process)
    /// </summary>
    public void SetReflexes(Dictionary<string, (string, bool)>[] reflexes)
    {
        for (int i = 0; i < reflexes.Length; i++)
            _actionModules[i].Reflexes = reflexes[i];
    }

    /// <summary>
    /// Signals action modules to mature (update phenotype)
    /// </summary>
    public void Mature(float maturityProportion)
    {
        foreach (ActionModule actionModule in _actionModules)
            actionModule.Mature(maturityProportion);
    }

    /// <summary>
    /// Signals action modules to mutate (mutate genotype)
    /// </summary>
    public void Mutate(float mutationAmount)
    {
        foreach (ActionModule actionModule in _actionModules)
            actionModule.Mutate(mutationAmount);
    }

    /// <summary>
    /// Checks all actionmodules for if a reflex should mask the available actions
    /// </summary>
    /// <returns>The actions an animat can and cannot take</returns>
    public List<List<int>> GetActionMasks()
    {
        List<List<int>> actionMasks = new List<List<int>>();
        List<(string, bool)> reflexes = new List<(string, bool)>();
        foreach (ActionModule actionModule in _actionModules)
            reflexes.AddRange(actionModule.GetReflexes().Where(x => !string.IsNullOrEmpty(x.Item1)));
        
        for (int i = 0; i < _actionBranches; i++)
        {
            List<int> actionMask = new List<int>();
            IEnumerable<(string, bool)> reflexesInBranch = reflexes.Where(x => _actionToInt[x.Item1].Item1 == i);
            IEnumerable<string> prohibitedActions = reflexesInBranch.Where(x => !x.Item2).Select(y => y.Item1);
            List<int> forcedActions = reflexesInBranch.Where(x =>  x.Item2).Select(y => _actionToInt[y.Item1].Item2).OrderBy(z => z).ToList();
            if (prohibitedActions.Any())
                foreach (string prohibitedAction in prohibitedActions)
                    actionMask.Add(_actionToInt[prohibitedAction].Item2);
            
            foreach (int forcedAction in forcedActions)
            {
                if (prohibitedActions.Contains(_intToAction[(i, forcedAction)]))
                    forcedActions.Remove(forcedAction);
            }
            int j = 0;
            for (int k = 0; k < ActionSpaceSize[i]; k++)
            {
                if (j == forcedActions.Count)
                    break;
                
                if (forcedActions[j] == k)
                    j++;
                else
                    actionMask.Add(k);
            }
            actionMasks.Add(actionMask);
        }
        
        return actionMasks;
    }
}