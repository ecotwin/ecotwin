using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility;

[CreateAssetMenu(menuName = "Scriptable Objects/TerrainMovement")]
public class TerrainMovement : ActionModule
{
    [Serializable]
    private class TerrainMovementDNA : DNA
    {
        public float Velocity;
        public float TurnSpeed;

        public TerrainMovementDNA(float[] genotype)
        { 
            SetValues(genotype);
        }

        public override float[] GetValues()
        {
            return new float[] { Velocity, TurnSpeed };
        }

        public override void SetValues(float[] genotype)
        {
            Velocity  = genotype[0];
            TurnSpeed = genotype[1];
        }
    }

    [Header("Genotype")]
    [SerializeField]
    private ConditionalFieldCode<TerrainMovementDNA> _genotype;
    private TerrainMovementDNA _phenotype;

    private Animat _animat;
    private CharacterController _characterController;
    private Vector3 _forward;
    private float _textureVelocity;
    private LayerMask _groundLayer;
    private RaycastHit _hitInfo;
    private TextureChecker _textureChecker;

    public override void Initialize(Animat animat)
    {
        _genotype.Disable();
        _phenotype = new TerrainMovementDNA(_genotype.Value.GetValues());

        _animat = animat;
        _characterController = _animat.GetComponent<CharacterController>();
        if (_characterController == null)
            throw new Exception($"Initializing TerrainMovement without a CharacterController attached to animat {animat}!");

        _groundLayer = LayerMask.GetMask("ground");
        _textureChecker = new TextureChecker(_animat.AreaManager.Terrain);
    }

    public override DNA GetGenotype()
    {
        return _genotype.Value;
    }

    public override void Mature(float maturityProportion)
    {
        _phenotype.Velocity  = _genotype.Value.Velocity  * maturityProportion;
        _phenotype.TurnSpeed = _genotype.Value.TurnSpeed * maturityProportion;
    }

    public override void Mutate(float mutationAmount)
    {
        _genotype.Value.Velocity  *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
        _genotype.Value.TurnSpeed *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
    }

    public override string[][] GetActions()
    {
        return new string[][] {
            new string[] { "walk", "run" },
            new string[] { "turnLeft", "turnRight" }
        };
    }

    public override bool TakeAction(string action)
    {
        CheckGround();
        CalculateForward();
        bool successfulAction = false;
        switch (action)
        {
            case "walk":
                Move(1);
                successfulAction = true;
                break;
            case "run":
                Move(2);
                successfulAction = true;
                break;
            case "turnLeft":
                Rotate(-1);
                successfulAction = true;
                break;
            case "turnRight":
                Rotate(1);
                successfulAction = true;
                break;
            default:
                Debug.LogError($"Attempting to take unavailable action {action} in TerrainMovement");
                break;
        }
        ApplyGravity();
        return successfulAction;
    }

    public override List<(string, bool)> GetReflexes()
    {
        return new List<(string, bool)>();
    }

    private void Move(float speed)
    {
        _characterController.Move(speed * _forward * CalculateVelocity());
    }

    private float CalculateVelocity()
    {
        // Check the texture of the terrain the player is on
        _textureChecker.GetTerrainTexture(_animat.transform.position);

        // Check whether any texture represents more than 50%, then assign corresponding velocity
        //TODO Maybe a loop over all textureValues which are asserted to be of samelength as a list textureVelocities so we can do IF textureValues[i]>0.5 then textureVelocity = textureVelocities[i] or something. And textureVelocities can be assigned in the inspector of the areaManager.
        if (_textureChecker.TextureValues[0] > 0.5f)
            _textureVelocity = 1;
        if (_textureChecker.TextureValues[1] > 0.5f)
            _textureVelocity = 0.5f;

        // If uphill
        float movementAngle = CalculateMovementAngle();
        if (movementAngle > 0)
            return _textureVelocity * _phenotype.Velocity * Mathf.Cos(Mathf.PI*2/360*90*(movementAngle/_characterController.slopeLimit));
        return _phenotype.Velocity;
    }

    private float CalculateMovementAngle()
    {
        return 90 - Vector3.Angle(_forward, Vector3.up);
    }

    private void CalculateForward()
    {
        _forward = Vector3.Cross(_animat.transform.right, _hitInfo.normal);
    }

    private void Rotate(float rotate)
    {
        _animat.transform.rotation = Quaternion.LookRotation(_forward, _animat.transform.up) * Quaternion.Euler(0, rotate * _phenotype.TurnSpeed, 0);
    }

    private void CheckGround()
    {
        // Check the ground with raycast to use get the normal vector and rotate agent accordingly
        Physics.Raycast(_animat.transform.position, -Vector3.up, out _hitInfo, 10, _groundLayer);

        // Naive way to reposition the animat above the ground if it falls through for some reason (or if it is too high in the air)
        if (_hitInfo.collider == null)
        {
            _characterController.enabled = false;
            _animat.transform.position = new Vector3(_animat.transform.position.x, _textureChecker.Terrain.SampleHeight(_animat.transform.position) + 0.00001f, _animat.transform.position.z);
            _characterController.enabled = true;
            CheckGround(); // Potential forever loop (but if so, then the repositioning is not working)
        }
    }

    // Applies a huge force of gravity to make almost sure that the animat is on the ground
    // TODO: apply gravity regardless of which action is taken
    private void ApplyGravity()
    {
        _characterController.Move(new Vector3(0, -9.81f * 0.1f, 0));
    }
}
