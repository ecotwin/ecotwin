﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility;

/// <summary>
/// An implementation of the animat's eating action
/// </summary>
[CreateAssetMenu(menuName = "Scriptable Objects/Eat")]
public class Eat : ActionModule
{
    [Header("Reflexes")]
    [SerializeField]
    private Reflex _closeFoodRed;
    
    [SerializeField]
    private Reflex _closeFoodGreen;
    
    [SerializeField]
    private Reflex _closeFoodBlue;
    
    [SerializeField]
    private Reflex _closeFoodYellow;
    
    [SerializeField]
    private Reflex _closeFoodMagenta;
    
    [SerializeField]
    private Reflex _closeFoodCyan;

    [SerializeField]
    private Reflex _closeFoodWhite;
    
    [SerializeField]
    private Reflex _onFood;

    [Serializable]
    private class EatDNA : DNA
    {
        public float EatEnergy;
        public float BiteSize;

        public EatDNA(float[] genotype)
        { 
            SetValues(genotype);
        }

        public override float[] GetValues()
        {
            return new float[] { EatEnergy, BiteSize };
        }

        public override void SetValues(float[] genotype)
        {
            EatEnergy = genotype[0];
            BiteSize  = genotype[1];
        }
    }

    [Header("Genotype")]
    [SerializeField]
    private ConditionalFieldCode<EatDNA> _genotype;
    private EatDNA _phenotype;

    private HomeostasisModule _homeostasis;
    
    private Dictionary<string, Dictionary<string, float>> _nutrientsEffectMap;
    
    private Animat _animat;
    
    private List<string> _nutrients;

    public override void Initialize(Animat animat)
    {
        _genotype.Disable();
        _phenotype = new EatDNA(_genotype.Value.GetValues());

        _animat = animat;
        _homeostasis = _animat.Homeostasis;
        _nutrientsEffectMap = _animat.NutrientsEffectMap;
        _nutrients = new List<string>();
        foreach (var nutrientEffects in _nutrientsEffectMap)
            _nutrients.Add(nutrientEffects.Key);
            
        _reflexes = new Dictionary<string, (string, bool)>() {
            { nameof(_closeFoodRed    ), _closeFoodRed    .ToTuple() },
            { nameof(_closeFoodGreen  ), _closeFoodGreen  .ToTuple() },
            { nameof(_closeFoodBlue   ), _closeFoodBlue   .ToTuple() },
            { nameof(_closeFoodYellow ), _closeFoodYellow .ToTuple() },
            { nameof(_closeFoodMagenta), _closeFoodMagenta.ToTuple() },
            { nameof(_closeFoodCyan   ), _closeFoodCyan   .ToTuple() },
            { nameof(_closeFoodWhite  ), _closeFoodWhite  .ToTuple() },
            { nameof(_onFood          ), _onFood          .ToTuple() }
        };
    }

    public override DNA GetGenotype()
    {
        return _genotype.Value;
    }

    public override void Mature(float maturityProportion)
    {
        _phenotype.EatEnergy = _genotype.Value.EatEnergy;
        _phenotype.BiteSize  = _genotype.Value.BiteSize * maturityProportion;
    }

    public override void Mutate(float mutationAmount)
    {
        _genotype.Value.EatEnergy *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
        _genotype.Value.BiteSize  *= UnityEngine.Random.Range(1 - mutationAmount, 1 / (1 - mutationAmount));
    }

    public override string[][] GetActions()
    {
        return new string[][] { new string[] { "eat" } };
    }

    public override bool TakeAction(string action)
    {
        switch (action)
        {
            case "eat":
                _animat.NearbyConsumables.RemoveAll(x => x == null);
                List<Consumable> closeFoods = _animat.NearbyConsumables;

                if (closeFoods.Count() < 1)
                    return false;
                    
                float shortestDistance = float.MaxValue;
                Consumable closestFood = null;
                int closestIndex = -1;
                for (int i = 0; i < closeFoods.Count; i++)
                {
                    float sqrDistance = Vector3.SqrMagnitude(_animat.transform.position - closeFoods[i].transform.position);
                    if (sqrDistance < shortestDistance)
                    {
                        shortestDistance = sqrDistance;
                        closestFood = closeFoods[i];
                        closestIndex = i;
                    }
                }
                if (closestFood == null)
                    return false;
                    
                Dictionary<string, float> hDeltaEating = new Dictionary<string, float>() {
                    ["energy"] = _phenotype.EatEnergy
                };
                bool edibleFood = false;
                foreach (var nutrientInFood in closestFood.Nutrients.Where(x => _nutrients.Contains(x.Key)))
                {
                    if (_nutrientsEffectMap.Keys.ToList().Contains(nutrientInFood.Key))
                    {
                        edibleFood = true;
                        break;
                    }
                }
                
                if (edibleFood)
                {
                    float r = closestFood.GetComponent<Renderer>().material.color.r;
                    float g = closestFood.GetComponent<Renderer>().material.color.g;
                    float b = closestFood.GetComponent<Renderer>().material.color.b;

                    var consumeInfo = closestFood.Consume(_phenotype.BiteSize, _animat.transform);
                    foreach (var nutrientLevel in consumeInfo.Item1.Where(x => _nutrients.Contains(x.Key)))
                    {
                        if (_nutrientsEffectMap.Keys.ToList().Contains(nutrientLevel.Key) && nutrientLevel.Value > 0)
                            foreach (var hEffect in _nutrientsEffectMap[nutrientLevel.Key])
                                hDeltaEating[hEffect.Key] = nutrientLevel.Value * hEffect.Value;
                    }
                    foreach (KeyValuePair<string, float> deltaElement in hDeltaEating)
                        _homeostasis.SetValueDelta(deltaElement.Key, deltaElement.Value);
                    
                    if (consumeInfo.Item2 < 0.001f)
                    {
                        _animat.NearbyConsumables.RemoveAt(closestIndex);
                        if (_animat.NearbyConsumables.Count < 1)
                            _animat.ResetCloseColor(new Vector3(r, g, b));
                    }
                }
                return true;
            default:
                Debug.LogError($"Invalid action {action} taken in Eat!");
                return false;
        }
    }

    public override List<(string, bool)> GetReflexes()
    {
        List<(string, bool)> reflexes = new List<(string, bool)>();
        foreach (Vector3 c in _animat.ClosestColors)
        {
            if (c.x > 0 && c.y > 0 && c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodWhite  )]);
            else if (c.x > 0 && c.y > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodYellow )]);
            else if (c.x > 0 && c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodMagenta)]);
            else if (c.y > 0 && c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodCyan   )]);
            else if (c.x > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodRed    )]);
            else if (c.y > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodGreen  )]);
            else if (c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodBlue   )]);
        }

        if (_animat.ClosestColor.x >= 0 || _animat.ClosestColor.y >= 0 || _animat.ClosestColor.z >= 0)
            reflexes.Add(_reflexes[nameof(_onFood)]);

        return reflexes;
    }
}
