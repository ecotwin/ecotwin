/*using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// An implementation of continuous movement based on using a force to move the animat.
/// </summary>
[CreateAssetMenu(menuName = "Scriptable Objects/ContinuousMovement")]
public class ContinuousMovement : ActionModule
{    
    [Header("Reflexes")]
    [SerializeField]
    private Reflex _closeFoodRed;
    
    [SerializeField]
    private Reflex _closeFoodGreen;
    
    [SerializeField]
    private Reflex _closeFoodBlue;
    
    [SerializeField]
    private Reflex _closeFoodYellow;
    
    [SerializeField]
    private Reflex _closeFoodMagenta;
    
    [SerializeField]
    private Reflex _closeFoodCyan;

    [SerializeField]
    private Reflex _closeFoodWhite;
    
    [SerializeField]
    private Reflex _onFood;
    
    [SerializeField]
    private Reflex _followCloseFoodFront;

    [SerializeField]
    private Reflex _followCloseFoodBehind;

    [SerializeField]
    private Reflex _followCloseFoodRight;

    [SerializeField]
    private Reflex _followCloseFoodLeft;

    [Header("Energy")]
    [SerializeField]
    private float _moveEnergy;
    
    [Header("Genotype")]
    [SerializeField]
    private float _maxSpeed;
    
    [SerializeField]
    private float _movingForce;
    
    [SerializeField]
    private float _rotationForce;

    private Rigidbody _rigidbody;
    private Animat _animat;

    public override void Initialize(Animat animat)
    {
        _rigidbody = animat.GetComponent<Rigidbody>();
        _animat    = animat;
        if (_rigidbody == null)
            throw new Exception("ContinuousMovement's modules initialized with null!");
        
        _genotype = new Dictionary<string, float>() {
            { nameof(_maxSpeed     ), _maxSpeed      },
            { nameof(_movingForce  ), _movingForce   },
            { nameof(_rotationForce), _rotationForce }
        };
        foreach (string trait in _genotype.Keys)
            _phenotype.Add(trait, _genotype[trait]);
            
        _reflexes = new Dictionary<string, (string, bool)>() {
            { nameof(_closeFoodRed         ), _closeFoodRed         .ToTuple() },
            { nameof(_closeFoodGreen       ), _closeFoodGreen       .ToTuple() },
            { nameof(_closeFoodBlue        ), _closeFoodBlue        .ToTuple() },
            { nameof(_closeFoodYellow      ), _closeFoodYellow      .ToTuple() },
            { nameof(_closeFoodMagenta     ), _closeFoodMagenta     .ToTuple() },
            { nameof(_closeFoodCyan        ), _closeFoodCyan        .ToTuple() },
            { nameof(_closeFoodWhite       ), _closeFoodWhite       .ToTuple() },
            { nameof(_onFood               ), _onFood               .ToTuple() },
            { nameof(_followCloseFoodFront ), _followCloseFoodFront .ToTuple() },
            { nameof(_followCloseFoodBehind), _followCloseFoodBehind.ToTuple() },
            { nameof(_followCloseFoodRight ), _followCloseFoodRight .ToTuple() },
            { nameof(_followCloseFoodLeft  ), _followCloseFoodLeft  .ToTuple() }
        };
    }

    public override void Mature(float maturityProportion)
    {
        string[] traits = new string[] { nameof(_maxSpeed), nameof(_movingForce) };
        foreach (string key in traits)
            _phenotype[key] = _genotype[key] * maturityProportion;
        
        _phenotype[nameof(_rotationForce)] = _genotype[nameof(_rotationForce)];
    }

    public override string[][] GetActions()
    {
        return new string[][] { new string[] { "accelerate", "decelerate", "turnLeft", "turnRight" } };
    }

    public override bool TakeAction(string action)
    {
        float energyDelta = 0f;
        switch (action)
        {
            case "accelerate":
                float maxForce =
                    (_rigidbody.velocity + _rigidbody.transform.forward * _phenotype[nameof(_movingForce)] * Time.fixedDeltaTime/ _rigidbody.mass).magnitude <= _phenotype[nameof(_maxSpeed)]
                    ? _phenotype[nameof(_movingForce)]
                    : (_phenotype[nameof(_maxSpeed)] - _rigidbody.velocity.magnitude) * _rigidbody.mass;
                _rigidbody.AddForce(_rigidbody.transform.forward * maxForce, ForceMode.Impulse);
                energyDelta += _moveEnergy;
                break;
            case "decelerate":
                //Only tested for environment where the y-axis is against gravity
                float sign = Mathf.Sign(_rigidbody.transform.InverseTransformDirection(_rigidbody.velocity).z);
                float minForce =
                    (_rigidbody.velocity - _rigidbody.transform.forward * _phenotype[nameof(_movingForce)] * Time.fixedDeltaTime / _rigidbody.mass).magnitude * sign >= 0f
                    ? _phenotype[nameof(_movingForce)]
                    : (_rigidbody.velocity.magnitude) * sign * _rigidbody.mass;
                _rigidbody.AddForce(-_rigidbody.transform.forward * minForce, ForceMode.Impulse);
                energyDelta += _moveEnergy;
                break;
            case "turnLeft":
                _rigidbody.AddTorque(_rigidbody.transform.up * 4 * -_phenotype[nameof(_rotationForce)], ForceMode.VelocityChange);
                break;
            case "turnRight":
                _rigidbody.AddTorque(_rigidbody.transform.up * 4 * _phenotype[nameof(_rotationForce)], ForceMode.VelocityChange);
                break;
            default:
                Debug.LogWarning($"Unknown action {action} taken in ContinuousMovement");
                break;
        }
        _animat.Homeostasis.SetValueDelta("energy", energyDelta);
        return true;
    }

    public override List<(string, bool)> GetReflexes()
    {
        List<(string, bool)> reflexes = new List<(string, bool)>();
        foreach (Vector3 c in _animat.ClosestColors)
        {
            if (c.x > 0 && c.y > 0 && c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodWhite  )]);
            else if (c.x > 0 && c.y > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodYellow )]);
            else if (c.x > 0 && c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodMagenta)]);
            else if (c.y > 0 && c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodCyan   )]);
            else if (c.x > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodRed    )]);
            else if (c.y > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodGreen  )]);
            else if (c.z > 0)
                reflexes.Add(_reflexes[nameof(_closeFoodBlue   )]);
        }

        int foodToAbove = 0, foodToFront = 0, foodToRight = 0;
        if (_animat.ClosestColor.x >= 0 || _animat.ClosestColor.y >= 0 || _animat.ClosestColor.z >= 0)
            foodToAbove = 1;
        else if (_animat.SmellObservation[1] != 0 && (int)Mathf.Sign(_animat.SmellObservation[1]) > 0 && Mathf.Pow(_animat.SmellObservation[1], 2f) >= Mathf.Pow(_animat.SmellObservation[2], 2f))
            foodToFront = 1;
        else if (_animat.SmellObservation[2] != 0)
            foodToRight = (int)Mathf.Sign(_animat.SmellObservation[2]);

        float closestRay = 20;
        int closestRayIndex = -2;
        for (int i = 0; i < _animat.VisionObservation.Count; i++)
        {
            if (_animat.VisionObservation[i] != -1 && _animat.VisionObservation[i] < closestRay)
            {
                closestRayIndex = i == 0 ? 0 : i % 2 == 0 ? -1 : 1;
                closestRay = _animat.VisionObservation[i];
            }
        }
        if (foodToAbove == 1f)
            reflexes.Add(_reflexes[nameof(_onFood)]);
        else if (closestRayIndex > -2)
        {
            if (closestRayIndex == 0)
                reflexes.Add(_reflexes[nameof(_followCloseFoodFront)]);
            else if (closestRayIndex == 1)
                reflexes.Add(_reflexes[nameof(_followCloseFoodRight)]);
            else if (closestRayIndex == -1)
                reflexes.Add(_reflexes[nameof(_followCloseFoodLeft )]);
        }
        else if (foodToFront > 0)
            reflexes.Add(_reflexes[nameof(_followCloseFoodFront )]);
        else if (foodToFront < 0)
            reflexes.Add(_reflexes[nameof(_followCloseFoodBehind)]);
        else if (foodToRight > 0)
            reflexes.Add(_reflexes[nameof(_followCloseFoodRight )]);
        else if (foodToRight < 0)
            reflexes.Add(_reflexes[nameof(_followCloseFoodLeft  )]);
        
        return reflexes;
    }
}*/