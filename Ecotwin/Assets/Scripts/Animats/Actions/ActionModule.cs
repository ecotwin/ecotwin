﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The base class of animats' action modules - the implementations of the animats' actions
/// Implemented as a ScriptableObject rather than an interface due to interfaces not being serializable
/// </summary>
public abstract class ActionModule : ScriptableObject
{
    [Serializable]
    /// <summary>
    /// Helper class to display tuple-like data in the inspector
    /// </summary>
    protected class Reflex
    {
        public string Action;
        public bool IsForced;

        public Reflex((string, bool) reflex)
        {
            Action   = reflex.Item1;
            IsForced = reflex.Item2;
        }

        public (string, bool) ToTuple()
        {
            return (Action, IsForced);
        }
    }

    [SerializeField]
    protected Dictionary<string, (string, bool)> _reflexes = new Dictionary<string, (string, bool)>();
    public Dictionary<string, (string, bool)> Reflexes {
        get { return new Dictionary<string, (string, bool)>(_reflexes); }
        set { _reflexes = new Dictionary<string, (string, bool)>(value); }
    }

    public abstract void Initialize(Animat animat);
    
    public ActionModule Copy()
    {
        ActionModule copy = Instantiate(this);
        copy.name = name;
        return copy;
    }

    public abstract DNA GetGenotype();

    /// <summary>
    /// Update phenotype using the genotype (genetic attributes) and age
    /// </summary>
    public abstract void Mature(float maturityProportion);
    
    /// <summary>
    /// Mutates the genotype (should be done only during initialization!)
    /// </summary>
    public abstract void Mutate(float mutationAmount);
    
    /// <summary>
    /// Get the name of all actions available in this action module
    /// CAUTION: do not use the same name of actions in different action modules!
    /// </summary>
    public abstract string[][] GetActions();
    
    /// <summary>
    /// Perform the action identified by the action name
    /// </summary>
    public abstract bool TakeAction(string action);

    /// <summary>
    /// Find if an action should be forced or prohibited
    /// </summary>
    /// <returns>Item1 => action to mask. Item2 => true==force, false==prohibit</returns>
    public abstract List<(string, bool)> GetReflexes();
}