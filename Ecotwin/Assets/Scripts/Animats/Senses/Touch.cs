﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Represents a sense of touch, implemented by registering collisions
/// </summary>
public class Touch
{
    public Dictionary<string, int> ObservableTags;

    private int[] _isTouching;
    public Touch(List<ObservableTag> observableTags)
    {
        ObservableTags = observableTags.ToDictionary(x=> x.Tag, x=> observableTags.IndexOf(x));
        _isTouching = new int[observableTags.Count];
    }

    public void OnTouch(Collider collider)
    {
        if (ObservableTags.ContainsKey(collider.tag))
            _isTouching[ObservableTags[collider.tag]]++;
    }
    
    public void OnTouchStop(Collider collider)
    {
        if (ObservableTags.ContainsKey(collider.tag))
            _isTouching[ObservableTags[collider.tag]]--;
    }

    public IEnumerable<bool> GetSense()
    {
        return _isTouching.Select(x=> x>0).ToList();
    }
}

