﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// All the observable information of objects
/// </summary>
[Serializable]
public struct ObservableTag
{
    [SerializeField] [Tooltip("The exact tag of gameObjects that are observed through this observableTag.")]
    private string _tag;
    public string Tag { get { return _tag; } private set { _tag = value; } }

    [SerializeField] [Tooltip("Whether objects with this tag are detected by inverse distance observations (a primitive smell-vision)")]
    private bool _isDetectedBySmell;
    public bool IsDetectedBySmell { get { return _isDetectedBySmell; } private set { _isDetectedBySmell = value; } }
    
    [SerializeField] [Tooltip("Whether objects with this tag are detected (distinctively) by touch observation")]
    private bool _isDetectedByTouch;
    public bool IsDetectedByTouch { get { return _isDetectedByTouch; } private set { _isDetectedByTouch = value; } }
    
    [SerializeField] [Tooltip("Whether objects with this tag are considered food by the animat, i.e. it can try to eat them")]
    private bool _isFood;
    public bool IsFood { get { return _isFood; } private set { _isFood = value; } }
    
    [SerializeField] [Tooltip("(OPTIONAL) Name of the sensory variable associated to smell observations of objects with this tag."
        + "The same sensory variable has to be defined in the homeostatic network as well.")]
    private string _sensoryVariable;
    public string SensoryVariable { get { return _sensoryVariable; } private set { _sensoryVariable = value; } }

    [SerializeField] [Tooltip("Whether objects with this tag are observed by preprocessed vision.")]
    private bool _isDetectedByPreprocessedVision;
    public bool IsDetectedByPreprocessedVision { get { return _isDetectedByPreprocessedVision; } private set { _isDetectedByPreprocessedVision = value; } }
    
    
    /*Currently unused
    private bool _isClose;
    public bool IsClose { get { return _isClose; } private set { _isClose = value; } }
    
    private float _smellIntensity;
    public float SmellIntensity { get { return _smellIntensity; } private set { _smellIntensity = value; } }
    
    private float _maxSmellExperienced;
    public float MaxSmellExperienced { get { return _maxSmellExperienced; } private set { _maxSmellExperienced = value; } }*/
}
