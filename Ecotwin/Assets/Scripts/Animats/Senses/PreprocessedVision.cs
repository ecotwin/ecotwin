using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PreprocessedVision
{
    private List<ObservableTag> _observables;
    private int _numberObservedPerObservable;

    private AreaManager _areaManager;
    private Animat _animat;

    // What is observed as "zero observation", for example when not numberObservedPerObservable is not found, this is observed to indicate that nothing was found.
    private int _zeroObservationMagnitude = 99999;
    private float _visionRadius;

    public PreprocessedVision(List<ObservableTag> observables, AreaManager areaManager, Animat animat, int numberObservedPerObservable, float visionRadius)
    {
        _observables = observables; 
        _areaManager = areaManager;
        _animat = animat;
        _numberObservedPerObservable = numberObservedPerObservable;
        _visionRadius = visionRadius;
    }

    public List<float> Observe()
    {
        List<float> observation = new List<float>();
        foreach (ObservableTag observable in _observables)
        {
            // Add one to consider height over ground
            Vector3 headPosition = _animat.transform.position + Vector3.up;
            // Find all gameObjects with tag observable
            List<GameObject> objects = new List<GameObject>();
            List<ChunkManager> chunkManagers =_areaManager.GetChunkManagersWithinRadius(headPosition, _visionRadius);
            foreach (ChunkManager chunkManager in chunkManagers)
                objects.AddRange(chunkManager.FindGameObjectsOfTagWithinRadius(observable.Tag, headPosition, _visionRadius));

            // Sort them by relative distance to animat
            objects.Sort((g1, g2) => (Vector3.SqrMagnitude(headPosition - g1.transform.position) < Vector3.SqrMagnitude(headPosition - g2.transform.position)) ? -1 : 1);

            int loopLength = _numberObservedPerObservable;
            for (int i = 0; i < loopLength; i++)
            {
                // If we try to observe more objects than exist in environment
                if (i >= objects.Count)
                {
                    // Add zero observations
                    observation.Add(_zeroObservationMagnitude); //magnitude
                    // TODO, should angle maybe be randomized in this case?
                    observation.Add(0); //angle 
                }

                // If itself is included among the objects, skip this item and add one to loop to move to next
                else if (objects[i] == _animat.gameObject)
                {
                    loopLength++;
                }

                // Else, observe magnitude and relative angle in xz-plane
                else
                {
                    // Check if a ray can be connected with the object without passing through anything on its way
                    Vector3 sightVector = objects[i].GetComponent<Collider>().ClosestPoint(headPosition) - headPosition;
                    RaycastHit hit;
                    
                    if (objects[i].layer != 4)
                        Physics.Raycast(headPosition, sightVector, out hit, sightVector.magnitude+1, 4);
                    else
                        Physics.Raycast(headPosition, sightVector, out hit, sightVector.magnitude+1);
                    
                    // If the ray hits something closer than the object, add one to loop in order to check if next object is observable
                    // -1 since the raycast hits the collider which is closer than the actual position of the object. This will not work when there is a large difference between where the collider and the transform is.
                    // TODO Maybe let observable objects has a "thickness" property of similar instead of taking -1.
                    
                    // If no obstacle, observe it
                    if (objects[i] == hit.transform.gameObject)
                    {
                        // Debug.DrawRay(headPosition, sightVector, Color.yellow);
                        observation.Add(sightVector.magnitude);
                        // Set sightVectortors to equal y values to get xz plane angle
                        sightVector.y = 0;
                        Vector3 adjustedForward = _animat.transform.forward;
                        adjustedForward.y = 0;
                
                        // Observe signed relative angle normalized
                        observation.Add(Vector3.SignedAngle(adjustedForward, sightVector, Vector3.up)/180);
                    }
                    else
                    {
                        loopLength++;
                    }
                }
            }
        }
        return observation;
    }
}