﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Represents the animat's ability of sensing light intensities
/// </summary>
public class LightSense
{
    private AreaManager environment;
    private HomeostasisModule homeostasis; 
    private Animat animat;

    public LightSense(AreaManager environment, HomeostasisModule homeostasis, Animat animat)
    {
        this.environment = environment;
        this.homeostasis = homeostasis;
        this.animat = animat;
    }

    public IEnumerable<float> GetSense()
    {
        if (environment.DayNightCycle)
        {
            float intensity = GetLightIntensity();
            //homeostasis.SetValue("light", intensity);
            return new List<float> { intensity };
        }
        
        return new List<float>();
    }

    public float GetLightIntensity()
    {
        if (environment.DayNightCycle)
            return environment.GetLightIntensity(animat.transform.position);

        return 0f;
    }
}

