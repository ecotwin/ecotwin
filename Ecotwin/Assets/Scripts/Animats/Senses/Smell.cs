﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The animat's sense of smell, based on the distance to objects
/// </summary>
public class Smell
{
    private readonly List<string> _observableTags;
    private AreaManager _areaManager;
    private Animat _animat;

    public Smell(List<ObservableTag> observableTags, AreaManager environment, Animat animat)
    {
        _observableTags = (from tag in observableTags select tag.Tag).ToList();
        _areaManager = environment;
        _animat = animat;
    }

    public List<float> GetSense()
    {
        List<float> obs = new List<float>();
        foreach(string tag in _observableTags)
            obs.AddRange(GetSmell(tag));
        
        return obs;
    }

    private List<float> GetSmell(string tag)
    {
        Vector3? smellGradient;
        if (false && tag == _animat.tag)
        {
            smellGradient = GetSmellGradient(
                _areaManager.GetSmellables(tag, _animat.transform.position, _animat.Phenotype.SmellRadius).Where(
                    x => x.GetComponent<Animat>() == null || (!x.Equals(_animat.gameObject) && x.GetComponent<Animat>().IsFemale != _animat.IsFemale)
                )
            );
        }
        else
        {
            smellGradient = 
                (_animat.LightSensitive) 
                ? GetSmellGradient(_areaManager.GetSmellVisionables(tag, _animat.transform.position, _animat.Phenotype.SmellRadius).Where(
                    x => x.Item1 != _animat.gameObject && x.Item2 > _animat.MinVisibleLight).Select(x => x.Item1))
                : GetSmellGradient(_areaManager.GetSmellables(tag, _animat.transform.position, _animat.Phenotype.SmellRadius).Where(
                    x => x != _animat.gameObject)
            );
        }
            
        float smellOnAnimat = 0;
        float smellAboveAnimat = 0;
        float smellRightOfAnimat = 0;

        if (smellGradient.HasValue)
        {
            smellOnAnimat = smellGradient.Value.sqrMagnitude <= 0.25 ? 1 : 0;
            smellAboveAnimat = smellGradient.Value.sqrMagnitude == 0 ? 0 : Vector3.Dot(_animat.transform.forward, smellGradient.Value);
            smellRightOfAnimat = smellGradient.Value.sqrMagnitude == 0 ? 0 : Vector3.Dot(_animat.transform.right, smellGradient.Value);
        }
        List<float> smellGradients = new List<float>() { smellOnAnimat, smellAboveAnimat, smellRightOfAnimat };
        //float lightIntensity = (_animat.LightSensitive) ? _animat.LightSense.GetLightIntensity() : 1f;
        //_animat.Homeostasis.SetValue($"{tag}SmellVision", Mathf.Min(lightIntensity * smellGradients.Sum(), 1));
        return smellGradients;
    }

    private Vector3? GetSmellGradient(IEnumerable<GameObject> smellables)
    {
        Vector3 smellGradient = new Vector3();
        if (smellables.Count() == 0)
            return null;
        foreach (GameObject s in smellables)
        {
            Vector3 gradient = (s.transform.position - _animat.transform.position);
            float dist = gradient.sqrMagnitude < _animat.Phenotype.SmellRadius ? gradient.sqrMagnitude : 0f;
            smellGradient += gradient.normalized / Mathf.Max(1, dist);
        }
            
        return smellGradient;
    }
}
