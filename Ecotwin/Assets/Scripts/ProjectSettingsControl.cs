﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to control the time in-between time steps
/// </summary>
public class ProjectSettingsControl : MonoBehaviour
{   
    [SerializeField] 
    private bool _runAtDemoSpeed = false;

    [SerializeField] [Tooltip("The fixed delta time when using runAtDemoSpeed.")]
    private float _demoDeltaTime = .1f;
    
    [SerializeField] [Tooltip("The fixed delta time when NOT using runAtDemoSpeed.")]
    private float _trainingDeltaTime = .005f;
    
    
    private void Awake()
    {
        if (_runAtDemoSpeed)
            Time.fixedDeltaTime = _demoDeltaTime;
        else
            Time.fixedDeltaTime = _trainingDeltaTime;
    }
}
