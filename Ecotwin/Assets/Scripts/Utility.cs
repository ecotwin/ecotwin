using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    public class ConditionalField { }
    [Serializable]
    public class ConditionalField<T>
    {
        public bool IsUsed;
        [ConditionalEnable("IsUsed")]
        public T Value;
    }

    public class ConditionalFieldCode { }
    [Serializable]
    public class ConditionalFieldCode<T>
    {
        [HideInInspector]
        public bool IsUsed = true;
        [ConditionalEnable("_isUsed")]
        public T Value;

        public void Disable()
        {
            IsUsed = false;
        }
    }
}