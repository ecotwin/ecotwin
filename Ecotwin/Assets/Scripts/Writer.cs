﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

/// <summary>
/// Writes data from simulations to .csv-files for later plots and graphs
/// </summary>
public class Writer
{
    private static string timestamp = DateTime.Now.ToString("yyy_MM_dd_HH.mm.ss");
    private static string folderName = null;

    public static void SetFolderName(string name)
    {
        folderName = $"{name}_{timestamp}";
    }

    /// <summary>
    /// Appends information to an existing .csv-file or creates a new one if it does not exist.
    /// Formatting: floating-point numbers use (.) => e.g. 1.33 and values are separated by (;)
    /// </summary>
    public static void WriteInformation(string type, string name, Dictionary<string, string> info)
    {
        if(folderName == null)
        {
            Debug.Log("No foldername set. Skipping logging");
            return;
        }
        string infoFile = $"results/{folderName}/{type}.csv";
        bool fileExists = File.Exists(infoFile);
        if (!fileExists)
            Directory.CreateDirectory(Path.GetDirectoryName(infoFile));

        using (StreamWriter sw = File.AppendText(infoFile))
        {
            List<KeyValuePair<string, string>> orderedInfo = info.OrderBy(x => x.Key).ToList();
            if (!fileExists) // set header
                sw.WriteLine($"name;{String.Join(";", orderedInfo.Select(x => x.Key)).Replace(',', '.')}");
                
            sw.WriteLine($"{name};{String.Join(";", orderedInfo.Select(x => x.Value)).Replace(',', '.')}");
        }
    }
}