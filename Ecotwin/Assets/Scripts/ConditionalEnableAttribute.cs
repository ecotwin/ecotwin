using UnityEngine;
using System;

//Original version of the ConditionalHideAttribute created by Brecht Lecluyse (www.brechtos.com)

/// <summary>
/// Disables/enables a variable in the inspector depending on a boolean flag
/// </summary>
[AttributeUsage(AttributeTargets.Field)]
public class ConditionalEnableAttribute : PropertyAttribute
{
    public readonly string ConditionalSourceField;
    public bool HideInInspector = false;

    // Use this for initialization
    public ConditionalEnableAttribute(string conditionalSourceField)
    {
        ConditionalSourceField = conditionalSourceField;
    }
}