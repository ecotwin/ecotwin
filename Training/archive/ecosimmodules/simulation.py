import os
import threading
import warnings
import torch
import gym

from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.base_class import BaseAlgorithm

# Import PPO material
from stable_baselines3 import PPO
from stable_baselines3.ppo import MlpPolicy as ppo_mlp_policy

# Import DQN material
from stable_baselines3 import DQN
from stable_baselines3.dqn import MlpPolicy as dqn_mlp_policy

from ecosimmodules import gymutils, utils, gymmodels

MUTATION_CONSTANT = .02

class Thread_abort_callback(BaseCallback):
    def __init__(self, gym_env, verbose=0):
        super(Thread_abort_callback, self).__init__(verbose)
        self.gym_env = gym_env

    def _on_step(self) -> bool:
        return self.gym_env.is_agent_alive

def start_training(model, n_steps, gym_env, verbose=0):
    if isinstance(model, BaseAlgorithm):
        # Is using stable stable baselines
        callback = Thread_abort_callback(gym_env, verbose)
        model.learn(total_timesteps=n_steps+1+300, callback=callback)
        # TODO: The 300 is just a hack to fix a bug. Remove this and make proper solution.
    else:
        # Not using stable baselines
        model.learn(total_timesteps=n_steps+1+300)
    # print("Model completed training.")

def _mutate_model(gym_model, verbosity_int=0):
    if isinstance(gym_model, PPO):
        params = gym_model.get_parameters()

        for tensor in params["policy"].values():
            std = tensor.std()
            if std.isnan():
                std = .1 * tensor # TODO: This case is for single digit tensors, is this a reasonalbe approach?
            tensor.data += MUTATION_CONSTANT * std * torch.randn_like(tensor)

        gym_model.set_parameters(params, exact_match=True)
    elif isinstance(gym_model, gymmodels.Random_behaviour):
        if verbosity_int >= 1:
            print("Random model cannot be mutated.")
    else:
        NotImplementedError("A model that does not have mutation implemented is trying to be mutated.")

def _load_pretrained_PPO_model(agent_type, simulation_dir_path, gym_env, top_save_dir, verbosity_int, mutate_model):
    if agent_type == "GrassEater":
        gym_model = PPO.load(os.path.join(top_save_dir, "pretrained", "GrassEater_PPO_Evolution", "0002"),
                              tensorboard_log=simulation_dir_path)
    elif agent_type == "Predator":
        gym_model = PPO.load(os.path.join(top_save_dir, "pretrained", "Predator_PPO_trainedAsGrassEater", "20000"),
                             tensorboard_log=simulation_dir_path)
    if mutate_model:
        # Mutates in place
        _mutate_model(gym_model)

    gym_model.set_env(gym_env) # TODO: This can be moved into the load command
    return gym_model

def _choose_model(behavior_name, gym_env, use_pretrained_models, verbosity_int, top_save_dir, simulation_dir_path, mutate_model = False, **kwargs):
    agent_type, agent_model, _, _ = utils.decode_agent_name(behavior_name)

    # TODO (later, low prio): create method that can control different dimensions on the
    # observation and action space and match that with a pretrained model
    if agent_model == "PPO" or "Evolution":
        if use_pretrained_models:
            if verbosity_int >= 1:
                print("Loading pretrained PPO model for ", behavior_name.replace("?team=0",""))
            gym_model = _load_pretrained_PPO_model(agent_type, simulation_dir_path, gym_env, top_save_dir, verbosity_int, mutate_model)

#             PPO.load(os.path.join(top_save_dir, "pretrained", "GrassEater_PPO_noPredators", "50000"),
#                                  tensorboard_log=simulation_dir_path)
#             gym_model.set_env(gym_env)
        else:
            if verbosity_int >= 1:
                print("Loading new (not pretrained) PPO model for ", behavior_name.replace("?team=0",""))
            gym_model = PPO(ppo_mlp_policy, gym_env, verbose=verbosity_int, tensorboard_log=simulation_dir_path, **kwargs)

    elif agent_model == "DQN":
        if use_pretrained_models:
            if verbosity_int >= 1:
                print("Loading pretrained DQN model for ", behavior_name.replace("?team=0",""))
            raise NotImplementedError("There are no pretrained DQN yet.")
            gym_model = DQN.load(os.path.join(top_save_dir, "pretrained", "DQN"),
                                 tensorboard_log=simulation_dir_path)
            gym_model.set_env(gym_env)
        else:
            if verbosity_int >= 1:
                print("Loading new (not pretrained) DQN model for ", behavior_name.replace("?team=0",""))
            gym_model = DQN(dqn_mlp_policy, gym_env, verbose=verbosity_int)
    elif agent_model in ["RANDOM", "Random"]:
        if verbosity_int >= 1:
            print("Loading random model for ", behavior_name.replace("?team=0",""))
        gym_model = gymmodels.Random_behaviour(gym_env)
    elif type in ["TowardFood", "towardFood", "TowardsFood", "towardsFood"]:
        if verbosity_int >= 1:
            print("Loading 'toward food' model for ", behavior_name.replace("?team=0",""))
        gym_model = gymmodels.Toward_food_behaviour(gym_env)
        # gym_model = gymmodels.Toward_food_behaviour(gym_env)
    else:
        warning.warn("Behavior does not match existing model, defaulting to PPO.")
        if use_pretrained_models:
            if verbosity_int >= 1:
                print("Loading pretrained PPO model for ", behavior_name.replace("?team=0",""))
            gym_model = PPO.load(os.path.join(top_save_dir, "pretrained", "PPO_10000"),
                                 tensorboard_log=simulation_dir_path)
            gym_model.set_env(gym_env)
        else:
            if verbosity_int >= 1:
                print("Loading new (not pretrained) PPO model for ", behavior_name.replace("?team=0",""))
            gym_model = PPO(ppo_mlp_policy, gym_env, verbose=verbosity_int, tensorboard_log=simulation_dir_path)

    return gym_model

def _load_genotype(type: str, number: str, simulation_dir_path, gym_env: gym.Env):
    path = os.path.join(simulation_dir_path, "genotypes", number)
    if type == "PPO" or "Evolution":
        gym_model = PPO.load(path, tensorboard_log=simulation_dir_path)
    elif type in ["Random", "RANDOM"]:
        print(f"There is not genotype for agent of type {type}, returning new Random_behaviour")
        gym_model = gymmodels.Random_behaviour(gym_env)
    elif type in ["TowardFood", "towardFood", "TowardsFood", "towardsFood"]:
        print(f"There is not genotype for agent of type {type}, returning new Toward_food_behaviour")
        gym_model = gymmodels.Toward_food_behaviour(gym_env)
    else:
        NotImplementedError(f"There is no genotype loader implemented for {type}")
    return gym_model

def _save_genotype(gym_model, number: str, simulation_dir_path):
    # TODO: Make this more safe, catch error and print informative message
    gym_model.save(os.path.join(simulation_dir_path, "genotypes", number))

def create_model_from_genotype(agent_name, gym_env, top_save_dir, simulation_dir_path, use_pretrained_models, pretrained_path, verbosity_int):
    agent_type, agent_model, parent_ids, agent_id = utils.decode_agent_name(agent_name)

    genotype_number = parent_ids[0] # TODO: When adding sexual reproduction, change this.
    # TODO: clean this code, it is quite messy, and somewhat duplicated in other methods
    if int(genotype_number) == 0:
        if agent_model == "PPO":
            if use_pretrained_models:
                gym_model = _load_pretrained_PPO_model(agent_type, simulation_dir_path, gym_env, top_save_dir, verbosity_int, False)
            else:
                gym_model = PPO(ppo_mlp_policy, gym_env, verbose=verbosity_int, tensorboard_log=simulation_dir_path)
        elif agent_model in ["Random", "RANDOM"]:
            gym_model = gymmodels.Random_behaviour(gym_env)
        elif agent_model in ["TowardFood", "towardFood", "TowardsFood", "towardsFood"]:
            gym_model = gymmodels.Toward_food_behaviour(gym_env)
        elif agent_model in ["Evolution", "EVOLUTION"]:
            if use_pretrained_models:
                #gym_model = _load_pretrained_PPO_model(agent_type, simulation_dir_path, gym_env, top_save_dir, verbosity_int, False)
                gym_model = gymmodels.Evolution_model.load(pretrained_path,
                              tensorboard_log=simulation_dir_path, env=gym_env)
            else:
                gym_model = gymmodels.Evolution_model(ppo_mlp_policy, gym_env, verbose=verbosity_int, n_steps=1024)
        else:
            NotImplementedError(f"Model creation for agent using {agent_model} is not yet implemented.")
    else:
        gym_model = _load_genotype(agent_model, genotype_number, simulation_dir_path, gym_env)
    if not (isinstance(gym_model, gymmodels.Random_model) or
            isinstance(gym_model, gymmodels.Toward_food_model)) :
        _mutate_model(gym_model, verbosity_int)
        _save_genotype(gym_model, agent_id, simulation_dir_path)
        gym_model.set_env(gym_env)
    return gym_model


def create_behavior(
    unity_env,
    gym_manager,
    behavior_name,
    top_save_dir,
    simulation_dir_path,
    agent_wait_seconds,
    training_steps,
    using_stable_baselines = False,
    use_pretrained_models = False,
    pretrained_path = "",
    mutate_model = False,
    use_inheritance = True,
    verbosity_int = 0,
    **kwargs
    ):
    spec = unity_env.behavior_specs[behavior_name]

    # Create a brain for each agent
    observation_shape = spec.observation_shapes
    #observation_shape = spec.observation_specs[0].shape
    action_size = spec.action_spec.discrete_size #spec.action_size
    action_branch = spec.action_spec.discrete_branches #spec.discrete_action_branches

    # spec.action_spec.continuous_size: int
    # spec.action_spec.discrete_branches: Tuple[int, ...]

    discrete_action_space = True # TODO: determine this somehow

    gym_env = gymutils.Gym_env(
        gym_manager = gym_manager,
        behavior_name = behavior_name,
        observation_shapes = observation_shape,
        is_action_space_discrete = discrete_action_space,
        action_space_size = action_size,
        agent_wait_seconds = agent_wait_seconds,
        action_space_range = action_branch, # TODO: Does this need to be formatted?
        using_stable_baselines = using_stable_baselines
    )
    gym_manager.add_env(gym_env)

    if use_inheritance:
        gym_model = create_model_from_genotype(
            behavior_name,
            gym_env,
            top_save_dir,
            simulation_dir_path,
            use_pretrained_models,
            pretrained_path,
            verbosity_int)
    else:
        gym_model = _choose_model(
            behavior_name = behavior_name,
            gym_env = gym_env,
            use_pretrained_models = use_pretrained_models,
            verbosity_int = verbosity_int,
            top_save_dir = top_save_dir,
            simulation_dir_path = simulation_dir_path,
            mutate_model = mutate_model, **kwargs)

    gym_thread = threading.Thread(target=start_training, args=(gym_model, training_steps, gym_env, verbosity_int))

    behavior = utils.Behavior(
        behavior_name = behavior_name,
        gym_env = gym_env,
        gym_model = gym_model,
        is_dead = False,
        trajectory = [],
        thread = gym_thread,
        thread_started = False,
        observations = None
    )

    return behavior