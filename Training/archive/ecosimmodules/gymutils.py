import gym
from gym import spaces
from typing import List, Any
import time
import numpy as np

# Own modules
from ecosimmodules import utils

class Gym_env(gym.Env):
    """Custom Environment that follows gym interface"""
    metadata = {'render.modes': ['human']}

    def __init__(
            self,
            gym_manager, # TODO: want this to be: gym_manager: Gym_envs_manager, but this does not work due to cross referencing. How to solve?
            behavior_name: str,
            observation_shapes,
            # observation_range,
            is_action_space_discrete: bool,
            action_space_size: int,  # TODO: check if type is correct
            agent_wait_seconds: float,
            action_space_range: [int] = None, # TODO: check if this is the best type for this
            uint8_visual: bool = False,
            using_stable_baselines: bool = False
            ):
        super(Gym_env, self).__init__()

        # Create necessary variables
        self.is_ready_for_unity_action: bool = False
        self.is_observations_updated: bool = False
        self.chosen_actions = None

        self.current_step_information: utils.GymStepResult = None

        self.manager = gym_manager
        self.behavior_name = behavior_name

        self.using_stable_baselines = using_stable_baselines
        self.agent_wait_seconds = agent_wait_seconds

        # Define action and observation space, they should be gym.spaces objects
        if is_action_space_discrete:
            if action_space_size == 1:
                self.action_space = spaces.Discrete(action_space_range[0])
            else:
                self.action_space = spaces.MultiDiscrete(action_space_range)
        else:
            # TODO: implement continous action space
            raise NotImplementedError("Continous action space not yet implemented.")

        self._set_observation_space(observation_shapes, uint8_visual)

        self.is_agent_alive = True


    def step(self, action: List[Any]) -> utils.GymStepResult:
        # Set action
        self.chosen_actions = action.reshape(1, -1) # We have one agent per behavior, hence the "empty" first dimension

        self.is_ready_for_unity_action = True

        # Wait for unity step
        while not self.is_observations_updated:
            time.sleep(self.agent_wait_seconds)

        # When action is used, we are not ready for next step
        # self.is_ready_for_unity_action = False
        # Note: This can be problematic if we check is_ready before we put it to False because we wait above and the
        # check frequency is not high enough. Will this be a problem (analyse flow) and can it be solved by doing
        # additional checks?
        # ANSWER: Yes, this is a problem. Remove this and set this by the manager instead.

        # Set the observation updated to false as the information is used.
        self.is_observations_updated = False

        # Return observation, reward, done, info from gym manager
        return self.current_step_information

    def reset(self):
        # TODO: Should this do something? Maybe flag for reset somehow?
        # Return: Current observation (reward, done, info can not be included)
        return self.current_step_information[0]

    def render(self, mode='human'):
        pass

    def close (self):
        pass
        # TODO: End this gym env or not?

    def update_observations(self, data: utils.GymStepResult) -> None:
        self.current_step_information = data
        self.is_observations_updated = True

    def _set_observation_space(self, observation_shapes, uint8_visual) -> None:
        # This method is inspired from the gym wrapper in mlagents

        # Build observations space
        list_spaces: List[gym.Space] = []

        # Pick put the visual observations
        visual_shapes: List[Tuple] = []
        for shape in observation_shapes:
            if len(shape) == 3:
                visual_shapes.append(shape)
        for shape in visual_shapes:
            if uint8_visual:
                list_spaces.append(spaces.Box(0, 255, dtype=np.uint8, shape=shape))
            else:
                list_spaces.append(spaces.Box(0, 1, dtype=np.float32, shape=shape))

        # Pick out the vector observations
        n_vector_obs = 0
        for shape in observation_shapes:
            if len(shape) == 1:
                n_vector_obs += shape[0]
        if n_vector_obs > 0:
            # vector observation is last
            high = np.array([np.inf] * n_vector_obs) # TODO: check if this is always the case
            list_spaces.append(spaces.Box(-high, high, dtype=np.float32))

        # Set the obsevation space
        if self.using_stable_baselines:
            # print("Using stable baselines requires to only use one observation space. Picking the first one.")
            # NOTE: Sable baselines only accepts one observation space (when this code was written).
            # Picking the first one.
            self.observation_space = list_spaces[0]
        else:
            self.observation_space = spaces.Tuple(list_spaces)


class Gym_envs_manager():
    # __instance = None
    #
    # # Make this class a singleton (do not implement a constructor)
    # def __new__(self):
    #     if self.__instance is None:
    #         print("Creating the gym manager")
    #         self.__instance = super(Gym_envs_manager, self).__new__(self)
    #         # Put any initialization here.
    #     else:
    #         print("Returning the already existing gym manager")
    #     return self.__instance

    def add_env(self, gym_env: Gym_env):
        self.gym_envs[gym_env.behavior_name] = gym_env

    def remove_env(self, behavior_name: str):
        self.gym_envs.pop(behavior_name, None)

    def ready_for_unity_step(self) -> bool:
        is_ready = True
        for name, gym_env in self.gym_envs.items():
            if not gym_env.is_ready_for_unity_action:
                is_ready = False
                break # If one behavior is not ready, we don't need to check the rest
        return is_ready

    def actions_to_take(self, behavior_names) -> dict:
        actions = {}
        for behavior_name in behavior_names:
            actions[behavior_name] = self.gym_envs[behavior_name].chosen_actions
        return actions

    def set_agents_observations(self, observation_dict):
        for behavior_name in observation_dict:
            self.gym_envs[behavior_name].update_observations(observation_dict[behavior_name])

    def set_gym_envs_not_ready(self):
        for gym_env in self.gym_envs.values():
            gym_env.is_ready_for_unity_action = False

    def reset_manager(self):
        self.gym_envs: Dict[str, Gym_env] = {}
