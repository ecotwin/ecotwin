from typing import List, Tuple, Dict, NamedTuple, Any
import numpy as np
import threading
from dataclasses import dataclass

class Experience(NamedTuple):
  """
  An experience contains the data of one Agent transition.
  - Observation
  - Action
  - Reward
  - Simulation time
  """

  obs: np.ndarray
  action: np.ndarray
  reward: float
  simulation_time: int

# A Trajectory is an ordered sequence of Experiences
Trajectory = List[Experience]

# Template for the tuple returned by gym_env.step()
GymStepResult = Tuple[np.ndarray, float, bool, Dict]

# Used to store agent specific information sent by the agent in Unity via side channel
class Homeostasis(NamedTuple):
    step: np.ndarray
    energy: np.ndarray
    # To be extended when more homeostatic variables are added

# Used to store general simulation information sent by the environment in Unity via side channel
class Simulation_data(NamedTuple):
    step: np.ndarray
    n_predators: np.ndarray
    n_grass_eaters: np.ndarray
    n_good_food: np.ndarray
    n_bad_food: np.ndarray


# TODO Should we remove this somehow? replace it with a reference to a variable? (only used in Info_to_python_channel)
class Simulation_info():
    def __init__(self):
        self.simulation_step: int = 0

# TODO: look into if this dataclass should be combined with the information about the agents
# from Unity to store all data in one place.
@dataclass
class Behavior:
    behavior_name: str
    gym_env: Any # gymutils.Gym_env # TODO: this reference is problematic due to circular reference.
    gym_model: object  # TODO: Can we be more specific? Maybe not...
    is_dead: bool
    trajectory: Trajectory
    thread: threading.Thread
    thread_started: bool
    observations: GymStepResult

def decode_agent_name(behavior_name: str):
    behavior_info = behavior_name.replace("?team=0", "").split("_")
    agent_type = behavior_info[0]
    agent_model = behavior_info[1]
    parent_ids = behavior_info[2].split("-")
    agent_id = behavior_info[-1]
    return agent_type, agent_model, parent_ids, agent_id
