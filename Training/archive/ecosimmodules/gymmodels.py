import gym
import pathlib
import io
import time
import numpy as np
import torch as th

from gym import spaces
from stable_baselines3 import PPO

from stable_baselines3.common.policies import ActorCriticPolicy

from stable_baselines3.common.type_aliases import GymEnv, MaybeCallback, Schedule
from stable_baselines3.common.vec_env import VecEnv
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.buffers import RolloutBuffer
from typing import Any, Dict, List, Optional, Tuple, Type, Union
from abc import ABC, abstractmethod

toggle_print = False

class Model(ABC):
    def __init__(self, gym_env: gym.Env):
        self.gym_env = gym_env
        self.num_timesteps = 0
        self.obs = None

    @abstractmethod
    def learn(self, *args, **kwargs):
        pass
    
    @abstractmethod
    def save(*args, **kwargs):
        pass

    def set_env(gym_env: gym.Env):
        self.gym_env = gym_env

class Random_model(Model):
    def __init__(self, gym_env: gym.Env):
        super().__init__(gym_env)

    def learn(self, total_timesteps: int):
        while self.num_timesteps < total_timesteps:
            choice = self.gym_env.action_space.sample()
                
            actions = np.array([choice])
            self.gym_env.step(actions)
            
            self.num_timesteps += 1
            if not self.gym_env.is_agent_alive:
                break

    def save(*args):
        print("Random_model does not have an implemented save function!")

class Toward_food_model(Model):
    def __init__(self, gym_env: gym.Env):
        Exception("Toward_food_model is outdated!")
        super().__init__(gym_env)

    def learn(self, total_timesteps: int):
        while self.num_timesteps < total_timesteps:
            if self.obs is not None:
                if np.all(self.obs[0:4] == 0):
                    # The agent idles (action=0) if the sensor input is 0.
                    actions = 0
                else:
                    actions = np.argmax(self.obs[0:4]) + 1
            else:
                actions = 0  # Stay if no observation
            actions = np.array([actions])
            self.obs, _, _, _ = self.gym_env.step(actions)

            self.num_timesteps += 1
            if not self.gym_env.is_agent_alive:
                break

    def save(*args):
        print("Toward_food_model does not have an implemented save function!")

# Evolution model:
# Sub-class of PPO with support for action masks
class Evolution_model(PPO):
    def __init__(self,
                 policy: Union[str, Type[ActorCriticPolicy]],
                 env: Union[GymEnv, str],
                 learning_rate: Union[float, Schedule] = 3e-4,
                 n_steps: int = 2048,
                 batch_size: Optional[int] = 64,
                 n_epochs: int = 10,
                 gamma: float = 0.99,
                 gae_lambda: float = 0.95,
                 clip_range: Union[float, Schedule] = 0.2,
                 clip_range_vf: Union[None, float, Schedule] = None,
                 ent_coef: float = 0.0,
                 vf_coef: float = 0.5,
                 max_grad_norm: float = 0.5,
                 use_sde: bool = False,
                 sde_sample_freq: int = -1,
                 target_kl: Optional[float] = None,
                 tensorboard_log: Optional[str] = None,
                 create_eval_env: bool = False,
                 policy_kwargs: Optional[Dict[str, Any]] = None,
                 verbose: int = 0,
                 seed: Optional[int] = None,
                 device: Union[th.device, str] = "auto",
                 _init_setup_model: bool = True):
        # Init PPO
        super().__init__(policy, 
                         env, 
                         learning_rate=learning_rate, 
                         n_steps=n_steps, 
                         batch_size=batch_size, 
                         n_epochs=n_epochs,
                         gamma=gamma,
                         gae_lambda=gae_lambda,
                         tensorboard_log=tensorboard_log,
                         policy_kwargs=policy_kwargs,
                         verbose=verbose,
                         seed=seed,
                         device=device,
                         _init_setup_model=_init_setup_model)

        # Initiate action mask with ones (allow all actions)
        self.mask = np.ones(env.action_space.n)
        self.mask_ready = False
        self.is_dead = False

    # Edited version of OnPolicyAlgorithm.collect_rollouts with action mask support 
    def collect_rollouts(
        self, env: VecEnv, callback: BaseCallback, rollout_buffer: RolloutBuffer, n_rollout_steps: int
    ) -> bool:
        """
        Collect experiences using the current policy and fill a ``RolloutBuffer``.
        The term rollout here refers to the model-free notion and should not
        be used with the concept of rollout used in model-based RL or planning.

        :param env: The training environment
        :param callback: Callback that will be called at each step
            (and at the beginning and end of the rollout)
        :param rollout_buffer: Buffer to fill with rollouts
        :param n_steps: Number of experiences to collect per environment
        :return: True if function returned with at least `n_rollout_steps`
            collected, False if callback terminated rollout prematurely.
        """
        if toggle_print:
            print("Collecting rollouts")
        assert self._last_obs is not None, "No previous observation was provided"
        n_steps = 0
        i = 0
        rollout_buffer.reset()
        # Sample new weights for the state dependent exploration
        if self.use_sde:
            self.policy.reset_noise(env.num_envs)

        callback.on_rollout_start()

        while n_steps < n_rollout_steps:
            if self.use_sde and self.sde_sample_freq > 0 and n_steps % self.sde_sample_freq == 0:
                # Sample a new noise matrix
                self.policy.reset_noise(env.num_envs)

            with th.no_grad():
                # Convert to pytorch tensor
                while not(self.mask_ready) and not(self.is_dead):
                    time.sleep(0.001)
                self.mask_ready = False
                obs_tensor = th.as_tensor(self._last_obs).to(self.device)

                ### Choose action if there is an existing mask (mask+policy.forward)
                action_mask = self.mask
                if not all(action_mask):
                    #if toggle_print:
                        #print("Reflex triggered!")
                        #print(action_mask)
                    latent_pi, latent_vf, latent_sde = self.policy._get_latent(obs_tensor)
                    # Evaluate the values for the given observations
                    values = self.policy.value_net(latent_vf) # only neccessary if training on masked step
                    distribution = self.policy._get_action_dist_from_latent(latent_pi, latent_sde=latent_sde)
                    ### Get new probabilities and normalize based on action mask
                    masked_probability = np.array([action_mask[a]*np.exp(distribution.log_prob(th.tensor(a)).item()) for a in range(self.env.action_space.n)])
                    masked_probability = masked_probability/sum(masked_probability)
                    ### Pick action stochastically based on masked probabilities
                    actions = np.random.choice(self.env.action_space.n, 1, p = masked_probability)
                    log_probs = th.tensor(np.log(masked_probability[actions])) # only really necessary if training on masked step
                else: 
                    actions, values, log_probs = self.policy.forward(obs_tensor)
                    actions = actions.cpu().numpy()
            
            #if toggle_print:
            #print("Action (1): " + str(actions))
            
            # Rescale and perform action
            clipped_actions = actions
            # Clip the actions to avoid out of bound error
            if isinstance(self.action_space, gym.spaces.Box):
                clipped_actions = np.clip(actions, self.action_space.low, self.action_space.high)
            new_obs, rewards, dones, infos = env.step(clipped_actions)
            #print("Action: " + str(actions) + ", Rewards: " + str(rewards) + "\nObservations: " + str(new_obs))
            #if not(all(action_mask)): 
            self.num_timesteps += env.num_envs

            # Give access to local variables
            callback.update_locals(locals())
            if callback.on_step() is False:
                return False
            self._update_info_buffer(infos)
            ###  Iterate step only if there's no action mask since we don't train on this step
            if all(action_mask):
                n_steps += 1
            i += 1
            #print("n_steps (1): " + str(i))
            #if not(all(action_mask)): 
            if isinstance(self.action_space, gym.spaces.Discrete):
                # Reshape in case of discrete action
                actions = actions.reshape(-1, 1)
            ### Add to rollout buffer for training only if there's no action mask
            if all(action_mask):
                rollout_buffer.add(self._last_obs, actions, rewards, self._last_dones, values, log_probs)
            
            #if not(all(action_mask)): 
            self._last_obs = new_obs
            self._last_dones = dones

        with th.no_grad():
            # Compute value for the last timestep
            obs_tensor = th.as_tensor(new_obs).to(self.device)
            _, values, _ = self.policy.forward(obs_tensor)

        rollout_buffer.compute_returns_and_advantage(last_values=values, dones=dones)

        callback.on_rollout_end()

        return True
