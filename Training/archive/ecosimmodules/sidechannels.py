from typing import Dict
import numpy as np
import uuid
import json
import os
from mlagents_envs.side_channel.side_channel import (
    SideChannel,
    IncomingMessage,
    OutgoingMessage,
) 



class Info_to_python_channel(SideChannel):
    def __init__(self, path):
        super().__init__(uuid.UUID("d3785fc4607fe74abe4a6fa103ccf59b"))
        self.dir_path = path
        self.headers = {}

    def store_values(self, value_type, values):
        path = os.path.join(self.dir_path,f'{value_type}.csv')
        with open(path, 'a') as f:
            if value_type not in self.headers:
                self.headers[value_type] = sorted(values.keys())
                print(','.join(self.headers[value_type]), file=f)

            # This convulated way is to ensure order of values
            # As sets are unordered
            sorted_values = [str(values[key]) for key in self.headers[value_type]]
            print(','.join(sorted_values), file=f)


    def on_message_received(self, msg: IncomingMessage) -> None:
        """
        This method is a part of the SideChannel interface that receives messages
        from Unity.
        """
        try:
            value_type = msg.read_string()
            name = msg.read_string()
            values = json.loads(msg.read_string())
            values['name'] = name
        except:
            raise Exception("Info_to_python_channel failed to load a received message! \n" +
                      "Debug to make sure the sent message is a JSON object!")
        
        self.store_values(value_type, values)


