# Test _mutate_model
from ecosimmodules import simulation
model = simulation._load_pretrained_PPO_model("GrassEater", "./results_dev/tmp", None, "./results_dev", 1)

params1 = model.get_parameters()
import copy
tmp = {}
for key, val in params1["policy"].items():
    tmp[key] = copy.deepcopy(val)

simulation._mutate_model(model)

params2 = model.get_parameters()
for key in params1["policy"]:
    print(tmp[key] - params2["policy"][key])
