import yaml
import argparse

from yaml.loader import SafeLoader

def create_dictionary(args):
    data = [{'behaviors':
                {'behaviorname1':
                    {'trainer_type':'ppo',
                    'hyperparameters': {
                        'batch_size': 256,
                        'buffer_size': 10240,
                        'learning_rate': 0.003}},
                'behaviorname2':
                    {'trainer_type':'ppo',
                    'hyperparameters': {
                        'batch_size': 256,
                        'buffer_size': 10240,
                        'learning_rate': 0.003}}}}]


    # Convert Python dictionary into a YAML document
    print(yaml.dump(data))
    return data

def get_filename(args):
    return filename

def dict_to_yaml(data, filename):


def create_jobscript(filename):

def main():

    parser = argparse.ArgumentParser(description="Generates training configurations mlagents training.")
    parser.add_argument(
        '--behaviors', nargs='*', type=str)
    parser.add_argument(
        '--max_steps', nargs='*', type=int)
    parser.add_argument(
        '--time_horizon', nargs='*', type=int)
    parser.add_argument(
        '--learning_rate', nargs='*', type=float)
    parser.add_argument(
        '--batch_size', help='List of batch_size parameters to evaluate', nargs='*', type=int)
    parser.add_argument(
        '--buffer_size', nargs='*', type=int)
    parser.add_argument(
        '--hidden_units', nargs='*', type=int)
    parser.add_argument(
        '--num_layers', nargs='*', type=int)
    parser.add_argument(
        '--beta', nargs='*', type=float)
    parser.add_argument(
        '--epsilon', nargs='*', type=float)
    parser.add_argument(
        '--lambd', nargs='*', type=float)

    parser.add_argument(
        '--create_jobscript', help='Create an associated jobscript for alvis?', action='store_true')
    parser.add_argument(
        '--env_name', help='Name of this training environment', default='env_name')
    parser.add_argument(
        '--folder', help='Which folder to save the training configurations to', default='folder')
    args = parser.parse_args()

    # Add check if buffer size is multiple of batch size


    filename = get_filename(args)
    data = create_dictionary(args)

    if args.create_jobscript == True:
        create_jobscript(filename)

if __name__ == "__main__":
		main()
