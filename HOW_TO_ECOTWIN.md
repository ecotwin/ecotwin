# Foreword (v1.0)
This document is intended to aid in getting started using Ecotwin. This document will however not go into detail explaining programming language specifics nor give a tutorial on how to use the Unity game engine. The aim of this document is instead to explain how the developed components interact and should be used/developed further. The version tags in each title shows the last version the respective section was last updated. If a section's title version is not updated, it means either the information has not changed, or that the information is outdated.


# Script Structure (v0.5)
The scripts developed are categorized into four groups: `Animats`, `Editor`, `Environments` and `Food`. There are also several scripts which are non-categorized due to not interacting with other scripts themselves.

## Animats (v0.5)
- `Animat.cs` is the behaviour of an animat. The class inherits from ML Agents' `Agent` class and makes use of actions defined in `/Actions` to interact with its environment based on senses in `/Senses` and the animat's internal state defined in `/Homeostasis`.
- `Species.cs` defines parameters for how many individuals of an animat species are to be created upon initialization during a simulation run.

### Actions (v0.5)
- `ActionModule.cs` is an abstract class used to find a common ground between all actions which an animat should be able to take. The purpose of the class is to facilitate the creation of animats with different behaviour by developing actions in modules which can thereafter be used in combination in any animat desired. Currently a restriction is that no action may share the same string name, or else it will not be compatible with a module with an action of the same name.
- `ActionHandler.cs` is a helper class used to reduce clutter in `Animat.cs` when an action is to be called, or when genes/reflexes are to be found. The class takes an array of actionmodules upon creation and when one of its methods is called it will call upon a similar method in all of these actionmodules before returning a result to the animat.
- `Attack.cs` implements an action for attacking a prey. The module is based on using tags to identify prey, thereby limiting the attackable animats to those the predator consider to be prey.
- `Eat.cs` implements an action for eating objects. The module considers only objects inheriting from `Consumable.cs` for this action.
- `Reproduction.cs` implements an action for reproduction amongst animats. The module does not achieve much on its own as a reference to the animat's sexual partner may be required. Instead the module flags whether asexual or sexual reproduction is performed and the act of reproducing is handled directly in `Animat.cs`.
- `ContinuousMovement.cs` implements actions for moving in a continuous environment (i.e. not grid-based). The module relies on a RigidBody for its movement and applies a force to the animat to accelerate/decelerate.
- `TerrainMovement.cs` implements actions for moving which are compatible with 3D-terrain. The module relies on a CharacterController for its movement and sets a velocity based upon the incline as well as the current terrain type.
- `TextureChecker.cs` is a helper class for `TerrainMovement.cs` in order to determine which texture (i.e. terrain type) is found in the animat's location.

### Senses (v0.5)
- `Lightsense.cs` gives observations of how much light is cast upon an animat.
- `ObservableTag.cs` is a helper class used to define which objects can be observed, and how.
- `Smell.cs` gives observations of how much scent of an object can be smelled in the animat's horizontal and vertical direction.
- `Touch.cs` gives observations of how many objects of a certain type are currently being touched.

### Homeostasis (v1.0)
- `HomeostasisModule.cs` is used to increase/decrease homeostatic variables in an animat. The class also offers the reward which should be given to an animat in each step.
- `HomeostasisFactory.cs` is a helper class used to initialize a homeostasismodule object.
- The other classes are used only internally by the classes above, and need not be changed by the common user.
- The animats' rewards are found by calculating delta happiness. If another happiness function is wanted, that is not based on the homeostatic variables, then as opposed to calling HomeostasisModule.Reward(), HomeostasisModule.GetSenseDictionary() can be called and a user can compose a new function as desired using the individual values.

## Editor (v0.5)
These scripts are used only to enable editing in the Unity inspector for custom classes (i.e. `ExtendedScriptableObject.cs` and `ConditionalEnable.cs`). The name of this folder may not be changed, nor may the files in it be moved to a folder of a different name, or else the functionality will cease to function.

## Environments (v0.5)
- `AreaManager.cs` initializes all animats and food objects upon the simulation's start. The class also resets all these objects at the end of an episode, and terminates the simulation in case all animats die.
- `ChunkManager.cs` keeps track of all animats and food objects in a subset of the simulation's scene. This subset area is called a chunk, and this is done only to optimize position checks performed by the areamanager. The fewer chunkmanagers there are, the more accurate the position checks are, however the more chunkmanagers there are, the faster the position checks are. Adjust your numbers accordingly.
- `BoardCreation.cs` is used to resize the scene's walls (if needed) and to define the "end of the world" which is used to restrict objects from spawning/moving too far away. (The resizing of walls is currently obsolete.)

## Food (v0.5)
- `Consumable.cs` is an abstract class used to define a common ground of all objects which may be consumed by an animat.
- `FoodType.cs` defines parameters for how many objects of a consumable are to be created upon initialization during a simulation run.
- `Meat.cs` is an implementation of the consumable class which is designed to decay over time and eventually cease to exist.
- `StaticFood.cs` is an implementation of the consumable class which is designed to never decay and only require exactly one eating action to fully ingest.
- `Water.cs` is an implementation of the consumable class which is designed to never decay and may never be fully ingested.

### Crops (v0.5)
- `Crop.cs` is an abstract class used to define a common ground for consumables which may ripen before decaying.
- `GrassCrop.cs` is an implementation of the crop class which assumes spread to nearby locations (using roots).
- `DandelionCrop.cs` is an implementation of the crop class which assumes spread to locations far away (using wind).

## Other (v0.5)
- `ConditionalEnable.cs` creates an attribute which may be used to flag a variable. Depending on whether the value of the flag, the variable will either be drawn/not drawn in the inspector.
- `ProjectSettingsControl.cs` is used to set `Time.fixedDeltaTime` to a wanted value (to speed up simulations if desired).
- `SerializableDictionary.cs` is used to enable dictionaries in the inspector.
- `SimulationInfoCanvas.cs` is used to display information of the environment state during the simulation's run.
- `Utility.cs` is a collection of classes which offer useful solutions, or hack-fixes to difficult problems. (Currently the only class here is the `ConditionalField` class used to add a boolean field for an automatic flag for the conditionalenable class.)
- `Writer.cs` takes all the information given by the animats and the environment and saves to a .csv-file which may later be used for plotting results.


# How To Assign Actions To An Animat (v0.5)
An animat has a serialized field called `_actionModules` (displayed as `Action Modules` in the inspector). This array should be filled with all the actionmodules with the actions an animat may take. To allow the actionmodules to be assigned in the inspector, first a scriptableobject must be created. NOTE: the attribute `[CreateAssetMenu(menuName = "Scriptable Objects/xxx")]` which is used as prefix to the class definition of all actionmodules allows creating a scriptableobject through the drop-down menu by right-clicking in Unity's project view (Create->Scriptable Object->xxx). Thanks to `ExtendedScriptableObjectDrawer.cs`, it is furthermore possible to edit the modules' values in the inspector directly as opposed to editing the scriptable objects separately. <br />
If there is no actionmodule defined to perform a desired action, it is possible to create a new actionmodule but it must inherit from `ActionModule.cs`. Do not reuse any string name identifier of an action used in another actionmodule. <br />
To create an actionmodule which supports certain actions being taken simultaneously, the action names must be placed in separate string arrays, e.g. `new string[][] { new string[] { "walk", "run" }, new string[] { "turnLeft", "turnRight" } }` will allow walk/run to be done simultaneously with turnLeft/turnRight (example taken from `TerrainMovement.cs`). The animat's action space will automatically be set to match the dimensions of its actionmodules upon initialization. However if certain actions in one module should be simultaneous with certain actions from another module, but NOT simultaneous with actions from a third module, then explicit support for this must be added. 


# How To Create A Consumable (v0.5)
To create a consumable, a 3D-object must first be created in the scene, and a component inheriting from `Consumable.cs` must be attached to it. This object should be saved as a prefab and should thereafter be assigned to the `Prefab` field in the `Food Types` array in the areamanager. The available consumable components are as follows:

- `Meat`
    - Nutrients decay over time
    - Eating the whole consumable may require multiple bites
- `StaticFood`
    - Nutrients remain the same until consumable is destroyed
    - Consumable is consumed in exactly one bite
    - Consumable may decay over time (but nutrients remain the same until the consumable ceases to exist)
- `Water`
    - Nutrients never change over time
    - Consumable never ceases to exist
- `GrassCrop`
    - Consumable spreads (creates new consumables) in nearby locations
    - Nutrients increase over time (ripening) and then decay over time
    - Eating the whole consumable may require multiple bites
- `DandelionCrop`
    - Consumable spreads in far-away locations
    - Nutrients increase over time and then decay over time
    - Eating the whole consumable may require multiple bites
<br />
If a behaviour is desired which is not derivable from changing the above consumables' parameters, then a new consumable may be created inheriting from either `Consumable.cs` or `Crop.cs`.


# Necessary Components In Scene (v0.5)
The components which must be included in a scene in order for a simulation to run normally are `Assets/Prefabs/Inanimate/GridArea` and `Assets/Prefabs/Utilities/Necessities`. The GridArea is the object which has the `AreaManager` component attached and controls the scene's objects. Necessities is the object whose children are the scene's camera and the scene's light source. It is also recommended to include `Assets/Prefabs/Utilities/Info Canvas` and `Assets/Prefabs/Utilities/Project Settings`, however neither is essential. <br />
It is also necessary to include walls and roof to define the boundaries of the scene - these objects must also be assigned in the respective fields in the areamanager.


# Libraries Used In Project (v0.5)
- ML Agents
    - Package from Unity designed to facilitate machine learning
- Math.NET Numerics
    - Library providing methods and algorithms for numerical computations


# How To Add Graphics In Project (v0.5)
In Ecotwin's demonstration videos, the non-redistributable package [Animal pack deluxe](https://assetstore.unity.com/packages/3d/characters/animals/animal-pack-deluxe-99702) is used to provide animal graphics models to the animats. If users of Ecotwin do not wish to pay for a license, it is equally possible to use the prefabs provided which use only Unity's primitive shapes as graphics. Note that the graphics package does not alter any of the animats' behaviours and only offers visual effects. If a user chooses to obtain their own license for the above-mentioned package, then the package folder `Animal pack deluxe` is to be placed in `Assets/External Assets/`.


# Implementation Details (v0.5)
More information about the homeostasismodule can be found in the developers' master thesis: Ferrari P. and Kleve B., **A Generic Model of Motivation in Artificial Animals Based on Reinforcement Learning**, Master's thesis, Chalmers, 2021. <br />
More information about the evolution and reflexes can be found in the developers' master thesis: Glimmerfors H. and Skoglund V., **Combining Reflexes and Reinforcement Learning in Evolving Ecosystems for Artificial Animals**, Master's thesis, Chalmers, 2021. <br />
Details regarding the sense of smell can be found in both of the works above, however a description also follows below: <br />
An animat receives a scent signal for every observable type of object. The scent smelled of an object of type T is equal to: sum ((the normalized vector to O) / (distance to O)^2), for all objects O of type T. This scent signal is preprocessed into a vertical and a horizontal component (relative to the animat's rotation). As the direction to an object is divided by the distance squared, this means that a few nearby objects will impact the animat's sense of smell more than many far-away objects. Animats will thus primarily be drawn toward nearby objects and secondarily to larger groups of objects. <br />
When creating an actionmodule, any parameters which are desired to be evolved genetically, the actionmodule must be implemented in a way such that the genotype and phenotype are updated and read in the same way that the pre-existing actionmodules:
- Create variables for initial values
- Initialize genotype and phenotype to variables' initial values
- Define mature/mutate methods for genotype and phenotype
- Use only values from phenotype for actions <br />

An animat's reward signal is based on the animat's happiness (see Ferrari P. and Kleve B.). The happiness is maximized when all of the animat's homeostatic variables are at their desired values. When adding homeostatic variables, the needed parameters are:
- Name
- Initial value (the value an animat is born with)
- Desired (the variable which maximizes happiness)
- Is Sensable (should the value of the variable be an observation?)
- Death LB (the lower bound which will kill the animat - if used)
- Death UB (the upper bound which will kill the animat - if used)
- Domain LB (the lowest value the variable can take)
- Domain UB (the highest value the variable can take)
- Weight (the impact the variable has on happiness - the higher the value, the larger the impact)
- Delta (the change in the variable's value per step)


# Explanation Of Other Parameters In Inspector (v0.5)

## AreaManager (v0.5)
- Do Lifelong Learning (should the animat's policy change over its lifetime?)
- Episode Learning (should training be done in episodes?)
- Episodes Time Limits (the length of episodes)
- Async Episodes (should episodes be handled asynchronously?)
- Day Night Cycle (should the light source oscillate?)
- The Sun (the scene's light source)
- Day Length (the number of steps in a day)
- Month Length (the number of steps in a month)
- Year Length (the number of months in a year)
- Seasonal Food Spread (the percentual food spread based upon seasons) - 0 => normal spread all year, 1 => no spread in winter, full spread in summer
- Wall N/Wall E/Wall S/Wall W/Roof (the minimum/maximum positions available in the scene)
- World Size (the size of the scene - if used) - if used, reshapes the walls/roof to fit the size
- Species (the information of all animats to use in the simulation)
- Food Types (the information of all consumables to use in the simulation) - except Meat! Meat is produced upon an animat's death.
- Terrain (the scene's terrain)
- Sim Info (the scene's component with a `SimulationInfoCanvas.cs`)
- Penetrability (the ratio of an object which can be overlapped by an object before being stopped by collision)
- Count Chunk Managers (the number of chunkmanagers to have in each direction, i.e. the square root of the desired chunk managers)
- Maximum Consumables In Chunk (the maximum number of consumables which can be spawned in any chunk at one time)

## Animat (v0.5)
- Mass (the animat's mass, affects BMR and strength during attacks etc.)
- Size (the size with which an animat will spawn)
- Initial Age (the age of the animat spawned at the simulation's start)
- Maximum Age (the age at which the animat dies)
- Is Immortal (should the animat revive upon death?)
- Punishment For Dying (negative reward given upon death)
- Create Corpse Upon Unnatural Death (create meat upon dying due to being attacked/eating poisonous food etc.?)
- Create Corpse Upon Natural Death (create meat upon dying due to homeostatic variable's death bounds?)
- Corpse Prefab (the meat object to create)
- Fertility (the chance to have an offspring during reproduction) - this is multiplied with the partner's value
- Species Libido (the animat's change in libido per step) - is named *Species* Libido because this is not mutated
- Species Fertility Offset (the time offset between the animat's fertility and libido)
- Species Recovery Pregnancy (the number of months during which a female animat's libido and fertility cannot increase, after giving birth)
- Maturity Age (the age at which an animat gets access to fertility and libido, and stops maturing)
- Is Female (is the animat female?) - otherwise male
- Randomize Sex (should the animat's sex be random or set manually during creation?)
- Action Modules (the action modules with the actions an animat can take)
- Time Between Decisions At Inference (the time betwen taking decisions if using inference - i.e. not training)
- Observable Tags (the tags which may be observed, and how)
- Slider Bars (the visuals for the "health bars", both name and object attached to animat)
- HomeostasisFactory (the homeostatic variables used in creating a homeostasismodule)
- BMR (the constant multiplied by the mass^(3/4))
- Nutrients Effect Map (all the nutrients which can be intaken during ingestion, and how each nutrient affects the animat's energy levels)
- Light Sensitive (does the animat observe light?)
- Min Visible Light (the minimum light levels which can be observed)
- Disable Reflexes On Age (at what age should reflexes stop being used - if used)
- Detectable Tags (in `RayPerceptionSensor3D` component) - the tags which can be seen with raycasts

## Crop (v0.5)
Consumables are similar: <br />
- Nutrients (the nutrients found in the consumable)
- Genes (the initial nutrients and colors of the consumable, the ripened/decayed nutrients and colors of the consumable, the number of steps before ripening/decaying)
- Fertility (the expected crops to create in a crop's lifetime)
- Spread Radius (the radius within the crop can create new crops)
- Grace Radius (the radius within which no new crops may be created)
- Hostile Radius (the radius within which no new crops of other species may be created)
- Winter Resistance (the percentual resistance to Seasonal Food Spread)